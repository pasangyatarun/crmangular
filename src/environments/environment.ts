// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};
// export const baseUrl = 'http://94.237.3.78:5000/api/';
// export const baseUrl = 'http://localhost:3000/api/';
export const baseUrl = 'http://95.111.202.157:5000/api/';


/** this url for the invoice download */
// production url  :-    https://app.billwerk.com;
export const billwerkAccountUrl = 'https://sandbox.billwerk.com';

/** this is for the billwerk payment method */
// export const billwerkPaymentMethod = "Debit:FakeProvider";
export const billwerkPaymentMethod = "Debit:SepaXml";



/** this for the firebase setup */

export const fireStore = {
  apiKey: "AIzaSyC8iOcxDzuq-RdRufPOq9hSDeDurg7nEjE",
  authDomain: "friendlychat-97fd3.firebaseapp.com",
  databaseURL: "https://friendlychat-97fd3.firebaseio.com",
  projectId: "friendlychat-97fd3",
  storageBucket: "friendlychat-97fd3.appspot.com",
  messagingSenderId: "587050570367",
  appId: "1:587050570367:web:244c4636dbf2a9e47e654a"
};

export const mandateText = `Ich ermächtige/ Wir ermächtigen <br>
(A) Tineon AG Zahlungen von meinem/ unserem Konto mittels Lastschrift einzuziehen.
Zugleich <br>
(B) weise ich mein/ weisen wir unser Kreditinstitut an, die von Tineon AG auf mein/ unser
Konto
gezogenen Lastschriften einzulösen. <br>
<strong>Hinweis:</strong> Ich kann/ Wir können innerhalb von acht Wochen, beginnend mit
dem Belastungsdatum, die
Erstattung des belasteten Betrages verlangen. Es gelten dabei die mit meinem/ unserem
Kreditinstitut
vereinbarten Bedingungen. <br>`
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
