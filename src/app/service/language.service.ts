import { Injectable } from '@angular/core';
import  *  as deutschLanguage from "../deutsch_language.json";
import  *  as englishLanguage from "../english_language.json";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  deutschlanguage: any = (deutschLanguage as any).default;
  englishlanguage: any = (englishLanguage as any).default;

  constructor() { }

  getLanguaageFile() {
    let language = localStorage.getItem('language');
    if(language=="en"){
      return this.englishlanguage;
    }else{
      return this.deutschlanguage;
    }
  }
}
