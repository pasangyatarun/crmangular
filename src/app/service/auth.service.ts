import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, JsonpClientBackend } from '@angular/common/http';
import { Router } from '@angular/router';
import { baseUrl } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { map } from "rxjs/operators";
import { pipe, Subject, throwError } from 'rxjs';
import { ErrorHandlerComponent } from '../common/error-handler/error-handler.component';
import { MatSnackBar } from '@angular/material/snack-bar';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  uncaughtError: boolean = false;
  error: any;
  permissions: any;
  role: any[] = [];
  counter: any = new Subject;
  count: any
  constructor(private http: HttpClient, private router: Router,private snackBar: MatSnackBar) {
  }

  sendRequest(method: string, endPoint: string, data: any) {
    return this.actualSendRequest(method, endPoint, data);
  }

  actualSendRequest(method: any, endPoint: any, data: any) {
    let myHeaders: any;
    var token = "TinonNode@2021:" + JSON.parse(localStorage.getItem('token') || '{}');
    if (localStorage.getItem('token') == null) {
      myHeaders = new HttpHeaders({
        'authorization': 'VGlub25Ob2RlQDIwMjE=',
        'platform': 'web',
        'accept': 'application/json'
      });
    } else {
      var token = "TinonNode@2021:" + JSON.parse(localStorage.getItem('token') || '{}');
      let baseEncoded = btoa(token);
      myHeaders = new HttpHeaders({
        'authorization': baseEncoded,
        'platform': 'web',
        'accept': 'application/json'
      });
    }

    let endPointUrl: any;

    endPointUrl = `${baseUrl}` + endPoint + ``;
    if (method == 'post') {
      return this.http.post(endPointUrl, data, { headers: myHeaders })
        .pipe(
          map(data => {
            return data
          }),
          catchError(error => {
            return this.handleError(error);
          })
        );
    } else if (method == 'put') {
      return this.http.put(endPointUrl,
        data, { headers: myHeaders }).pipe(
          map(data => {
            return data
          }),
          catchError(error => {
            return this.handleError(error);
          })
        );
    } else if (method == 'delete') {
      return this.http.delete(endPointUrl, { headers: myHeaders }).pipe(
        map(data => {
          return data
        }),
        catchError(error => {
          return this.handleError(error);
        })
      );
    } else {
      return this.http.get(endPointUrl, { headers: myHeaders }).pipe(
        map(data => {
          return data
        }),
        catchError(error => {
          return this.handleError(error);
        })
      );
    }
  }

  private handleError(error: HttpErrorResponse) {
    this.error = error;
    let errorMessage = '';
    if (error.status == 0) {
      errorMessage = 'An error occurred:the request failed for some reason';
      this.showSnackbarAction(errorMessage,'ok')
    } else if (error.status == 401) {
      errorMessage = 'An error occurred :- Unauthorized error please login first';
      this.logout();
    } else if (error.status == 400) {
      errorMessage = 'An error occurred:-Bad Request Error occurs';
      this.showSnackbarAction(errorMessage,'ok')
    } else if (error.status == 500) {
      errorMessage = " An error occurred:-Something went wrong. Please try again after some time";
      this.showSnackbarAction(errorMessage,'ok')
    }
    else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
      this.showError();
      errorMessage = ` ${error.status},`, error.error;

    }
    // Return an observable with a user-facing error message.
    this.setLoader(false);
    return throwError(errorMessage);
  }


  showError() {
    return "'Something bad happened; please try again later.'";
  }

  IsLoggedIn() {
    //it returns a boolean value, if the token exsist then true else vice versa
    return !!localStorage.getItem('token');
  }

  loader: Boolean = false;
  setLoader(value: Boolean) {
    this.loader = value;
  }
  get showLoader() {
    return this.loader;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user-data');
    localStorage.removeItem('role');
    localStorage.removeItem('permission');
    localStorage.removeItem('trialProductInstalled');
    localStorage.removeItem('registration');
    localStorage.removeItem('process');
    localStorage.removeItem('user-details');
    this.router.navigate(['/signin']);
  }

  popUp: Boolean = false;
  setpopUp(value: Boolean) {
    this.popUp = value;
  }
  get showpopUp() {
    return this.popUp;
  }

  setCounter(isCounter: any) {
    this.counter.next(isCounter);
  }

  showSnackbarAction(content: string, action: string | undefined) {
    this.snackBar.open(content, action, {
      // duration: 2000,
      verticalPosition: "bottom", // Allowed values are  'top' | 'bottom'
      horizontalPosition: "center" // Allowed values are 'start' | 'center' | 'end' | 'left' | 'right'
    });
  }

  isAdmin() {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    return this.role.some((o: any) => o.name == 'Administrator' || o.role == 'Administrator' || o.name == 'Super-Admin' || o.role == 'Super-Admin' );
  }

}
