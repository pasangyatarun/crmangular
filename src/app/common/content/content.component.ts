import { Component, OnInit, SimpleChanges } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  language: any;
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  message: any;
  data: any;
  pageUrl: any;
  title: any;

  constructor(private location: Location, private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.route.params.subscribe((val: any) => {
      this.pageUrl = val.data;
      if (this.pageUrl == 'privacy') {
        this.title = this.language.common.privacy;
      } else if (this.pageUrl == 'imprint') {
        this.title = this.language.common.imprint;
      } else if (this.pageUrl == 'conditions') {
        this.title = this.language.common.condition
      }
      this.page(val.data);
    });
  }

  page(pageUrl: any) {
    this.authService.setLoader(true);
    var endPoint = 'pagesbyurl/' + pageUrl
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList;
        this.message = this.result.content.messageList.pages;
      } else if (this.result.success == true) {
        this.error = false;
        this.data = this.result.content.dataList
      }
    })
  }


}
