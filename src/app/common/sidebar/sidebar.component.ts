import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { LanguageService } from '../../service/language.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent implements OnInit {

  userData: any;
  role: any;
  language: any;
  permissions: any;
  modules: any = [];
  userId: any;

  constructor(private router: Router, public authService: AuthService, private langService: LanguageService) {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.userId = this.userData[0].uid
  }

  public ngOnInit() {
    $(document).ready(function () {
      $(document).ready(function () {
        $('#toggleMenubar, .hamburger').on('click', function () {
          $('body').toggleClass('site-menubar-unfold').toggleClass('site-menubar-fold');
        });
      });
      $('.site-menu-item').on('click', function (e) {
        $('.site-menu-item a').toggleClass("active"); //you can list several class names 
        e.preventDefault();
      });
    });

    this.getPermission();
    this.language = this.langService.getLanguaageFile();

  }

  getPermission() {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
  }

  isCustomer() {
    return this.role.some((o: any) => o.name == 'SGP-Wechsler' || o.role == 'SGP-Wechsler' || o.name == 'Customer' || o.role == 'Customer');
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

}
