import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
// import * as $ from 'jquery';
import { LanguageService } from 'src/app/service/language.service';
import { SubscriptionLoggable } from 'rxjs/internal/testing/SubscriptionLoggable';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
declare let $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userData: any;
  language: any;
  displayFlag: any = 'en';
  result: any;
  dayCount: any;
  trialProduct: any;
  role: any;
  user: any;
  productName: any;
  isCounter: any = true
  interval: any;
  imagePreview: any;
  hasPicture = false;
  private subs: Subscription = new Subscription;
  error: any;
  vereinspgTypeProduct: any = [];
  productData: any;
  productSelectForm: FormGroup;
  isSubmitted: any = false;
  displayMsg: any;

  constructor(public authservice: AuthService, private formBuilder: FormBuilder, private router: Router, private langService: LanguageService) {
    this.productSelectForm = this.formBuilder.group({
      PlanVariantId: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.displayFlag = localStorage.getItem('language');
    this.getuserData();
    this.getOrder();
    this.subs = this.authservice.counter.subscribe((res: any) => {
      if (res == true) {
        this.dayCount = null;
        clearInterval(this.interval);
      } else if (res == false) {
        this.getOrder();
      }
    });
    this.getAllProducts();
  }

  logout() {
    this.authservice.logout();
    this.router.navigate(['/signin'])
  }

  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    var endPoint = 'language'
    let data = {
      "language": lan
    }
    this.authservice.sendRequest('post', endPoint, data).subscribe((result: any) => {
      this.result = result;
      window.location.reload();

    })
  }

  getOrder() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')

    this.authservice.sendRequest('get', 'billwerk-orders/bycustomer/' + id[0].uid, null).subscribe(
      (respData: any) => {
        this.result = respData;      
          
        if (this.result.success == true) {
          let isRole = this.role.some((o: any) => o.name == 'SGP-Wechsler' || o.role == 'SGP-Wechsler');
          if (isRole) {
            this.result.content.dataList.forEach((element: any, index: any) => {
              let cudate = new Date()
              var endDate = new Date(element.Contract.StartDate);
              endDate.setDate(endDate.getDate() + 90);
              //  this.trialProduct = endDate
              if (element.AllowWithoutPaymentData == true && endDate >= cudate) {
                if (this.result.content.dataList.length > 1) {
                  this.displayMsg = 'Paid license starts in'
                } else {
                  this.displayMsg = this.language.billwerk.trialPeriod
                }
                if (element && element.Contract && element.Contract.EndDate) {
                  this.productName = element.PlanVariantName
                  this.trialProduct = element.Contract.EndDate;
                  let trialProduct = JSON.parse(localStorage.getItem('trialProductInstalled') || '{}');
                  if (trialProduct == 1 && this.trialProduct) {
                    $('#trialPop').modal({
                      backdrop: 'static'
                    });
                    $('#trialPop').modal('show');
                  }
                  this.countDown();
                } else {
                  this.productName = element.PlanVariantName;
                  var endDate = new Date(element.Contract.StartDate);
                  endDate.setMonth(endDate.getMonth() + 3);
                  this.trialProduct = endDate;
                  let trialProduct = JSON.parse(localStorage.getItem('trialProductInstalled') || '{}');
                  if (trialProduct == 1 && this.trialProduct) {
                    $('#trialPop').modal({
                      backdrop: 'static'
                    });
                    $('#trialPop').modal('show');
                  }
                  this.countDown();
                }
              }
            });
          } else {
            
            this.displayMsg = this.language.billwerk.trialPeriod
            if (this.result.content.dataList.length == 1) {
              
              this.result.content.dataList.forEach((element: any) => {
                if (element.AllowWithoutPaymentData == true) {
                  if (element && element.Contract && element.Contract.EndDate) {
                    this.productName = element.PlanVariantName
                    
                    this.trialProduct = element.Contract.EndDate;
                    let trialProduct = JSON.parse(localStorage.getItem('trialProductInstalled') || '{}');
                    if (trialProduct == 1 && this.trialProduct) {
                      $('#trialPop').modal({
                        backdrop: 'static'
                      });
                      $('#trialPop').modal('show');
                    }
                    this.countDown();
                  } else {
                    this.productName = element.PlanVariantName;
                    var endDate = new Date(element.Contract.StartDate);
                    endDate.setMonth(endDate.getDate() + 30);
                    this.trialProduct = endDate;
                    let trialProduct = JSON.parse(localStorage.getItem('trialProductInstalled') || '{}');
                    if (trialProduct == 1 && this.trialProduct) {
                      $('#trialPop').modal({
                        backdrop: 'static'
                      });
                      $('#trialPop').modal('show');
                    }
                    this.countDown();
                  }

                }

              });
            }
          }

        }
      })
  }

  countDown() {
    var intevalId    
    if (this.trialProduct) {
      var countDownDate = new Date(this.trialProduct).getTime();
      this.interval = setInterval(() => {
        // Get today's date and time
        var now = new Date().getTime();
        // Find the distance between now and the count down date
        var distance = countDownDate - now;        
        // var distance = 7945513439 - 7945513439;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));        
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        if (days > 0 || hours > 0 || minutes > 0 || seconds > 0) {
          this.dayCount = days + ' day ' + hours + ' hours ' + minutes + ' minutes ' + seconds + ' seconds '          
        }
        else {
          if (this.role[0].name == 'SGP-Wechsler') {
            $('#trialspgProductExpire').modal({
              backdrop: 'static'
            });
            $('#trialspgProductExpire').modal('show');
          } else {
            $('#trial1ProductExpire').modal({
              backdrop: 'static'
            });
            $('#trial1ProductExpire').modal('show');
          }
        }
      }, 1000);
      
    } else {
    }


  }

  getuserData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'userbyid/' + id[0].uid;
    this.authservice.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
      } else if (this.result.success == true) {
        this.user = this.result.content.dataList;
        if (this.user[0].image != '' && this.user[0].image != 'undefined') {
          this.hasPicture = true;
          this.imagePreview = this.user[0].image;
        }

      }
    })

  }

  proceed() {
    var trialProductInstalled = '0';
    localStorage.setItem('trialProductInstalled', trialProductInstalled);
    $('#trialPop').modal('hide');
  }


  getAllProducts() {
    var endPoint = 'billwerk-product'
    this.authservice.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.productData = this.result.content.dataList;
        this.productData.forEach((element: any) => {
          if (element.planDetail.CustomFields.type == 3 && element.AllowWithoutPaymentData == false) {
            this.vereinspgTypeProduct.push(element);
          }
        });
      }
    })
  }

  trial1ProductExpire() {
    this.dayCount = null;
    clearInterval(this.interval);
    $('#trial1ProductExpire').modal('hide');
    this.router.navigate(['/product-price'])
  }

  onradioChange(planId: any) {
    this.productSelectForm.controls["PlanVariantId"].setValue(planId);
  }

  get formControls() { return this.productSelectForm.controls; }

  onSubmit() {
    this.isSubmitted = true;
    if (this.productSelectForm.invalid) {
      return;
    } else {
      $('#trialspgProductExpire').modal('hide');
      this.router.navigate(['product-details/' + this.productSelectForm.value.PlanVariantId])
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
