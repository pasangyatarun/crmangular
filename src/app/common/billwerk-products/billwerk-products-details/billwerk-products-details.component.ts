import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import { ActivatedRoute } from '@angular/router';
declare let $: any;

@Component({
  selector: 'app-billwerk-products-details',
  templateUrl: './billwerk-products-details.component.html',
  styleUrls: ['./billwerk-products-details.component.css']
})
export class BillwerkProductsDetailsComponent implements OnInit {

  userData: any;
  language: any;
  result: any;
  productDetails: any;
  productData: any
  msg: any;
  visibility: any = 0;
  customer: any;
  userOrderList: any;
  productId: any;
  isSubmitted: boolean = false;
  orderId: any;
  pdfButton: boolean = false;
  field: boolean = false;
  displayFlag: any = 'en';
  errorMessage: boolean = false;
  successMessage: boolean = false;
  message: any;
  error: boolean = false;
  success: boolean = false;
  show: boolean = false;
  emailVerifyForm: FormGroup;
  rediredtedTo: any;

  constructor(private formBuilder: FormBuilder, private router: Router, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {
    this.emailVerifyForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required])],
    });

    if (this.authService.IsLoggedIn()) {
      let userData = JSON.parse(localStorage.getItem('user-data') || '{}');
      this.emailVerifyForm.controls['email'].setValue(userData[0].email)
    }
  }

  get formControls() { return this.emailVerifyForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.productId = this.route.snapshot.paramMap.get('productId');
    this.getproductDetails();
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.displayFlag = localStorage.getItem('language');
  }


  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    window.location.reload();
  }

  getproductDetails() {
    this.visibility = 1;
    this.authService.setLoader(true);
    var endPoint = 'billwerk-productbyid/' + this.productId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.errorMessage = true;
        this.successMessage = false;
        this.message = this.result.content.messageList.billwerk;
      } else if (this.result.success == true) {
        this.errorMessage = false;
        this.msg = this.result.content.messageList.billwerk;
        this.productDetails = this.result.content.dataList;
      }
    })
  }

  orderProduct(orderId: any) {
    $('#email').modal('show');
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.emailVerifyForm.invalid) {
      return;
    } else {
      var endPoint = 'userbyemail/' + this.emailVerifyForm.value.email;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result;
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.errorMessage = false;
        } else if (this.result.success == true) {
          this.errorMessage = false;
          this.error = false;
          this.success = true;
        }
      })

    }
  }

  redirectedTo() {
    if (this.error == true) {
      $('#email').modal('hide');
      this.router.navigate(['/signup'], { queryParams: { product: this.productId, email: this.emailVerifyForm.value.email } });
    } else if (this.success == true) {
      $('#email').modal('hide');
      this.router.navigate(['/signin'], { queryParams: { product: this.productId, email: this.emailVerifyForm.value.email } });
    }
  }

  cancel() {
    this.error = false;
    this.success = false;
  }

}
