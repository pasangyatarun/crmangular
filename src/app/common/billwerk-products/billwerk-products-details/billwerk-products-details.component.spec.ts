import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillwerkProductsDetailsComponent } from './billwerk-products-details.component';

describe('BillwerkProductsDetailsComponent', () => {
  let component: BillwerkProductsDetailsComponent;
  let fixture: ComponentFixture<BillwerkProductsDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillwerkProductsDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillwerkProductsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
