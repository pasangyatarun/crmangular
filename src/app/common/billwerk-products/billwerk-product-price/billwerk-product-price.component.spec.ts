import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillwerkProductPriceComponent } from './billwerk-product-price.component';

describe('BillwerkProductPriceComponent', () => {
  let component: BillwerkProductPriceComponent;
  let fixture: ComponentFixture<BillwerkProductPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillwerkProductPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillwerkProductPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
