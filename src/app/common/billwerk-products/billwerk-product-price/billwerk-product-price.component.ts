import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { LanguageService } from '../../../service/language.service';
declare let $: any;

@Component({
  selector: 'app-billwerk-product-price',
  templateUrl: './billwerk-product-price.component.html',
  styleUrls: ['./billwerk-product-price.component.css']
})
export class BillwerkProductPriceComponent implements OnInit {
  userData: any;
  language: any;
  result: any;
  error: any;
  productDetails: any;
  productData: any
  msg: any;
  customer: any;
  currentPageNmuber: number = 1;
  itemPerPage = 8;
  limitPerPage = [
    { value: '8' },
    { value: '16' },
    { value: '24' },
    { value: '32' },
    { value: '40' }
  ];

  totalRecord = 0;
  responseMessage: any;
  clubTypeProducts: any = [];
  financialTypeProducts: any = [];
  vereinCloudTypeProduct: any = [];
  productComponent: any = [];

  constructor(private router: Router, public authService: AuthService, private langService: LanguageService) {
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getAllProducts();
    this.getProductComponent();
  }

  getCustomersDetails() {
    this.authService.setLoader(true);
    var endPoint = 'userbyid/' + this.userData[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.customer = this.result.content.dataList;
      }
    })
  }

  getAllProducts() {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-product'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.productData = this.result.content.dataList;
        this.clubTypeProducts = [];
        this.financialTypeProducts = [];
        this.productData.forEach((element: any) => {
          if (element.planDetail.CustomFields.IsClubAdministration == 1) {
            this.clubTypeProducts.push(element);
          } else if (element.planDetail.CustomFields.IsClubAdministration == 2) {
            this.financialTypeProducts.push(element);
          } else if (element.planDetail.CustomFields.IsClubAdministration == 0) {
            this.vereinCloudTypeProduct.push(element);
          }
        });
        this.totalRecord = this.result.content.dataList.length;
      }
    })
  }

  getProductComponent() {
    var endPoint = 'billwerk-product-components'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {
          if (element.CustomFields.type == 1) {
            this.productComponent.push(element);
          }
        });
      }
    })
  }


}
