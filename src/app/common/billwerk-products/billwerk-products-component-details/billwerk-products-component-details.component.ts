import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import { ActivatedRoute } from '@angular/router';
declare let $: any;

@Component({
  selector: 'app-billwerk-products-component-details',
  templateUrl: './billwerk-products-component-details.component.html',
  styleUrls: ['./billwerk-products-component-details.component.css']
})
export class BillwerkProductsComponentDetailsComponent implements OnInit {

  language: any;
  result: any;
  error: any;
  userOrderList: any;
  productId: any;
  isSubmitted: any = false;
  orderId: any;
  pdfButton: any = false;
  field: any = false;
  type: any;
  editProfile: any;
  commitData: any;
  clubOrderError: any;
  errorMessage: any = false;
  successMessage: any = false;
  message: any;
  productComponent: any = [];

  constructor(private formBuilder: FormBuilder, private router: Router, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.productId = this.route.snapshot.paramMap.get('productId');
    this.getProductComponents(this.productId);
  }

  getProductComponents(productId: any) {
    var endPoint = 'billwerk-product-components'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {
          if (element.Id == productId) {
            this.productComponent.push(element);
          }
        });
      }
    })
  }

}
