import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillwerkProductsComponentDetailsComponent } from './billwerk-products-component-details.component';

describe('BillwerkProductsComponentDetailsComponent', () => {
  let component: BillwerkProductsComponentDetailsComponent;
  let fixture: ComponentFixture<BillwerkProductsComponentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillwerkProductsComponentDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillwerkProductsComponentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
