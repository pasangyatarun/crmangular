import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { LanguageService } from 'src/app/service/language.service';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  forgetPassForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  language: any;
  error: any;
  success: any;
  displayFlag: any = 'en';


  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private langService: LanguageService) {
    this.forgetPassForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
    });


  }

  get formControls() { return this.forgetPassForm.controls; }

  get f() {
    return this.forgetPassForm.controls;
  }

  ngOnInit(): void {
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.language = this.langService.getLanguaageFile();
  }

  onSubmit() {
    this.isSubmitted = true;
    this.authService.setLoader(true);
    if (this.forgetPassForm.invalid) {
      this.authService.setLoader(false);
      return;
    } else {
      var endPoint = 'resetpasswordlink'
      this.authService.sendRequest('post', endPoint, this.forgetPassForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.forget;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.forget;
          this.isSubmitted = false;
          this.forgetPassForm.reset();
        }
      })
    }

  }

  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    window.location.reload();
  }

}
