import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageService } from '../../service/language.service';
declare let $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  errorMsg: any;
  show: any = false;
  file: any;
  id: any;
  reEnterData: any;
  language: any;
  error: any = false;
  success: any = false;
  domain: any;
  imageSrc: any;
  hasPicture = false;
  imagePreview: any;
  user: any;
  customer_id: any;
  params: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.registrationForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      conEmail: ["", Validators.compose([Validators.required, Validators.email])],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      society: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      zipcode: ["", Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      place: ["", Validators.compose([Validators.required, Validators.required])],
      contactNumber: ["", Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[- +()0-9]+')])],
      termsCon: ['', Validators.required],

    });

  }


  get formControls() { return this.registrationForm.controls; }

  get f() {
    return this.registrationForm.controls;
  }

  ngOnInit(): void {
    this.mustMatch();
    this.language = this.langService.getLanguaageFile();
    this.route.queryParams.subscribe(params => {
      this.params = params;
      this.registrationForm.controls['email'].setValue(this.params.email);
      this.registrationForm.controls['conEmail'].setValue(this.params.email);
    });
    this.setValue();
  }

  mustMatch() {
    this.registrationForm.valueChanges.subscribe(data => {
      if (data.email == data.conEmail) {
        this.show = false;
      } else {
        this.show = true;
      }
    });;
  }

  setValue() {
    let data: any
    data = JSON.parse(localStorage.getItem('registration') || '{}');
    if (Object.keys(this.params).length != 0) {
      this.registrationForm.controls['email'].setValue(this.params.email);
      this.registrationForm.controls['conEmail'].setValue(this.params.email);
    } else {
      this.registrationForm.controls["email"].setValue(data.email);
      this.registrationForm.controls["conEmail"].setValue(data.conEmail);
    }
    this.registrationForm.controls["society"].setValue(data.society);
    this.registrationForm.controls["firstName"].setValue(data.firstName);
    this.registrationForm.controls["lastName"].setValue(data.lastName);
    this.registrationForm.controls["address"].setValue(data.address);
    this.registrationForm.controls["zipcode"].setValue(data.zipcode);
    this.registrationForm.controls["place"].setValue(data.place);
    this.registrationForm.controls["country"].setValue(data.country);
    this.registrationForm.controls["contactNumber"].setValue(data.contactNumber);
    if (data.termsCon == true) {
      this.registrationForm.controls["termsCon"].setValue(data.termsCon);
    }
  }

  onSubmit() {

    this.registrationForm.value.productId = this.params.product
    this.isSubmitted = true;
    if (this.registrationForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'signup-user'
      this.authService.sendRequest('post', endPoint, this.registrationForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList

          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.userAdd;

        } else if (this.result.success == true) {
          $('#register').modal('show');
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.userAdd;
          this.isSubmitted = false;
          this.registrationForm.reset();
          localStorage.removeItem('registration');
        }
      })

    }
  }

  termsCondition() {
    localStorage.setItem('registration', JSON.stringify(this.registrationForm.value));
    this.router.navigate(['/terms-and-condition']);
  }

  close() {
    $('#register').modal('hide');
    this.router.navigate(['/signin']);
  }
}
