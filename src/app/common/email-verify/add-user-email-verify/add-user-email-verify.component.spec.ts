import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserEmailVerifyComponent } from './add-user-email-verify.component';

describe('AddUserEmailVerifyComponent', () => {
  let component: AddUserEmailVerifyComponent;
  let fixture: ComponentFixture<AddUserEmailVerifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUserEmailVerifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserEmailVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
