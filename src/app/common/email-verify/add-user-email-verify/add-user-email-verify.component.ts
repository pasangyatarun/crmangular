import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { timer, Observable } from "rxjs";
import { takeWhile, takeUntil, tap, map } from "rxjs/operators";

@Component({
  selector: 'app-add-user-email-verify',
  templateUrl: './add-user-email-verify.component.html',
  styleUrls: ['./add-user-email-verify.component.css']
})
export class AddUserEmailVerifyComponent implements OnInit {

  passwordForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  id: any;
  data: any;
  userId: any;
  language: any;
  product: any;
  value: any = 0;
  sub: any;
  progress: any = false;
  params: any;
  userData: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {

    this.data = this.route.snapshot.paramMap.get('data');
    this.passwordForm = this.formBuilder.group({
      id: this.data,
      password: ["", Validators.compose([Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{5,}')])],
      confirmPassword: ["", Validators.compose([Validators.required])],
      processStatus: [""],

    });
    this.language = this.langService.getLanguaageFile();
  }

  get formControls() { return this.passwordForm.controls; }

  get f() {
    return this.passwordForm.controls;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('data');
    this.mustMatch();
    this.route.queryParams.subscribe(params => {
      this.params = params;
      
    });

  }

  mustMatch() {
    this.passwordForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.passwordForm.invalid) {
      this.authService.setLoader(false);
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'emailverify'
      if (this.params && this.params.spg) {
        this.passwordForm.value.processStatus = 's1'
      } else  if (this.params && this.params.customer)  {
        this.passwordForm.value.processStatus = 'n1'
      }else {
        this.passwordForm.value.processStatus = ''
      }
   
      this.authService.sendRequest('post', endPoint, this.passwordForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.verify;
          this.msg = this.result.content.messageList
        } else if (this.result.success == true) {
          this.setLanguage();
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.verify;
          var data = this.result.content.dataList[0]['token'];
          localStorage.setItem('permission', JSON.stringify(this.result.content.dataList[0].permission))
          sessionStorage.setItem('token', data);
          localStorage.setItem('process', this.result.content.dataList[0].processStatus);
          localStorage.setItem('token', JSON.stringify(data));
          localStorage.setItem('role', JSON.stringify(this.result.content.dataList[0].role));
          localStorage.setItem('user-data', JSON.stringify(this.result.content.dataList));
          this.userId = this.result.content.dataList[0].uid;
          this.getbillworkToken();
          if (this.params && this.params.spg) {
            this.router.navigate(['/profile-edit-spg']);
          } else if (this.params && this.params.customer){
            this.router.navigate(['/profile-edit']);
          }else{
            this.router.navigate(['/dashboard']);
          }
        }
      })
    }
  }

  setLanguage() {
    let lan = localStorage.getItem('language');
    var endPoint = 'language'
    let data = {
      "language": lan
    }
    this.authService.sendRequest('post', endPoint, data).subscribe((result: any) => {
      this.result = result;
    })
  }

  getbillworkToken() {
    var endPoint = 'billwerk-signin'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
    })
  }

}
