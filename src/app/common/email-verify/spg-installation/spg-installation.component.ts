import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { timer, Observable } from "rxjs";
import { takeWhile, takeUntil, tap, map } from "rxjs/operators";
import { billwerkPaymentMethod } from 'src/environments/environment';
import { mandateText } from 'src/environments/environment';
declare let $: any;

@Component({
  selector: 'app-spg-installation',
  templateUrl: './spg-installation.component.html',
  styleUrls: ['./spg-installation.component.css']
})
export class SpgInstallationComponent implements OnInit {

  editProfile: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  id: any;
  data: any;
  userId: any;
  language: any;
  product: any;
  value: any = 0;
  sub: any;
  progress: any = false;
  params: any;
  userData: any;
  field: any = false;
  productData: any;
  vereinspgTypeProduct: any = [];
  productDetails: any;
  productComponent: any = [];
  role: any;
  imagePreview: any;
  hasPicture = false;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {

    this.data = this.route.snapshot.paramMap.get('data');
    this.editProfile = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      society: ['', Validators.required],
      addressLine1: ['', Validators.required],
      zipcode: ["", Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      place: ["", Validators.compose([Validators.required, Validators.required])],
      accountNumber: ["", Validators.compose([Validators.pattern('[0-9]+')])],
      contactNumber: ["", Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[- +()0-9]+')])],
      country: ['', Validators.required],
      termsCon: ['', Validators.required],
      bankCode: [''],
      addressLine2: [''],
      status: [''],
      userRole: new FormArray([]),
      image: [''],
      // auther_id:['']

    });
    this.language = this.langService.getLanguaageFile();
  }

  get formControls() { return this.editProfile.controls; }
  get f() {
    return this.editProfile.controls;
  }
  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.getAllProducts();
    this.getProductComponent();
    this.getCustomerData();
  }

  getAllProducts() {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-product'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.productData = this.result.content.dataList;
        this.productData.forEach((element: any) => {
          if (element.planDetail.CustomFields.type == 3 && element.AllowWithoutPaymentData == false) {
            this.vereinspgTypeProduct.push(element);
          }
        });
      }
    })
  }

  getProductComponent() {
    var endPoint = 'billwerk-product-components'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {

        let isRole = this.role.some((o: any) => o.name == 'SGP-Wechsler' || o.role == 'SGP-Wechsler');

        if (isRole) {
          this.result.content.dataList.forEach((element: any) => {
            if (element.CustomFields.type == '0') {
              this.productComponent.push(element);
            }
          });
        } else {
          this.result.content.dataList.forEach((element: any) => {
            if (element.CustomFields.type == '1') {
              this.productComponent.push(element);
            }
          });
        }
      }
    })
  }


  getproductDetails(productId: any) {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-productbyid/' + productId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.billwerk;
      } else if (this.result.success == true) {
        this.error = false;
        this.msg = this.result.content.messageList.billwerk;
        this.productDetails = this.result.content.dataList;
        if (this.productDetails) {
          $('#productDetails').modal({
            backdrop: 'static'
          });
          $('#productDetails').modal('show');
        }

      }
    })
  }

  getCustomerData() {
    let userId = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'userbyid/' + userId[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.userData = this.result.content.dataList;
        this.setValue(this.userData[0]);
      }
    })
  }

  setValue(data: any) {

    if (localStorage.getItem('user-details') != null) {
      let userData = JSON.parse(localStorage.getItem('user-details') || '{}');
      this.editProfile.controls["email"].setValue(userData.email);
      this.editProfile.controls["firstName"].setValue(userData.firstName);
      this.editProfile.controls["lastName"].setValue(userData.lastName);
      this.editProfile.controls["society"].setValue(userData.society);
      this.editProfile.controls["addressLine1"].setValue(userData.addressLine1);
      this.editProfile.controls["zipcode"].setValue(userData.zipcode);
      this.editProfile.controls["place"].setValue(userData.place);
      this.editProfile.controls["country"].setValue(userData.country);
      this.editProfile.controls["contactNumber"].setValue(userData.contactNumber);
      this.editProfile.controls["addressLine2"].setValue(userData.addressLine2);
      this.editProfile.controls["status"].setValue(userData.status);
      // this.editProfile.controls["auther_id"].setValue(userData.auther_id);
      if (userData && userData.userRole) {
        userData.userRole.forEach((element: any) => {
          const formArray: FormArray = this.editProfile.get('userRole') as FormArray;
          formArray.push(new FormControl(element));
        });
      }

      if (userData && userData.termsCon) {
        this.editProfile.controls["termsCon"].setValue(true);
      }

      if (userData.image != '' && userData.image != 'undefined') {
        this.editProfile.controls['image'].clearValidators();
        this.editProfile.controls['image'].updateValueAndValidity();
        this.hasPicture = true;
        this.imagePreview = userData.image;
      }

    } else {
      this.editProfile.controls["email"].setValue(data.mail);
      this.editProfile.controls["email"].setValue(data.mail);
      this.editProfile.controls["firstName"].setValue(data.firstName);
      this.editProfile.controls["lastName"].setValue(data.lastName);
      this.editProfile.controls["society"].setValue(data.society);
      this.editProfile.controls["addressLine1"].setValue(data.addressLine1);
      this.editProfile.controls["zipcode"].setValue(data.zipcode);
      this.editProfile.controls["place"].setValue(data.place);
      this.editProfile.controls["country"].setValue(data.country);
      this.editProfile.controls["contactNumber"].setValue(data.contactNumber);
      this.editProfile.controls["addressLine2"].setValue(data.addressLine2);
      this.editProfile.controls["status"].setValue(data.status);
      // this.editProfile.controls["auther_id"].setValue(data.auther_id);

      if (data && data.role) {
        data.role.forEach((element: any) => {
          const formArray: FormArray = this.editProfile.get('userRole') as FormArray;
          formArray.push(new FormControl(element.rid));
        });
      }

      if (data && data.termsCon) {
        this.editProfile.controls["termsCon"].setValue(true);
      }

      if (data.image != '' && data.image != 'undefined') {
        this.editProfile.controls['image'].clearValidators();
        this.editProfile.controls['image'].updateValueAndValidity();
        this.hasPicture = true;
        this.imagePreview = data.image;
      }

    }

  }

  termsCondition() {
    localStorage.setItem('user-details', JSON.stringify(this.editProfile.value));
    this.router.navigate(['/terms-and-condition']);
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.editProfile.invalid) {
      this.authService.setLoader(false);
      return;
    } else {
      let process = 's0'
      localStorage.setItem('process', process);
      this.editProfile.value.processStatus = 's0'
      let self = this;
      var formData: any = new FormData();
      for (const key in this.editProfile.value) {
        if (Object.prototype.hasOwnProperty.call(this.editProfile.value, key)) {
          const element = this.editProfile.value[key];

          if (key == 'userRole') {
            formData.append('userRole', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'userRole')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      let id = JSON.parse(localStorage.getItem('user-data') || '{}');
      var endPoint = 'profile-edit/' + id[0].uid
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.Signup;
          this.msg = this.result.content.messageList
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.Signup;
          setTimeout(() => {
            this.getTrialProduct();
          }, 2000);

        }
      })
    }
  }
  
  click() {
    this.sub && this.sub.unsubscribe(); //to unsubscribe if not
    this.sub = this.interval(12000).subscribe(res => {
      this.value = res;
    });
  }

  interval(timeToWait: number) {
    const initial = new Date().getTime();
    return timer(0, 200).pipe(
      map(() => new Date().getTime()),
      takeWhile((res) => res <= initial + timeToWait, true),
      map(now => {
        const porc = (100 * (now - initial)) / timeToWait;
        return porc < 100 ? Math.round(porc) : 100
      })
    );
  }

  getTrialProduct() {
    var endPoint = 'billwerk-threemonth-trialproduct';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
      } else if (this.result.success == true) {
        this.product = this.result.content.dataList;
        this.progress = true;
        this.click()
        this.orderProduct(this.product[0].Id)
      }
    })
  }

  orderProduct(productId: any) {
    let customer = JSON.parse(localStorage.getItem('user-data') || '{}');
    let orderDetails;
    orderDetails =
    {
      "triggerInterimBilling": false,
      "cart": {
        "planVariantId": productId,
        "inheritStartDate": false,
   
      },
      "customer": {
        "companyName": this.editProfile.value.society,
        "firstName": this.editProfile.value.firstName,
        "lastName": this.editProfile.value.lastName,
        "emailAddress": this.editProfile.value.email,
        "address": {
          "street": this.editProfile.value.addressLine1,
          "postalCode": this.editProfile.value.zipcode,
          "city": this.editProfile.value.place,
          "country": 'DE'
        },
        "hidden": false
      },
      "previewAfterTrial": false
    };
    var endPoint = 'billwerk-order';
    this.authService.sendRequest('post', endPoint, orderDetails).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.error = '';
        let order_id = this.result.content.dataList[0].Id;
        this.commit(order_id);

      }
    })
  }

  commit(order_id: any) {
    var payMent = {
    }
    var endPoint = 'billwerk-order/commit/' + order_id;
    this.authService.sendRequest('post', endPoint, payMent).subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
        this.msg = '';
      } else if (this.result.success == true) {
        this.error = '';
        this.msg = this.language.productPrice.productOrderSuccess;
        var trialProductInstalled = '1';
        localStorage.setItem('trialProductInstalled', trialProductInstalled);
        this.router.navigate(['/view-profile']);
      }
    })
  }



}
