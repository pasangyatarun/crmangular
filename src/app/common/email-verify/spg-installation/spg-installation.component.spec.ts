import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpgInstallationComponent } from './spg-installation.component';

describe('SpgInstallationComponent', () => {
  let component: SpgInstallationComponent;
  let fixture: ComponentFixture<SpgInstallationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpgInstallationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpgInstallationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
