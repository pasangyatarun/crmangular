import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
  passwordForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  id: any;
  data: any;
  userId: any;
  language: any;
  product: any;
  value: any = 0;
  sub: any;
  progress: any = false;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.data = this.route.snapshot.paramMap.get('data');
    this.passwordForm = this.formBuilder.group({
      id: this.data,
      password: ["", Validators.compose([Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{5,}')])],
      confirmPassword: ["", Validators.compose([Validators.required])],
    });
    this.language = this.langService.getLanguaageFile();
  }

  get formControls() { return this.passwordForm.controls; }

  get f() {
    return this.passwordForm.controls;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.mustMatch();
  }

  mustMatch() {
    this.passwordForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }

  onSubmit() {
    this.isSubmitted = true;

    if (this.passwordForm.invalid) {
      this.authService.setLoader(false);
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'emailverify'
      this.authService.sendRequest('post', endPoint, this.passwordForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.verify;
          this.msg = this.result.content.messageList
        } else if (this.result.success == true) {
          this.setLanguage();
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.verify;
          var data = this.result.content.dataList[0]['token'];
          localStorage.setItem('permission', JSON.stringify(this.result.content.dataList[0].permission))
          sessionStorage.setItem('token', data);
          localStorage.setItem('token', JSON.stringify(data));
          localStorage.setItem('role', JSON.stringify(this.result.content.dataList[0].role));
          localStorage.setItem('user-data', JSON.stringify(this.result.content.dataList));
          this.userId = this.result.content.dataList[0].uid;
          this.getbillworkToken();
          if (this.id == 'undefined') {
            this.router.navigate(['/dashboard']);
          }
          else {
            this.router.navigate(['/product-details/' + this.id]);
          }
        }
      })
    }
  }

  setLanguage(){
    let lan = localStorage.getItem('language');
    var endPoint = 'language'
    let data = {
      "language":lan
    }
    this.authService.sendRequest('post', endPoint, data).subscribe((result: any) => {
      this.result = result;      
    })
  }

  getbillworkToken() {
    var endPoint = 'billwerk-signin'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
    })
  }

}
