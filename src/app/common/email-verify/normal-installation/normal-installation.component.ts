import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { timer, Observable } from "rxjs";
import { takeWhile, takeUntil, tap, map } from "rxjs/operators";
import { billwerkPaymentMethod } from 'src/environments/environment';
import { mandateText } from 'src/environments/environment';
declare let $: any;

@Component({
  selector: 'app-normal-installation',
  templateUrl: './normal-installation.component.html',
  styleUrls: ['./normal-installation.component.css']
})
export class NormalInstallationComponent implements OnInit {
  editProfile: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  id: any;
  data: any;
  userId: any;
  language: any;
  product: any;
  value: any = 0;
  sub: any;
  progress: any = false;
  params: any;
  userData: any;
  field: any = false;
  productData: any;
  vereinspgTypeProduct: any = [];
  productDetails: any;
  productComponent: any = [];
  role: any;
  imagePreview: any;
  hasPicture = false;
  istrial: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editProfile = this.formBuilder.group({
      email: ["", Validators.compose([Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      firstName: [''],
      lastName: [''],
      society: [''],
      addressLine1: [''],
      zipcode: ["", Validators.compose([Validators.minLength(5), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      place: [""],
      contactNumber: ["", Validators.compose([Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[- +()0-9]+')])],
      country: [''],
      termsCon: ['', Validators.required],
      addressLine2: [''],
      status: [''],
      userRole: new FormArray([]),
      image: [''],
      PlanVariantId: new FormArray([]),
      // auther_id:['']
    });
    this.language = this.langService.getLanguaageFile();
  }

  get formControls() { return this.editProfile.controls; }
  get f() {
    return this.editProfile.controls;
  }
  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.getCustomerData();
    this.getAllProducts();
    this.getProductComponent();

  }

  getAllProducts() {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-normalclub-product'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      let self = this;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.productData = this.result.content.dataList;
        this.productData.forEach((element: any, index: any) => {
          if (element.planDetail && element.planDetail.CustomFields.type == 1) {
            this.vereinspgTypeProduct.push(element);
          }
          if (localStorage.getItem('user-details') != null) {
            let data = JSON.parse(localStorage.getItem('user-details') || '{}');
            if (element.Id == data.PlanVariantId) {
              if (element.AllowWithoutPaymentData == true) {
                this.istrial = true;
              } else {
                this.istrial = false;
              }
              const formArray: FormArray = this.editProfile.get('PlanVariantId') as FormArray;
              formArray.push(new FormControl(element.Id));
              self.productData[index]['r_id'] = element.Id;
            } else {
              self.productData[index]['r_id'] = '';
            }
          }


        });

      }
    })
  }

  getProductComponent() {
    var endPoint = 'billwerk-product-components'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {
          if (element.CustomFields.type == '1') {
            this.productComponent.push(element);
          }
        });
      }
    })
  }


  getproductDetails(productId: any) {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-productbyid/' + productId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.billwerk;
      } else if (this.result.success == true) {
        this.error = false;
        this.msg = this.result.content.messageList.billwerk;
        this.productDetails = this.result.content.dataList;
        if (this.productDetails) {
          $('#productDetails').modal({
            backdrop: 'static'
          });
          $('#productDetails').modal('show');
        }

      }
    })
  }

  termsCondition() {
    localStorage.setItem('user-details', JSON.stringify(this.editProfile.value));
    this.router.navigate(['/terms-and-condition']);
  }




  onSubmit() {
    this.isSubmitted = true;

    if (this.editProfile.invalid) {
      this.authService.setLoader(false);
      return;
    } else {
      let process = 'n0'
      localStorage.setItem('process', process);
      this.editProfile.value.processStatus = 'n0'
      let self = this;
      var formData: any = new FormData();
      for (const key in this.editProfile.value) {
        if (Object.prototype.hasOwnProperty.call(this.editProfile.value, key)) {
          const element = this.editProfile.value[key];

          if (key == 'userRole') {
            formData.append('userRole', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'userRole')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      let id = JSON.parse(localStorage.getItem('user-data') || '{}');
      var endPoint = 'profile-edit/' + id[0].uid
      this.authService.sendRequest('post', endPoint, this.editProfile.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.Signup;
          this.msg = this.result.content.messageList
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.Signup;

          if (this.istrial == true) {

            setTimeout(() => {
              this.getTrialProduct();
            }, 2000);
          } else {
            setTimeout(() => {
              this.router.navigate(['product-details/' + this.editProfile.value.PlanVariantId])
            }, 2000);
          }

        }
      })
    }
  }

  getCustomerData() {
    let userId = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'userbyid/' + userId[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.userData = this.result.content.dataList;
        this.setValue(this.userData[0]);
      }
    })
  }

  onradioChange(event: any, istrial: any) {
    this.istrial = istrial;

    if (this.istrial == true) {
      this.editProfile.controls['email'].setValidators([Validators.required]);
      this.editProfile.controls['email'].updateValueAndValidity();

      this.editProfile.controls['society'].setValidators([Validators.required]);
      this.editProfile.controls['society'].updateValueAndValidity();

      this.editProfile.controls["firstName"].clearValidators();
      this.editProfile.controls["firstName"].updateValueAndValidity();

      this.editProfile.controls["lastName"].clearValidators();
      this.editProfile.controls["lastName"].updateValueAndValidity();

      this.editProfile.controls["addressLine1"].clearValidators();
      this.editProfile.controls["addressLine1"].updateValueAndValidity();

      this.editProfile.controls["zipcode"].clearValidators();
      this.editProfile.controls["zipcode"].updateValueAndValidity();

      this.editProfile.controls["place"].clearValidators();
      this.editProfile.controls["place"].updateValueAndValidity();

      this.editProfile.controls["country"].clearValidators();
      this.editProfile.controls["country"].updateValueAndValidity();

      this.editProfile.controls["contactNumber"].clearValidators();
      this.editProfile.controls["contactNumber"].updateValueAndValidity();

    } else {

      this.editProfile.controls['email'].setValidators([Validators.required]);
      this.editProfile.controls['email'].updateValueAndValidity();

      this.editProfile.controls['society'].setValidators([Validators.required]);
      this.editProfile.controls['society'].updateValueAndValidity();

      this.editProfile.controls["firstName"].setValidators([Validators.required]);
      this.editProfile.controls["firstName"].updateValueAndValidity();

      this.editProfile.controls["lastName"].setValidators([Validators.required]);
      this.editProfile.controls["lastName"].updateValueAndValidity();

      this.editProfile.controls["addressLine1"].setValidators([Validators.required]);
      this.editProfile.controls["addressLine1"].updateValueAndValidity();

      this.editProfile.controls["zipcode"].setValidators([Validators.required]);
      this.editProfile.controls["zipcode"].updateValueAndValidity();

      this.editProfile.controls["place"].setValidators([Validators.required]);
      this.editProfile.controls["place"].updateValueAndValidity();

      this.editProfile.controls["country"].setValidators([Validators.required]);
      this.editProfile.controls["country"].updateValueAndValidity();

      this.editProfile.controls["contactNumber"].setValidators([Validators.required]);
      this.editProfile.controls["contactNumber"].updateValueAndValidity();

    }
    const formArray: FormArray = this.editProfile.get('PlanVariantId') as FormArray;

    /* Selected */
    if (event.target.checked) {
      let i: number = 0;
      formArray.removeAt(i);
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }

  }

  setValue(data: any) {

    if (localStorage.getItem('user-details') != null) {
      data = JSON.parse(localStorage.getItem('user-details') || '{}');
    }

    if (data && data.mail) {
      this.editProfile.controls["email"].setValue(data.mail);

    } else {
      this.editProfile.controls["email"].setValue(data.email);

    }

    this.editProfile.controls["firstName"].setValue(data.firstName);
    this.editProfile.controls["lastName"].setValue(data.lastName);
    this.editProfile.controls["society"].setValue(data.society);
    this.editProfile.controls["addressLine1"].setValue(data.addressLine1);
    this.editProfile.controls["zipcode"].setValue(data.zipcode);
    this.editProfile.controls["place"].setValue(data.place);
    this.editProfile.controls["country"].setValue(data.country);
    this.editProfile.controls["contactNumber"].setValue(data.contactNumber);
    this.editProfile.controls["addressLine2"].setValue(data.addressLine2);
    this.editProfile.controls["status"].setValue(data.status);
    // this.editProfile.controls["auther_id"].setValue(data.auther_id);
    if (data && data.role) {

      data.role.forEach((element: any) => {
        const formArray: FormArray = this.editProfile.get('userRole') as FormArray;
        formArray.push(new FormControl(element.rid));
      });
    }


    if (data && data.userRole) {
      data.userRole.forEach((element: any) => {
        const formArray: FormArray = this.editProfile.get('userRole') as FormArray;
        formArray.push(new FormControl(element));
      });
    }

    if (data && data.termsCon) {
      this.editProfile.controls["termsCon"].setValue(true);
    }

    if (data.image != '' && data.image != 'undefined') {
      this.editProfile.controls['image'].clearValidators();
      this.editProfile.controls['image'].updateValueAndValidity();
      this.hasPicture = true;
      this.imagePreview = data.image;
    }

  }

  click() {
    this.sub && this.sub.unsubscribe(); //to unsubscribe if not
    this.sub = this.interval(12000).subscribe(res => {
      this.value = res;
    });
  }

  interval(timeToWait: number) {
    const initial = new Date().getTime();
    return timer(0, 200).pipe(
      map(() => new Date().getTime()),
      takeWhile((res) => res <= initial + timeToWait, true),
      map(now => {
        const porc = (100 * (now - initial)) / timeToWait;
        return porc < 100 ? Math.round(porc) : 100
      })
    );
  }

  getTrialProduct() {
    var endPoint = 'billwerk-trialproduct';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
      } else if (this.result.success == true) {
        this.product = this.result.content.dataList;
        this.progress = true;
        this.click()
        this.orderProduct(this.product[0].Id)
      }
    })
  }

  orderProduct(productId: any) {
    let customer = JSON.parse(localStorage.getItem('user-data') || '{}');
    let orderDetails;
    orderDetails =
    {
      "triggerInterimBilling": false,
      "cart": {
        "planVariantId": productId,

        "inheritStartDate": false
      },
      "customer": {
        "companyName": customer[0].society,
        "firstName": '',
        "lastName": '',
        "emailAddress": customer[0].email,
        "address": {
          "street": '',
          "postalCode": '',
          "city": '',
          "country": 'DE'
        },
        "hidden": false
      },
      "previewAfterTrial": false
    };
    var endPoint = 'billwerk-order';
    this.authService.sendRequest('post', endPoint, orderDetails).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.error = '';
        let order_id = this.result.content.dataList[0].Id;
        this.commit(order_id);
      }
    })
  }

  commit(order_id: any) {
    var payMent = {
    }
    var endPoint = 'billwerk-order/commit/' + order_id;
    this.authService.sendRequest('post', endPoint, payMent).subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
        this.msg = '';
      } else if (this.result.success == true) {
        this.error = '';
        this.msg = this.language.productPrice.productOrderSuccess;
        var trialProductInstalled = '1';
        localStorage.setItem('trialProductInstalled', trialProductInstalled);
        this.router.navigate(['/view-profile']);

      }
    })
  }

}
