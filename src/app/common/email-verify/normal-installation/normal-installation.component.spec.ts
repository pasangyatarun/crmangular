import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalInstallationComponent } from './normal-installation.component';

describe('NormalInstallationComponent', () => {
  let component: NormalInstallationComponent;
  let fixture: ComponentFixture<NormalInstallationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NormalInstallationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalInstallationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
