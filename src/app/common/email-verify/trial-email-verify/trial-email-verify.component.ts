import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { timer, Observable } from "rxjs";
import { takeWhile, takeUntil, tap, map } from "rxjs/operators";

@Component({
  selector: 'app-trial-email-verify',
  templateUrl: './trial-email-verify.component.html',
  styleUrls: ['./trial-email-verify.component.css']
})
export class TrialEmailVerifyComponent implements OnInit {
  passwordForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  id: any;
  userId: any;
  language: any;
  product: any;
  value: any = 0;
  sub: any;
  progress: any = false;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {

    this.id = this.route.snapshot.paramMap.get('id');
    this.passwordForm = this.formBuilder.group({
      id: this.id,
      password: ["", Validators.compose([Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{5,}')])],
      confirmPassword: ["", Validators.compose([Validators.required])],
    });
    this.language = this.langService.getLanguaageFile();
  }

  get formControls() { return this.passwordForm.controls; }

  get f() {
    return this.passwordForm.controls;
  }

  ngOnInit(): void {
    this.mustMatch();
  }

  mustMatch() {
    this.passwordForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }

  onSubmit() {
    this.isSubmitted = true;

    if (this.passwordForm.invalid) {
      this.authService.setLoader(false);
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'emailverify'
      this.authService.sendRequest('post', endPoint, this.passwordForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);

        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.verify;
          this.msg = this.result.content.messageList
        } else if (this.result.success == true) {
          this.setLanguage();
          var data = this.result.content.dataList[0]['token'];
          localStorage.setItem('permission', JSON.stringify(this.result.content.dataList[0].permission))
          sessionStorage.setItem('token', data);
          localStorage.setItem('token', JSON.stringify(data));
          localStorage.setItem('role', JSON.stringify(this.result.content.dataList[0].role));
          localStorage.setItem('user-data', JSON.stringify(this.result.content.dataList));
          this.userId = this.result.content.dataList[0].uid;
          var trialProductInstalled = '1';
          localStorage.setItem('trialProductInstalled', trialProductInstalled);

          this.getbillworkToken();

        }
      })
    }
  }

  setLanguage() {
    let lan = localStorage.getItem('language');
    var endPoint = 'language'
    let data = {
      "language": lan
    }
    this.authService.sendRequest('post', endPoint, data).subscribe((result: any) => {
      this.result = result;
    })
  }

  click() {
    this.sub && this.sub.unsubscribe(); //to unsubscribe if not
    this.sub = this.interval(12000).subscribe(res => {
      this.value = res;
    });
  }
  interval(timeToWait: number) {
    const initial = new Date().getTime();
    return timer(0, 200).pipe(
      map(() => new Date().getTime()),
      takeWhile((res) => res <= initial + timeToWait, true),
      map(now => {
        const porc = (100 * (now - initial)) / timeToWait;
        return porc < 100 ? Math.round(porc) : 100
      })
    );
  }

  getbillworkToken() {
    var endPoint = 'billwerk-signin'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.progress = true;
      this.getTrialProduct();
      this.click()
    })
  }

  getTrialProduct() {
    var endPoint = 'billwerk-trialproduct';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
      } else if (this.result.success == true) {
        this.product = this.result.content.dataList;
        this.orderProduct(this.product[0].Id)
      }
    })
  }

  orderProduct(productId: any) {
    let customer = JSON.parse(localStorage.getItem('user-data') || '{}');
    let orderDetails;
    orderDetails =
    {
      "triggerInterimBilling": false,
      "cart": {
        "planVariantId": productId,

        "inheritStartDate": false
      },
      "customer": {
        "companyName": customer[0].society,
        "firstName": '',
        "lastName": '',
        "emailAddress": customer[0].email,
        "address": {
          "street": '',
          "postalCode": '',
          "city": '',
          "country": 'DE'
        },
        "hidden": false
      },
      "previewAfterTrial": false
    };
    var endPoint = 'billwerk-order';
    this.authService.sendRequest('post', endPoint, orderDetails).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.error = '';
        let order_id = this.result.content.dataList[0].Id;
        this.commit(order_id)

      }
    })
  }

  commit(order_id: any) {
    var payMent = {
    }
    var endPoint = 'billwerk-order/commit/' + order_id;
    this.authService.sendRequest('post', endPoint, payMent).subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
        this.msg = '';
      } else if (this.result.success == true) {
        this.error = '';
        this.msg = this.language.productPrice.productOrderSuccess;
        this.router.navigate(['/edit-profile/' + this.userId]);

      }
    })
  }


}
