import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrialEmailVerifyComponent } from './trial-email-verify.component';

describe('TrialEmailVerifyComponent', () => {
  let component: TrialEmailVerifyComponent;
  let fixture: ComponentFixture<TrialEmailVerifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrialEmailVerifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrialEmailVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
