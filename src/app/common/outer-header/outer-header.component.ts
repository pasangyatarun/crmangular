import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-outer-header',
  templateUrl: './outer-header.component.html',
  styleUrls: ['./outer-header.component.css']
})
export class OuterHeaderComponent implements OnInit {
  userData: any;
  language: any;
  result: any;
  error: any;
  productDetails: any;
  productData: any
  msg: any;
  visibility: any = 0;
  customer: any;
  userOrderList: any;
  productId: any;
  isSubmitted: any = false;
  orderId: any;
  pdfButton: any = false;
  field: any = false;
  displayFlag: any = 'en';
  process: any;
  constructor(private formBuilder: FormBuilder, private router: Router, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.productId = this.route.snapshot.paramMap.get('productId');
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.displayFlag = localStorage.getItem('language');
  }


  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    var endPoint = 'language'
    let data = {
      "language": lan
    }
    this.authService.sendRequest('post', endPoint, data).subscribe((result: any) => {
      this.result = result;
      window.location.reload();
    })
  }

  gotoPage(page: any) {
    this.process = localStorage.getItem('process') || '{}'

    if (this.authService.IsLoggedIn()) {
      if (this.process == 'n1') {
        this.router.navigate(['/profile-edit']);
      } else {
        this.router.navigate(['/profile-edit-spg']);
      }
    } else {
      if (page == 'privacy' || page == 'imprint' || page == 'conditions') {
        this.router.navigateByUrl('/content/' + page);
      } else if (page == 'all-news' || page == 'products-prices') {
        this.router.navigate(['/' + page]);
      } else {

        window.location.href = page
      }
    }

  }

  activePage(id: any) {
    this.process = localStorage.getItem('process') || '{}'
    if (!this.authService.IsLoggedIn() && (this.process != 'n1' || this.process != 's1')) {
      $('.active').removeClass('active')
      $('#' + id).addClass('active');
    }

  }

}
