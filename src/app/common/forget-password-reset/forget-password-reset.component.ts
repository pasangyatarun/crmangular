import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-forget-password-reset',
  templateUrl: './forget-password-reset.component.html',
  styleUrls: ['./forget-password-reset.component.css']
})
export class ForgetPasswordResetComponent implements OnInit {
  passwordForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  id: any;
  language: any;
  error: any = false;
  success: any = false;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.passwordForm = this.formBuilder.group({
      id: this.id,
      password: ["", Validators.compose([Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{5,}')])],
      confirmPassword: ["", Validators.compose([Validators.required])],
    });

    this.language = this.langService.getLanguaageFile();

  }

  get formControls() { return this.passwordForm.controls; }

  get f() {
    return this.passwordForm.controls;
  }

 

  ngOnInit(): void {
   this. mustMatch();
  }

  mustMatch() {
    this.passwordForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        return this.show = false;
      } else {
        return this.show = true;
      }
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.passwordForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'resetpassword'
      this.authService.sendRequest('post', endPoint, this.passwordForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.reset;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.reset;
          setTimeout(() => {
            this.router.navigate(['/signin']);
          }, 2000);
        }
      })
    }

  }

}
