import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { LanguageService } from '../../service/language.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
  language: any;

  constructor(private auth: AuthService, private langService: LanguageService) {

  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();

  }

}
