import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { LanguageService } from '../../service/language.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmitted: any = false;
  language: any;
  message: any;
  result: any;
  error: any = false;
  success: any = false;
  displayFlag: any = 'en';
  params: any;
  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {

    this.loginForm = this.formBuilder.group({
      // email:["", Validators.compose([Validators.required, Validators.email])] ,
      // password:["", Validators.compose([Validators.required,Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{5,}')])] ,
      email: ["", Validators.required],
      password: ["", Validators.required],
    });


  }

  get formControls() { return this.loginForm.controls; }

  get f() {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
    this.authService.setLoader(false);
    this.language = this.langService.getLanguaageFile();
    if (this.authService.IsLoggedIn()) {
      this.router.navigate(['/view-profile'])
    }
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.displayFlag = localStorage.getItem('language');
    this.route.queryParams.subscribe(params => {
      this.params = params
      this.loginForm.controls['email'].setValue(this.params.email);
    });

  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'signin'
      this.authService.sendRequest('post', endPoint, this.loginForm.value).subscribe((result: any) => {
        this.authService.setLoader(false);
        this.result = result;
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.Signin;
        } else if (this.result.success == true) {
          this.setLanguage();
          var data = this.result.content.dataList[0]['token'];          
          localStorage.setItem('permission', JSON.stringify(this.result.content.dataList[0].permission));
          sessionStorage.setItem('token', data);
          localStorage.setItem('token', JSON.stringify(data));
          localStorage.setItem('role', JSON.stringify(this.result.content.dataList[0].role));
          localStorage.setItem('user-data', JSON.stringify(this.result.content.dataList));
          var customerEditStatus = '1';
          // let process = 's0'
          // localStorage.setItem('process', process);
          localStorage.setItem('process', this.result.content.dataList[0].processStatus);
          localStorage.setItem('customerEditStatus', customerEditStatus);
          this.getbillworkToken();
          if (this.params && this.params.product) {
            var productId = this.params.product;
            this.router.navigate(['/product-details/' + productId]);
          } else if (this.params && !this.params.product) {
            this.router.navigate(['/dashboard']);
          }
        }
      })
    }
  }

  setLanguage() {
    let lan = localStorage.getItem('language');
    var endPoint = 'language'
    let data = {
      "language": lan
    }
    this.authService.sendRequest('post', endPoint, data).subscribe((result: any) => {
      this.result = result;
    })
  }

  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    window.location.reload();
  }

  getbillworkToken() {
    var endPoint = 'billwerk-signin'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
    })
  }



}
