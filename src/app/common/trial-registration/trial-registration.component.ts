import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { LanguageService } from '../../service/language.service';
import { CustomEmailValidators } from 'src/app/providers/customEmailValidator';

@Component({
  selector: 'app-trial-registration',
  templateUrl: './trial-registration.component.html',
  styleUrls: ['./trial-registration.component.css']
})
export class TrialRegistrationComponent implements OnInit {
  trialregisterForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  languages: any;
  msg: any;
  displayFlag: any = 'en';

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private lang: LanguageService) {
    this.trialregisterForm = this.formBuilder.group({
      society: ['', Validators.required],
      termsCon: ['', Validators.required],
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      conEmail: ["",Validators.required],
    },
    {
      validators: [CustomEmailValidators.mustMatch('email', 'conEmail')],
    });
  }
  
  
  get formControls() { return this.trialregisterForm.controls; }
  
  get f() {
    return this.trialregisterForm.controls;
  }
  
  ngOnInit(): void {
    this.languages = this.lang.getLanguaageFile()
    // this.mustMatch();
    this. setValue();
  }

  mustMatch() {
    this.trialregisterForm.valueChanges.subscribe(data => {
      if (data.email == data.conEmail) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }

  setValue(){
    let data: any
    data = JSON.parse(localStorage.getItem('registration') || '{}');
    this.trialregisterForm.controls["email"].setValue(data.email);
    this.trialregisterForm.controls["conEmail"].setValue(data.conEmail);
    this.trialregisterForm.controls["society"].setValue(data.society);
    if(data.termsCon == true){
      this.trialregisterForm.controls["termsCon"].setValue(data.termsCon);
    }
  }

  onSubmit() {
    this.isSubmitted = true;

    if (this.trialregisterForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'trial-register'
      this.authService.sendRequest('post', endPoint, this.trialregisterForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.Signup;
          this.msg = this.result.content.messageList
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.Signup;
          this.isSubmitted = false;
          localStorage.removeItem('registration');
          this.trialregisterForm.reset();
        }

      })
    }

  }

  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    window.location.reload();
  }

  termsCondition(){
    localStorage.setItem('registration', JSON.stringify(this.trialregisterForm.value));
    this.router.navigate(['/terms-and-condition']);
  }

}
