import { Component, Inject, OnInit } from '@angular/core';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA
} from "@angular/material/snack-bar";
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.css']
})
export class ErrorHandlerComponent implements OnInit {
  language: any;

  constructor(   
     public sbRef: MatSnackBarRef<ErrorHandlerComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any , private langService: LanguageService) { }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
  }

}
