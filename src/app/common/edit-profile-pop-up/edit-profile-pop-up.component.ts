import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { billwerkPaymentMethod } from 'src/environments/environment';

declare let $: any;

@Component({
  selector: 'app-edit-profile-pop-up',
  templateUrl: './edit-profile-pop-up.component.html',
  styleUrls: ['./edit-profile-pop-up.component.css']
})
export class EditProfilePopUpComponent implements OnInit {

  editUserForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  errorMsg: any;
  show: any = false;
  file: any;
  id: any;
  reEnterData: any;
  language: any;
  error: any = false;
  success: any = false;
  domain: any;
  imageSrc: any;
  hasPicture = false;
  imagePreview: any;
  user: any;
  customer_id: any;
  displayMsg: any = false;
  @Input() orderData: any;
  @Input() paymentData: any;
  clubOrderError: any;
  commitData: any;
  messagePurchase: any;
  isValidIban!: string;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editUserForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      society: ['', Validators.required],
      addressLine1: ['', Validators.required],
      zipcode: ["", Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      place: ["", Validators.compose([Validators.required, Validators.required])],
      accountNumber: ["", Validators.compose([Validators.pattern('[0-9]+')])],
      contactNumber: ["", Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[- +()0-9]+')])],
      country: ['', Validators.required],
      termsCon: ['', Validators.required],
      bankCode: [''],
      addressLine2: [''],
      status: [''],
      userRole: new FormArray([]),
      image: ['',],
      iban: [''],
      // auther_id:['']
      
    });

    if (this.authService.IsLoggedIn()) {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}')
      this.id = id[0].uid;
    } else {
      this.id = this.route.snapshot.paramMap.get('id');
    }
  }

  get formControls() { return this.editUserForm.controls; }

  get f() {
    return this.editUserForm.controls;
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getUserData();
  }

  getUserData() {
    if (localStorage.getItem('token') !== null) {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}')
      var endPoint = 'userbyid/' + id[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result;
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.reEnterData = this.result.content.dataList;
          this.reEnterValue();
        }
      })
    }
  }

  reEnterValue() {
    this.editUserForm.controls["email"].setValue(this.reEnterData[0].mail);
    this.editUserForm.controls["firstName"].setValue(this.reEnterData[0].firstName);
    this.editUserForm.controls["lastName"].setValue(this.reEnterData[0].lastName);
    this.editUserForm.controls["society"].setValue(this.reEnterData[0].society);
    this.editUserForm.controls["addressLine1"].setValue(this.reEnterData[0].addressLine1);
    this.editUserForm.controls["zipcode"].setValue(this.reEnterData[0].zipcode);
    this.editUserForm.controls["place"].setValue(this.reEnterData[0].place);
    this.editUserForm.controls["accountNumber"].setValue(this.reEnterData[0].accountNumber);
    this.editUserForm.controls["country"].setValue(this.reEnterData[0].country);
    this.editUserForm.controls["contactNumber"].setValue(this.reEnterData[0].contactNumber);
    this.editUserForm.controls["addressLine2"].setValue(this.reEnterData[0].addressLine2);
    this.editUserForm.controls["bankCode"].setValue(this.reEnterData[0].bankCode);
    this.editUserForm.controls["status"].setValue(this.reEnterData[0].status);
    this.editUserForm.controls["iban"].setValue(this.paymentData.iban);
    // this.editUserForm.controls["auther_id"].setValue(this.reEnterData[0].auther_id);

    this.reEnterData[0].role.forEach((element: any) => {
      const formArray: FormArray = this.editUserForm.get('userRole') as FormArray;
      formArray.push(new FormControl(element.rid));
    });
    this.editUserForm.controls["termsCon"].setValue(true);
    if (this.reEnterData[0].image != '' && this.reEnterData[0].image != 'undefined') {
      this.editUserForm.controls['image'].clearValidators();
      this.editUserForm.controls['image'].updateValueAndValidity();
      this.hasPicture = true;
      this.imagePreview = this.reEnterData[0].image;
    }

  }

  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
      this.editUserForm.patchValue({
        image: file
      });
      this.editUserForm.controls["image"].updateValueAndValidity();
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }


  validIban(event: any) {
    let validateIban = {
      iban: event.target.value
    }
    var endPoint = 'validate-iban';
    this.authService.sendRequest('post', endPoint, validateIban).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.isValidIban = this.result.content.messageList.iban;
      } else if (this.result.success == true) {
        this.isValidIban = '';
      }
    })
  }

  onSubmit() {
    this.isSubmitted = true;

    if(this.editUserForm.value.iban != (this.paymentData && this.paymentData.iban)){
          this.paymentData.iban = this.editUserForm.value.iban          
      } 

    if (this.editUserForm.invalid) {
      return;
    } else {
      var formData: any = new FormData();
      // this.authService.setLoader(true);
      this.editUserForm.value.image = this.imageSrc;
      let self = this;
      for (const key in this.editUserForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.editUserForm.value, key)) {
          const element = this.editUserForm.value[key];

          if (key == 'image') {
            if (self.imageSrc) {
              formData.append('file', self.imageSrc);
            } else {
              formData.append('imageUrl', self.imagePreview);
            }
          }
          if (key == 'userRole') {
            formData.append('userRole', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'userRole')) {
              formData.append(key, element);
            }
          }
        }
      }

      this.editUserForm.value.image = this.file
      var endPoint = 'profile-edit/' + this.id;
      this.authService.setLoader(true);
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          if (this.msg == null || this.msg == "") {
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.Signup;
          }
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.Signup;
          this.getOrderProductData();
        }
      })

    }
  }



  getOrderProductData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'billwerk-orders/bycustomer/' + id[0].uid;
    this.authService.setLoader(true);
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.authService.setLoader(false);
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.customer_id = this.result.content.dataList[0].CustomerId;
        if (this.customer_id) {
          this.getUpdateUserData();
        }

      }
    })
  }

  getUpdateUserData() {
    if (localStorage.getItem('token') !== null) {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}')
      var endPoint = 'userbyid/' + id[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result;
        if (this.result.success == false) {
          this.authService.setLoader(false);

          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.user;
        } else if (this.result.success == true) {
          this.user = this.result.content.dataList
          this.updateBillwerkUser(this.user);
        }
      })
    }
  }

  updateBillwerkUser(user: any) {
    let updateCustomer =
    {
      "companyName": user[0].society,
      "firstName": user[0].firstName,
      "lastName": user[0].lastName,
      "emailAddress": user[0].mail,

      "address": {
        "street": user[0].addressLine1,
        "postalCode": user[0].zipcode,
        "city": user[0].place,
        "country": "DE"
      },
      "defaultBearerMedium": "Email",
      "hidden": false
    };
    var endPoint = 'billwerk-customer-update/' + this.customer_id;
    this.authService.sendRequest('put', endPoint, updateCustomer).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.authService.setLoader(false);
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.orderProduct();
      }
    })
  }

  orderProduct() {
    this.success = false;
    this.message = ''
    var endPoint = 'billwerk-order';
    this.authService.sendRequest('post', endPoint, this.orderData).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.authService.setLoader(false);
        this.error = true;
        this.success = false;
        if (this.result.content.messageList.order == 'To purchase the sub-products, please order the Club-Administration product first.') {
          this.messagePurchase = this.result.content.messageList.order;
        } else {
          this.message = this.result.content.messageList.order;
        }
      } else if (this.result.success == true) {
        this.error = false;
        let order_id = this.result.content.dataList[0].Id;
        this.commit(order_id)

      }
    })
  }

  commit(order_id: any) {
    var payMent = {
      paymentMethod: billwerkPaymentMethod,
      bearer: {
        holder: this.paymentData.holder,
        iban: this.paymentData.iban,
        mandateText: this.paymentData.mandateText
      }
    }
    var endPoint = 'billwerk-order/commit/' + order_id;
    this.authService.sendRequest('post', endPoint, payMent).subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.error = false;
        this.commitData = this.result.content.dataList;
        this.authService.setCounter(true);
        this.displayMsg = true;

      }
    })
  }

  proceed() {
    $('#editProfile').modal('hide');
    this.authService.setpopUp(false);
    this.router.navigate(['/order-product-list'])
  }

  closeModal() {
    $('#editProfile').modal('hide');
    this.authService.setpopUp(false);
  }

  redirect() {
    $('#editProfile').modal('hide');
    this.authService.setpopUp(false);
    this.router.navigate(['/product-price'])
  }

}
