import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from 'src/app/service/auth.service';
import { LanguageService } from 'src/app/service/language.service';
@Component({
  selector: 'app-clubs',
  templateUrl: './clubs.component.html',
  styleUrls: ['./clubs.component.css']
})
export class ClubsComponent implements OnInit {

  userData: any = [];
  displayedColumns: string[] = [
    'Email',
    'Club-Name',
    'Status',
    'Membar-Since',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource !: MatTableDataSource<any>;
  error: any = false;
  result: any;
  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator) matpaginator!: MatPaginator;
  @ViewChild(MatSort) matsort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  isData: any = true;

  constructor(public authService: AuthService, private langService: LanguageService,
    private formBuilder: FormBuilder) {
  }


  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getuserData();
  }

  getuserData() {
    this.authService.setLoader(true);
    var endPoint = 'clubsCustomer'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.isData = false;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {
          element.status = element.status == '1' ? this.language.viewUser.active : this.language.viewUser.locked
        });
        this.userData = this.result.content.dataList;
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;
        }
        this.dataSource.paginator = this.matpaginator;
        this.dataSource.sort = this.matsort;
      }
    })

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


}
