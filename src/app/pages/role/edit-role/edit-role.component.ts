import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import {ConfirmDialogService} from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.css']
})
export class EditRoleComponent implements OnInit {

  editRoleForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  id: any;
  reEnterData: any;
  role: any;
  roleData: any;
  error: any = false;
  success: any = false;
  language: any;
  permissions: any;
  roleCategoryList: any = [];
  roleCategoryItems: any = [];
  roleCategorySettings: any;
  roleCategoryData: any;
  roleCate: any = [];

  constructor(private confirmBoxService:ConfirmDialogService, private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editRoleForm = this.formBuilder.group({
      cat_id: ['', Validators.required],
      role: [''],
      description: ['', [Validators.maxLength(50), Validators.minLength(32)]]
    });

  }
  
  
  get formControls() { return this.editRoleForm.controls; }
  
  get f() {
    return this.editRoleForm.controls;
  }
  
  
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getRollCategories();
    this.getRole();
    
  }

  getRollCategories() {
    let self = this;
    var endPoint = 'roleCategory';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.roleCategoryData = this.result.content.dataList;                 
        if(this.roleCategoryData && this.roleCategoryData.length){
          this.result.content.dataList.forEach((val: any, key: any) => {
            this.roleCategoryList.push({ 'id': val.id, 'name': val.category_name });
          });
          self.roleCategorySettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'name',
          };
        }
      }
    })
  }

  roleCategorySelect(items: any) {    
    this.roleCategoryItems = items.id ;
  }

  roleCategoryDeSelect(item: any) {

    const index = this.roleCategoryItems.indexOf(item.name);
    if (index > -2 || -1) {
      this.roleCategoryItems.splice(index, 1);
    }
  }

  onSubmit() {
    this.editRoleForm.value.cat_id =  this.roleCategoryItems;
    this.isSubmitted = true;
    if (this.editRoleForm.invalid) {
      return;
    } else {      
      this.authService.setLoader(true);
      var endPoint = 'roleupdate/' + this.id
      this.authService.sendRequest('put', endPoint, this.editRoleForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.role;
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.role;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.role;
          setTimeout(() => {
            this.router.navigate(['/roles']);
          }, 3000)
        }
      })
    }
  }


  getRole() {
    if (localStorage.getItem('token') !== null) {
      var endPoint = 'rolebyid/' + this.id
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result;        
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.reEnterData = this.result.content.dataList;
          this.reEnterValue();
        }
      })
    }
  }

  reEnterValue() {
    this.editRoleForm.controls["role"].setValue(this.reEnterData[0].name);
    this.editRoleForm.controls["description"].setValue(this.reEnterData[0].description);


    if(this.roleCategoryData && this.roleCategoryData.length ){
      this.roleCategoryData.forEach((ele: any) => {
  
        if (ele.id == this.reEnterData[0].cat_id) {
          this.roleCategoryItems = ele.id;
          this.roleCate.push({'id':ele.id,'name':ele.category_name});            
          this.editRoleForm.controls["cat_id"].setValue(this.roleCate);
        }
      });
    }
  }

  deleteRole(){
    let self = this;
    self.confirmBoxService.confirmThis(this.language.role.deleteRole,this.language.confirmBox.delete_role, function () {

      self.authService.setLoader(true);
      var endPoint = 'roledelete/' + self.id
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.role;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.result.content.messageList.role;
          setTimeout(() => {
            self.router.navigate(['/roles']);
          }, 3000)
        }
      })
     

    }, function () { }
    )

}

}
