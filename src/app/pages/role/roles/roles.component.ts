import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  fruit: string;
}
@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})


export class RolesComponent implements OnInit {

  roleForm: FormGroup;
  isSubmitted: boolean = false;
  message: any;
  result: any;
  error: boolean = false;
  success: boolean = false;
  show: boolean = false;
  language: any;
  nameSearch: any;
  displayedColumns: string[] = [
    'Name',
    'Role Description',
    'role-category',
    'Edit',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  msg: any;
  permissions: any;
  role: any;
  userData: any;
  isData:any = true ;
  roleCategoryList: any = [];
  roleCategoryItems: any = [];
  roleCategorySettings: any;
  roleCategoryData: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.roleForm = this.formBuilder.group({
      cat_id: ['', Validators.required],
      role: ['', Validators.required],
      description: ['', [Validators.maxLength(50), Validators.minLength(32)]]
    });
  }

  get formControls() { return this.roleForm.controls; }


  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getRollCategories();
    this.getRolelist();
    this.getuserData();
  }

  getRollCategories() {
    let self = this;
    var endPoint = 'roleCategory';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.roleCategoryData = this.result.content.dataList;        
        if(this.roleCategoryData && this.roleCategoryData.length){
          this.result.content.dataList.forEach((val: any, key: any) => {
            this.roleCategoryList.push({ 'id': val.id, 'name': val.category_name });
          });
          self.roleCategorySettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'name',
          };
        }
      }
    })
  }

  roleCategorySelect(items: any) {    
    this.roleCategoryItems = items.id ;
  }

  roleCategoryDeSelect(item: any) {

    const index = this.roleCategoryItems.indexOf(item.name);
    if (index > -2 || -1) {
      this.roleCategoryItems.splice(index, 1);
    }
  }

  onSubmit() {
    this.roleForm.value.cat_id =  this.roleCategoryItems
    this.isSubmitted = true;
    if (this.roleForm.invalid) {
      return;
    } else {
      
      this.authService.setLoader(true);
      var endPoint = 'rolecreate'
      this.authService.sendRequest('post', endPoint, this.roleForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          if (this.msg == null || this.msg == "") {
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.role;
          }
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.role;
          setTimeout(() => {
            this.success = false;
            this.message = '';
            this.isSubmitted = false;
            this.getRolelist();
          }, 3000)
        }
      })
    }
  }

  getRolelist() {
    var endPoint = 'roles'
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.role;
        this.isData = false;
      } else if (this.result.success == true) {
        
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if(this.dataSource.data.length){
          this.isData = true;
        }else{
          this.isData = false;

        }
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    })
  }

  getuserData() {
    this.authService.setLoader(true);
    var endPoint = 'users'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.userData = this.result.content.dataList

      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort){
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  
  removeColumn() {
    if (this.role[0].name != 'Administrator' && this.permissions.role && !this.permissions.role.includes('EDIT')) {
      this.columnsToDisplay.pop();
    }
  }

}
