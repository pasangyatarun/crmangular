import { Component, ElementRef, OnInit, } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import { ActivatedRoute } from '@angular/router';
import { billwerkPaymentMethod } from 'src/environments/environment';
import { mandateText } from 'src/environments/environment';
import { LoginComponent } from 'src/app/common/login/login.component';
declare let $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  userData: any;
  language: any;
  result: any;
  error: any;
  productDetails: any;
  productData: any
  msg: any;
  visibility: any = 0;
  customer: any;
  userOrderList: any;
  productId: any;
  paymentForm: FormGroup;
  isSubmitted: any = false;
  orderId: any;
  pdfButton: any = false;
  field: any = false;
  type: any;
  editProfile: any;
  commitData: any;
  clubOrderError: any;
  errorMessage: any = false;
  successMessage: any = false;
  message: any;
  productComponent: any = [];
  orderProductData: any;
  orderData: any;
  paymentData: any;
  mandateFiledDisplay: any = false;
  role: any;
  isValidIban: any;
  editUserForm: FormGroup;
  imagePreview!: string;
  paymentBearer: any;

  constructor(private formBuilder: FormBuilder, private router: Router, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {

    this.paymentForm = this.formBuilder.group({
      holder: ['', Validators.required],
      association: ['', Validators.required],
      iban: ['', Validators.required],
      termsCon: ['', Validators.required],
      mandateText: [''],
      chairManName: ['', Validators.required],
      coupon: [''],
      mandate: ['', Validators.required],
      PlanVariantId: [''],
    });

    this.editUserForm = this.formBuilder.group({
      email: [""],
      firstName: [''],
      lastName: [''],
      society: [''],
      addressLine1: [''],
      zipcode: [""],
      place: [""],
      accountNumber: [""],
      contactNumber: [""],
      country: [''],
      termsCon: [''],
      bankCode: [''],
      iban: [''],
      addressLine2: [''],
      status: [''],
      userRole: new FormArray([]),
      image: [''],
    });

  }

  get formControls() { return this.paymentForm.controls; }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.productId = this.route.snapshot.paramMap.get('productId');
    this.paymentForm.controls["PlanVariantId"].setValue(this.productId);
    this.getproductDetails();
    this.getCustomersDetails();
    this.getOrderData();
  }

  getproductDetails() {
    this.visibility = 1;
    this.authService.setLoader(true);
    var endPoint = 'billwerk-productbyid/' + this.productId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.errorMessage = true;
        this.successMessage = false;
        this.message = this.result.content.messageList.billwerk;
      } else if (this.result.success == true) {
        this.errorMessage = false;
        this.msg = this.result.content.messageList.billwerk;
        this.productDetails = this.result.content.dataList;
      }
    })
  }


  getCustomersDetails() {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'userbyid/' + this.userData[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.customer = this.result.content.dataList;
        this.reEnterValue();
        if (this.customer && this.customer[0].ibanValue) {
          this.paymentForm.controls["iban"].setValue(this.customer[0].ibanValue);

        }

      }
    })
  }


  reEnterValue() {
    this.editUserForm.controls["email"].setValue(this.customer[0].mail);
    this.editUserForm.controls["firstName"].setValue(this.customer[0].firstName);
    this.editUserForm.controls["lastName"].setValue(this.customer[0].lastName);
    this.editUserForm.controls["society"].setValue(this.customer[0].society);
    this.editUserForm.controls["addressLine1"].setValue(this.customer[0].addressLine1);
    this.editUserForm.controls["zipcode"].setValue(this.customer[0].zipcode);
    this.editUserForm.controls["place"].setValue(this.customer[0].place);
    this.editUserForm.controls["accountNumber"].setValue(this.customer[0].accountNumber);
    this.editUserForm.controls["country"].setValue(this.customer[0].country);
    this.editUserForm.controls["contactNumber"].setValue(this.customer[0].contactNumber);
    this.editUserForm.controls["addressLine2"].setValue(this.customer[0].addressLine2);
    this.editUserForm.controls["bankCode"].setValue(this.customer[0].bankCode);
    this.editUserForm.controls["status"].setValue(this.customer[0].status);
    if (this.customer && this.customer[0].ibanValue) {
      this.editUserForm.controls["iban"].setValue(this.customer[0].ibanValue);
    }
    this.customer[0].role.forEach((element: any) => {
      const formArray: FormArray = this.editUserForm.get('userRole') as FormArray;
      formArray.push(new FormControl(element.rid));
    });

    this.editUserForm.controls["termsCon"].setValue(true);
    if (this.customer[0].image != '' && this.customer[0].image != 'undefined') {
      this.editUserForm.controls['image'].clearValidators();
      this.editUserForm.controls['image'].updateValueAndValidity();
      this.imagePreview = this.customer[0].image;
    }

  }



  getOrderData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'billwerk-orders/bycustomer/' + id[0].uid;
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;

      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.userOrderList = this.result.content.dataList;
        if (this.userOrderList.length > 0) {
          this.pamentBearer();
        }
        let mandateData = this.userOrderList.filter((o: { Contract: { PaymentBearer: any; }; }) => o.Contract.PaymentBearer);
        if (mandateData.length > 0) {
          this.mandateFiledDisplay = true;
          this.paymentForm.controls["mandateText"].setValue(this.userOrderList[0].Contract.PaymentBearer.MandateText);
        }
      }

    })
  }

  checkAcceptOrNot(object: any) {
    if (this.userOrderList) {
      return this.userOrderList.some((obj: any) => obj.PlanVariantId === object.Id);
    } else {
      return true;
    }
  }

  orderProduct(orderId: any, type: any) {
    this.orderId = orderId;
    this.type = type;

    if (this.paymentForm.value.chairManName) {
      var mandate = mandateText +
        '<p> chairman = ' + this.paymentForm.value.chairManName + '</p>'
      this.paymentForm.controls["mandateText"].setValue(mandate);
    }

    this.isSubmitted = true;
    if (this.paymentForm.invalid) {
      return;
    } else {
      let validateIban = {
        iban: this.paymentForm.value.iban
      }
      var endPoint = 'validate-iban';
      this.authService.sendRequest('post', endPoint, validateIban).subscribe(result => {
        this.result = result;
        if (this.result.success == false) {
          this.isValidIban = this.result.content.messageList.iban;
        } else if (this.result.success == true) {
          this.isValidIban = '';
          if (this.paymentForm.value.iban != (this.customer && this.customer[0].ibanValue)) {
            this.updateUser(this.paymentForm.value.iban)

          }
          let isRole = this.role.some((obj: any) => obj.name == 'Customer' || obj.role == 'Customer' || obj.name == 'Customer');
          if ((isRole == true) && (this.productDetails[0].planDetail.CustomFields.type == 1 && this.userOrderList.length == 1 && this.userOrderList[0].AllowWithoutPaymentData == true)) {
            this.licenseCancel(this.userOrderList[0].ContractId);
            this.orderProcess();
          } else {
            this.orderProcess();
          }
        }
      })
    }
  }

  orderProcess() {
    var order_id;
    let test: any;
    this.authService.setLoader(false);
    if (this.userOrderList.length == 0) {
      if (this.paymentForm.value.coupon == '') {
        test =
        {
          "triggerInterimBilling": false,
          "cart": {
            "planVariantId": this.orderId,
            "inheritStartDate": false
          },
          "customer": {
            "companyName": this.customer[0].society,
            "firstName": this.customer[0].firstName,
            "lastName": this.customer[0].lastName,
            "emailAddress": this.customer[0].mail,
            "address": {
              "street": this.customer[0].addressLine1,
              "postalCode": this.customer[0].zipcode,
              "city": this.customer[0].place,
              "country": 'DE'
            },
            "hidden": false
          },
          "previewAfterTrial": false
        };
      } else {
        if (this.type == 1) {
          test =
          {
            "triggerInterimBilling": false,
            "cart": {
              "planVariantId": this.orderId,

              "inheritStartDate": false,
              "CouponCode": this.paymentForm.value.coupon
            },
            "customer": {
              "companyName": this.customer[0].society,
              "firstName": this.customer[0].firstName,
              "lastName": this.customer[0].lastName,
              "emailAddress": this.customer[0].mail,
              "address": {
                "street": this.customer[0].addressLine1,
                "postalCode": this.customer[0].zipcode,
                "city": this.customer[0].place,
                "country": 'DE'
              },
              "hidden": false
            },
            "previewAfterTrial": false
          };
        } else {
          test =
          {
            "triggerInterimBilling": false,
            "cart": {
              "planVariantId": this.orderId,

              "inheritStartDate": false,
            },
            "customer": {
              "firstName": this.customer[0].firstName,
              "lastName": this.customer[0].lastName,
              "emailAddress": this.customer[0].mail,
              "address": {
                "street": this.customer[0].addressLine1,
                "postalCode": this.customer[0].zipcode,
                "city": this.customer[0].place,
                "country": 'DE'
              },
              "hidden": false
            },
            "previewAfterTrial": false
          };
        }

      }
    } else if (this.userOrderList.length > 0) {

      if (this.paymentForm.value.coupon == '') {
        if (this.type == 3) {
          var startDate = new Date(this.userOrderList[0].Contract.StartDate);
          startDate.setMonth(startDate.getMonth() + 3);
          var endDate = startDate;
          test =
          {
            "triggerInterimBilling": false,
            "CustomerId": this.userOrderList[0].CustomerId,
            "cart": {
              "planVariantId": this.orderId,


            },
            "StartDate": endDate,
            "previewAfterTrial": false
          };
        } else {
          test =
          {
            "triggerInterimBilling": false,
            "CustomerId": this.userOrderList[0].CustomerId,
            "cart": {
              "planVariantId": this.orderId,

              "inheritStartDate": false
            },
            "previewAfterTrial": false
          };
        }

      } else {
        if (this.type == 1) {
          test =
          {
            "triggerInterimBilling": false,
            "CustomerId": this.userOrderList[0].CustomerId,
            "cart": {
              "planVariantId": this.orderId,
              "CouponCode": this.paymentForm.value.coupon,
              "inheritStartDate": false
            },
            "previewAfterTrial": false
          };
        } else if (this.type == 3) {
          var startDate = new Date(this.userOrderList[0].Contract.StartDate);
          startDate.setMonth(startDate.getMonth() + 3);
          var endDate = startDate;
          test =
          {
            "triggerInterimBilling": false,
            "CustomerId": this.userOrderList[0].CustomerId,
            "cart": {
              "planVariantId": this.orderId,
              "CouponCode": this.paymentForm.value.coupon,

            },
            "StartDate": endDate,
            "previewAfterTrial": false
          };
        }
        else {
          test =
          {
            "triggerInterimBilling": false,
            "CustomerId": this.userOrderList[0].CustomerId,
            "cart": {
              "planVariantId": this.orderId,
              "inheritStartDate": false
            },
            "previewAfterTrial": false
          };
        }
      }

    }

    this.orderData = test;
    this.paymentData = this.paymentForm.value;
    if (this.customer && this.customer[0].firstName == '') {
      this.orderProductData = test
      this.authService.setpopUp(true);
    } else {
      if (this.paymentForm.value.iban != (this.customer && this.customer[0].ibanValue)) {
        this.updateUser(this.paymentForm.value.iban)
      }
      this.authService.setLoader(true);
      var endPoint = 'billwerk-order';
      this.authService.sendRequest('post', endPoint, test).subscribe(result => {
        this.result = result;
        if (this.result.success == false) {
          this.authService.setLoader(false);
          this.visibility = 2;
          if (this.result.content.messageList.order == 'To purchase the sub-products, please order the Club-Administration product first.') {
            this.clubOrderError = this.result.content.messageList.order
          } else {
            this.error = this.result.content.messageList.order;
          }
        } else if (this.result.success == true) {
          this.error = '';
          this.visibility = 2;
          order_id = this.result.content.dataList[0].Id;
          this.commit(order_id);
        }
      })
    }
  }

  commit(order_id: any) {
    var payMent = {
      paymentMethod: billwerkPaymentMethod,
      bearer: {
        holder: this.paymentForm.value.holder,
        iban: this.paymentForm.value.iban,
        mandateText: this.paymentForm.value.mandateText
      }
    }
    var endPoint = 'billwerk-order/commit/' + order_id;
    this.authService.sendRequest('post', endPoint, payMent).subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
        this.msg = '';
      } else if (this.result.success == true) {
        this.error = '';
        this.commitData = this.result.content.dataList;
        let isRole = this.role.some((o: any) => o.name == 'SGP-Wechsler' || o.role == 'SGP-Wechsler');
        if (isRole) {
          this.authService.setCounter(false);
        } else {
          this.authService.setCounter(true);
        }
        if (this.commitData) {
          $('#commit').modal({
            backdrop: 'static'
          });
          $('#commit').modal('show');
        }
      }
    })
  }

  getMandateText(event: any) {
    if (event.target.checked) {
      var mandate = mandateText +
        '<p> chairman = ' + this.paymentForm.value.chairManName + '</p>'
      this.paymentForm.controls["mandateText"].setValue(mandate);
    } else if (!event.target.checked) {
      this.paymentForm.controls["mandateText"].setValue('');
    }
  }

  fieldVisibility(event: any) {
    if (event.target.checked) {
      this.field = true;
      this.paymentForm.controls['chairManName'].setValidators([Validators.required]);
      this.paymentForm.controls['chairManName'].updateValueAndValidity();
    } else if (!event.target.checked) {
      this.field = false;
    }
  }

  close() {
    $('#commit').modal('hide');
    this.router.navigate(['/order-product-list'])
  }

  licenseCancel(contractId: any) {
    let cudate = new Date();
    let cancleOrder = {
      "endDate": cudate.toISOString()
    }
    let self = this;
    self.authService.setLoader(true);
    var endPoint = 'billwerk-contract-end/' + contractId
    self.authService.sendRequest('post', endPoint, cancleOrder).subscribe((result: any) => {
      self.result = result;
    })
  }

  updateUser(iban: string) {
    var formData: any = new FormData();
    this.editUserForm.value.iban = iban
    let self = this;
    for (const key in this.editUserForm.value) {
      if (Object.prototype.hasOwnProperty.call(this.editUserForm.value, key)) {
        const element = this.editUserForm.value[key];

        if (key == 'image') {
          formData.append('imageUrl', this.imagePreview);
        }
        if (key == 'userRole') {
          formData.append('userRole', JSON.stringify(element));
        }
        else {
          if ((key != 'image') && (key != 'userRole')) {
            formData.append(key, element);
          }
        }
      }

    }
    let userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'profile-edit/' + userData[0].uid
    this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
      this.result = result;
    })
  }

  // /billwerk-allpaymentbearer-bcustomer/+ this.userOrderList[0].CustomerId

  pamentBearer() {
    var endPoint = 'billwerk-allpaymentbearer-bcustomer/' + this.userOrderList[0].CustomerId
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {
          var chairman = element.MandateText.split('chairman =');
          element.chairman_name = chairman[1].split('</p>')[0]
        });
        this.paymentBearer = this.result.content.dataList.find((o: any) => o.IBAN == this.customer[0].ibanValue)
        if (this.paymentBearer) {
          this.paymentForm.controls["holder"].setValue(this.paymentBearer.Holder ? this.paymentBearer.Holder : '');
          this.paymentForm.controls["iban"].setValue(this.paymentBearer.IBAN ? this.paymentBearer.IBAN : '');
          this.paymentForm.controls["chairManName"].setValue(this.paymentBearer.chairman_name ? this.paymentBearer.chairman_name : '');
        }
      }

    })
  }

}
