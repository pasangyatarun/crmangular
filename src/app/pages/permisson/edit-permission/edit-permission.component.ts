import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import *  as module from "../../../module.json";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-permission',
  templateUrl: './edit-permission.component.html',
  styleUrls: ['./edit-permission.component.css']
})
export class EditPermissionComponent implements OnInit {

  modules: any = (module as any).default;
  rolesList: any = [];
  rolesItems: any = [];
  rolesSettings: any;
  permissionList = [
    { item_id: 1, item_text: 'ADD' },
    { item_id: 2, item_text: 'VIEW' },
    { item_id: 3, item_text: 'EDIT' },
    { item_id: 4, item_text: 'DELETE' },
  ];
  permissionSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    enableCheckAll: true,
    unSelectAllText: 'UnSelect All',
    allowSearchFilter: true
  };
  permissionItems: any = [];

  error: any;
  language: any;
  msg: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  visibility: any = 0;
  distributors: any;
  selectedData: any;
  result: any;
  permissionForm: FormGroup;
  role: any;
  id: any;


  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) {
    this.permissionForm = this.formBuilder.group({
      modules: ['', Validators.required],
      permissions: ['', Validators.required],
      roles: ['', Validators.required],

    });
    this.id = this.route.snapshot.paramMap.get('id');
  }

  get formControls() { return this.permissionForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getRolelist();
  }

  getRolelist() {
    this.authService.setLoader(true);
    let self = this;
    var endPoint = 'roles'
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.userAdd;
      } else if (this.result.success == true) {
        
        this.role = this.result.content.dataList;
        this.result.content.dataList.forEach((val: any, key: any) => {
          // this.users = this.users.concat({item_id:1,item_text:"test"})
          this.rolesList.push({ 'id': val.rid, 'name': val.name });
        });
        self.rolesSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          enableCheckAll: false,
        };
      }
    })
  }

  roleSelect(items: any) {
    this.rolesItems.push(items.name);
  }

  roleDeSelect(item: any) {

    const index = this.rolesItems.indexOf(item.id);
    if (index > -2 || -1) {
      this.rolesItems.splice(index, 1);
    }
  }

  roleSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.rolesItems.push({ 'id': element.id });
      }
    }
  }

  roleDeSelectAll(item: any) {
    this.rolesItems = [];
  }

  permissionSelect(items: any) {
    this.permissionItems.push(items.item_text);
  }

  permissionDeSelect(item: any) {

    const index = this.permissionItems.indexOf(item.item_text);
    if (index > -2 || -1) {
      this.permissionItems.splice(index, 1);
    }
  }

  permissionSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.permissionItems.push({ 'id': element.item_text });
      }
    }
  }

  permissionDeSelectAll(item: any) {
    this.permissionItems = [];
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.permissionForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      this.permissionForm.controls["permissions"].setValue(this.permissionItems);
      var endPoint = 'rolepermissioncreate'
      this.authService.sendRequest('post', endPoint, this.permissionForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList;
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.rolepermission;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.rolepermission;
          setTimeout(() => {
            this.router.navigate(['/view-permission']);
          }, 3000)
        }
      })
    }

  }



}
