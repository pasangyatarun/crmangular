import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import *  as module from "../../../module.json";

@Component({
  selector: 'app-add-permission',
  templateUrl: './add-permission.component.html',
  styleUrls: ['./add-permission.component.css']
})
export class AddPermissionComponent implements OnInit {

  modules: any = (module as any).default;
  modulesList: any = [];
  modulesSettings: any;
  modulesItems: any = [];
  rolesList: any = [];
  rolesItems: any = [];
  rolesSettings: any;
  permissionList = [
    { item_id: 1, item_text: 'ADD' },
    { item_id: 2, item_text: 'VIEW' },
    { item_id: 3, item_text: 'EDIT' },
    { item_id: 4, item_text: 'DELETE' },
  ];
  permissionSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    enableCheckAll: true,
    unSelectAllText: 'UnSelect All',
    allowSearchFilter: true
  };
  permissionItems: any = [];
  error: any = false;
  language: any;
  msg: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  visibility: any = 0;
  distributors: any;
  selectedData: any;
  result: any;
  permissionForm: FormGroup;
  role: any;
  permissionn: any;


  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router) {
    this.permissionForm = this.formBuilder.group({
      modules: ['', Validators.required],
      permissions: ['', Validators.required],
      roles: ['', Validators.required],

    });

  }

  get formControls() { return this.permissionForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getRolelist();
    this.getModules();
    this.getPermissionData();
  }

  getModules() {
    let self = this;
    this.modules.module.forEach((val: any, key: any) => {
      this.modulesList.push({ 'id': val.id, 'name': val.value });
    });

    self.modulesSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      enableCheckAll: false,
    };
  }


  moduleSelect(items: any) {
    this.modulesItems = items.name
    // this.modulesItems.push(items.name);

  }

  moduleDeSelect(item: any) {

    const index = this.modulesItems.indexOf(item.id);
    if (index > -1) {
      this.modulesItems.splice(index, 1);
    }
  }

  moduleSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.modulesItems.push({ 'id': element.id });
      }
    }
  }

  moduleDeSelectAll(item: any) {
    this.modulesItems = [];
  }

  getRolelist() {
    this.authService.setLoader(true);
    let self = this;
    var endPoint = 'roles'
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.userAdd;
      } else if (this.result.success == true) {
        this.error = false;
        this.authService.setLoader(false);
        this.role = this.result.content.dataList;
        this.result.content.dataList.forEach((val: any, key: any) => {
          // this.users = this.users.concat({item_id:1,item_text:"test"})
          this.rolesList.push({ 'id': val.rid, 'name': val.name });
        });
        self.rolesSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          enableCheckAll: false,
        };
      }
    })
  }

  roleSelect(items: any) {
    // this.rolesItems.push(items.id);
    this.rolesItems = items.id

  }

  roleDeSelect(item: any) {
    const index = this.rolesItems.indexOf(item.id);
    if (index > -2 || -1) {
      this.rolesItems.splice(index, 1);
    }
  }

  roleSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.rolesItems.push({ 'id': element.id });
      }
    }
  }

  roleDeSelectAll(item: any) {
    this.rolesItems = [];
  }

  permissionSelect(items: any) {
    this.permissionItems.push(items.item_text);
  }

  permissionDeSelect(item: any) {

    const index = this.permissionItems.indexOf(item.item_text);
    if (index > -2 || -1) {
      this.permissionItems.splice(index, 1);
    }
  }

  permissionSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];

        this.permissionItems.push(element.item_text);
      }
    }
  }

  permissionDeSelectAll(item: any) {
    this.permissionItems = [];
  }

  getPermissionData() {
    this.authService.setLoader(true);
    var endPoint = 'rolespermissions'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        if (this.result.content.dataList) {
          let self = this
          this.result.content.dataList.forEach((val: any) => {
            let r_id = this.result.content.dataList.includes('DELETE') === 'DELETE';

            // this.result.content.dataList.forEach((element: any) => {
            //   if (val.item_text == element.permission) {
            //     self.permissionn = [];
            //     self.permissionn.push({ id: val.item_id, name: val.item_text })
            //     this.permissionForm.controls["permissions"].setValue(self.permissionn);
            //   }
            // });

          })
        }
      }
    })
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.permissionForm.invalid) {
      return;
    } else {
      this.permissionForm.controls["permissions"].setValue(this.permissionItems);
      this.permissionForm.controls["modules"].setValue(this.modulesItems);
      this.permissionForm.controls["roles"].setValue(this.rolesItems);
      this.authService.setLoader(true);
      var endPoint = 'rolepermissioncreate'
      this.authService.sendRequest('post', endPoint, this.permissionForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);

        if (this.result.success == false) {
          this.msg = this.result.content.messageList;
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.rolepermission;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.rolepermission;
          setTimeout(() => {
            this.router.navigate(['/view-permission']);
          }, 1000)
        }
      })
    }

  }


}
