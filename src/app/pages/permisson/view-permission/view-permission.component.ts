import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { data } from 'jquery';
declare let $: any;
import *  as module from "../../../module.json";

@Component({
  selector: 'app-view-permission',
  templateUrl: './view-permission.component.html',
  styleUrls: ['./view-permission.component.css']
})
export class ViewPermissionComponent implements OnInit {
  displayedColumns: string[] = [
    'roles',
    'modules',
    'permissions',
    'Delete',

  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;

  userData: any[] = [];
  error: any = false;
  result: any;

  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  role: any;
  id: any;
  filterForm: FormGroup;
  modules: any = (module as any).default;
  permissions: any;
  logrole: any;
  isData:any = true ;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.filterForm = this.formBuilder.group({
      rid: [''],
      module: [''],
    });
 
  }

  ngOnInit(): void {
    this.logrole = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    
    this.language = this.langService.getLanguaageFile();
    this.getPermissionData();
    this.getRolelist();
    this.removeColumn();
  }

  getPermissionData() {
    this.authService.setLoader(true);
    var endPoint = 'rolespermissions'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
        this.isData = false;
      } else if (this.result.success == true) {
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if(this.dataSource.data.length){
          this.isData = true;
        }else{
          this.isData = false;
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort){
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  
  getRolelist() {
    this.authService.setLoader(true);
    let self = this;
    var endPoint = 'roles'
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.userAdd;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.role = this.result.content.dataList;
      }
    })
  }

  modal(event: any) {
    if (event == 'delete') {
      $('#exampleModal').modal('show')
    } else if (event == 'no') {
      $('#exampleModal').modal('hide')
    } else if (event == 'yes') {
      $('#exampleModal').modal('hide')
      this.deletePermision();
    }
  }

  getPermisionId(id: any) {
    this.id = id;
  }

  deletePermision() {
    this.authService.setLoader(true);
    var endPoint = 'rolepermissiondelete/' + this.id
    this.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.rolepermission;
      } else if (this.result.success == true) {
        this.error = false;
        this.success = true;
        this.message = this.result.content.messageList.rolepermission;
        setTimeout(() => {
          this.getPermissionData();
        }, 3000)
      }
    })
  }


  onSubmit() {
    this.authService.setLoader(true);
    this.isSubmitted = true;
    if (this.filterForm.invalid) {
      return;
    } else {
      var endPoint = 'rolepermissionfilter'
      this.authService.sendRequest('post', endPoint, this.filterForm.value).subscribe((result: any) => {
        this.authService.setLoader(false
        );
        this.result = result;
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.rolepermission;
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          this.dataSource.paginator = this.paginator;
          this.isData = false;
          this.dataSource.sort = this.sort;
        } else if (this.result.success == true) {
          this.error = false;
          this.message = this.result.content.content
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          if(this.dataSource.data.length){
            this.isData = true;
          }else{
            this.isData = false;
          }
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;


        }
      })
    }
  }
  removeColumn() {
    if (!this.authService.isAdmin() && this.permissions.permission && !this.permissions.permission.includes('DELETE')) {
      this.columnsToDisplay.pop();
    }
  }

  reset() {
    this.filterForm.reset();
    this.getPermissionData();
  }
}
