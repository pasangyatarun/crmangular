import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
declare let $: any;

@Component({
  selector: 'app-view-s-license',
  templateUrl: './view-s-license.component.html',
  styleUrls: ['./view-s-license.component.css']
})
export class ViewSLicenseComponent implements OnInit {

  displayedColumns: string[] =
    ['Title',
      'Description',
      'Publish_to',
      'Domain-Name',
      'Edit-s_license',

    ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;
  error: any;
  result: any;
  nameSearch: any;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;

  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  licenseListForm: FormGroup;
  isSubmitted: any = false;
  permissions: any;
  role: any;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.licenseListForm = this.formBuilder.group({
      role: [''],
      status: [''],
    });
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getLicensedata();
  }

  getLicensedata() {
    this.authService.setLoader(true);
    var endPoint = 'licences'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  onSubmit() {
    this.isSubmitted = true;
    if (this.licenseListForm.invalid) {
      return;
    } else {
      // var endPoint = 'signin'
      // this.authService.sendRequest('post', endPoint,  this.licenseForm.value).subscribe((result: any)=>{
      //   this.result = result;
      //   if(this.result.success == false){
      //     this.error = true;
      //     this.success = false;
      //     this.message =  this.result.content.messageList.Signin;
      //   }else if(this.result.success == true){
      //     var data = this.result.content.dataList[0]['token'];

      //   }
      // })
    }
  }
  modal(event: any) {
    if (event == 'delete') {
      $('#exampleModal').modal('show')
    } else if (event == 'no') {
      $('#exampleModal').modal('hide')
    } else if (event == 'yes') {
      this.confirm = event
    }
  }
  // ngAfterViewInit() {
  //   this.dataSource.sort = this.sort;
  //   this.dataSource.paginator = this.paginator;
  // }

  deleteUser(id: any,) {
    $('#exampleModal').modal('show')
    if (this.confirm == 'yes') {
      var endPoint = 'licencedelete/' + id
      this.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        this.result = result
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = this.result.content.messageList.user;
        } else if (this.result.success == true) {
          window.location.reload();
        }
      })
    }

  }

  removeColumn() {
    if ((this.role[0].name != 'Administrator') && this.permissions.Slicense && !this.permissions.Slicense.includes('EDIT')) {
      this.columnsToDisplay.pop();
    }
  }


}
