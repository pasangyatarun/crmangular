import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSLicenseComponent } from './view-s-license.component';

describe('ViewSLicenseComponent', () => {
  let component: ViewSLicenseComponent;
  let fixture: ComponentFixture<ViewSLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewSLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
