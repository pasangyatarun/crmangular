import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-s-license',
  templateUrl: './s-license.component.html',
  styleUrls: ['./s-license.component.css']
})
export class SLicenseComponent implements OnInit {

  licenseForm:  FormGroup;
  isSubmitted:any = false;
  language:any;
  message: any;
  result: any;
  error: any = false;
  success: any =  false;
  msg: any;
  domain: any;


  constructor(private router:Router, private formBuilder:FormBuilder, private authService:AuthService, private langService:LanguageService) {

    this.licenseForm  =  this.formBuilder.group({
      title: ['',Validators.required],
      description: ['',Validators.required],
      domain: ['',Validators.required],
      publish:new FormArray([],Validators.required),
    });
     
    
  }

  get formControls() { return this.licenseForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();  
    this.getDomainName();
  }

  onSubmit(){
    this.isSubmitted = true;
    
    if(this.licenseForm.invalid){
      return;
    }else {
      let publish =  this.licenseForm.value.publish.toString()
      this.licenseForm.value.publish = publish
      var endPoint = 'licencecreate'
      this.authService.sendRequest('post', endPoint,  this.licenseForm.value).subscribe((result: any)=>{
        this.result = result;
        if(this.result.success == false){
          this.error = true;
          this.success = false;
          this.message =  this.result.content.messageList.license;
          this.msg =  this.result.content.messageList;
        }else if(this.result.success == true){
          this.message =  this.result.content.messageList.license;
           this.router.navigate(['/view-s-license']);
          
        }
      })
    }
  }

  getDomainName(){
    this.authService.setLoader(true); 
    let self = this;    
      var endPoint = 'domains';
      this.authService.sendRequest('get',endPoint,'').subscribe(result=>{
        this.result = result
        if(this.result.success == false){
          this.msg =  this.result.content.messageList.Signup;
        }else if(this.result.success == true){
          this.authService.setLoader(false); 
          this.msg =  this.result.content.messageList.Signup;
          this.domain= this.result.content.dataList; 
          if(this.domain.length > 0){
            this.domain.forEach((element: any,index: any) => {
            
            let d_id = "Mein S-Verein,S-Verein | Die Online-Vereinsverwaltung".split(',').find((o:any) => o === element.sitename);  
               
              if(d_id){
                const formArray: FormArray = this.licenseForm.get('publish') as FormArray;                
                formArray.push(new FormControl(element.sitename));
                self.domain[index]['d_id'] = element.sitename;
              }else{
                self.domain[index]['d_id'] = '';
              }
            });
          }
        }
      })
  }

  onCheckDomain(event:any) {
    const formArray: FormArray = this.licenseForm.get('publish') as FormArray;
    /* Selected */
    if(event.target.checked){
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else{
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if(ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
}
