import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SLicenseComponent } from './s-license.component';

describe('SLicenseComponent', () => {
  let component: SLicenseComponent;
  let fixture: ComponentFixture<SLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
