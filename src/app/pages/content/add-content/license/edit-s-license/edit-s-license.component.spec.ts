import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSLicenseComponent } from './edit-s-license.component';

describe('EditSLicenseComponent', () => {
  let component: EditSLicenseComponent;
  let fixture: ComponentFixture<EditSLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
