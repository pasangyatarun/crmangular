import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ActivatedRoute } from '@angular/router';
declare let $: any;

@Component({
  selector: 'app-edit-s-license',
  templateUrl: './edit-s-license.component.html',
  styleUrls: ['./edit-s-license.component.css']
})
export class EditSLicenseComponent implements OnInit {


  licenseForm: FormGroup;
  isSubmitted: any = false;
  language: any;
  message: any;
  result: any;
  error: any = false;
  success: any = false;
  msg: any;
  domain: any;
  domainData = [
    { domain_id: "1", sitename: "Mein S-Verein" },
    { domain_id: "2", sitename: "S-Verein | Die Online-Vereinsverwaltung" }

  ];
  id: string | null;
  reEnterData: any;
  permissions: any;
  role: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {

    this.licenseForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      domain: ['', Validators.required],
      publish: new FormArray([], Validators.required),
    });

    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
  }

  get formControls() { return this.licenseForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getLicenseData();
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.licenseForm.invalid) {
      return;
    } else {
      let publish = this.licenseForm.value.publish.toString()
      this.licenseForm.value.publish = publish
      var endPoint = 'licenceupdate/' + this.id
      this.authService.sendRequest('put', endPoint, this.licenseForm.value).subscribe((result: any) => {
        this.result = result;
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.license;
        } else if (this.result.success == true) {
          this.message = this.result.content.messageList.license;
          this.router.navigate(['/view-s-license']);

        }
      })
    }
  }


  getLicenseData() {
    this.authService.setLoader(true);
    var endPoint = 'licencebyid/' + this.id;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
        this.reEnterData = this.result.content.dataList;
        this.reEnterValue();
        this.getDomainName();

      }
    })
  }

  getDomainName() {
    this.authService.setLoader(true);
    let self = this;
    var endPoint = 'domains';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
        this.domain = this.result.content.dataList;
        if (this.domain.length > 0) {
          this.domain.forEach((element: any, index: any) => {
            let d_id = this.reEnterData[0].publish_to.split(',').find((o: any) => o == element.sitename);
            if (d_id) {
              const formArray: FormArray = this.licenseForm.get('publish') as FormArray;
              formArray.push(new FormControl(element.sitename));
              self.domain[index]['d_id'] = element.sitename;
            } else {
              self.domain[index]['d_id'] = '';
            }
          });
        }
      }
    })
  }

  onCheckDomain(event: any) {
    const formArray: FormArray = this.licenseForm.get('publish') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  reEnterValue() {
    this.licenseForm.controls["title"].setValue(this.reEnterData[0].title);
    this.licenseForm.controls["description"].setValue(this.reEnterData[0].data);
    this.licenseForm.controls["domain"].setValue(this.reEnterData[0].domain);

  }
  modal(event: any) {
    if (event == 'delete') {
      $('#exampleModal').modal('show')
    } else if (event == 'no') {
      $('#exampleModal').modal('hide')
    } else if (event == 'yes') {
      $('#exampleModal').modal('hide')
      this.deleteUser();
    }
  }

  deleteUser() {
    this.authService.setLoader(true);
    var endPoint = 'licencedelete/' + this.id
    this.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.router.navigate(['/view-s-license'])
      }
    })
  }

}
