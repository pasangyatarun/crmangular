import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import {ConfirmDialogService} from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-edit-pages',
  templateUrl: './edit-pages.component.html',
  styleUrls: ['./edit-pages.component.css']
})
export class EditPagesComponent implements OnInit {
  pagesForm: FormGroup;
  imageSrc: any;
  isSubmitted: any = false;
  checks = [
    { description: 'Feature', value: 'feature' },
    { description: "Training", value: 'training' },
    { description: "Team", value: 'team' },
    { description: "Event", value: 'event' },
    { description: "Various", value: 'various' },
    { description: "Video", value: 'video' },

  ];
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  message: any;
  language: any;
  id: any;
  reEnterData: any;
  msg: any;
  permissions: any;
  role: any;
  hasPicture = false;
  imagePreview: any;

  constructor(private confirmBoxService:ConfirmDialogService, private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.pagesForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      url: [''],
      type: ['', Validators.required],
      author: [''],
      image: [''],
    });
  }
  
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getpagesData();
  }

  get formControls() { return this.pagesForm.controls; }


  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;

      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }


  onSubmit() {

    this.isSubmitted = true;
    if (this.pagesForm.invalid) {
      return;
    } else {
      
      var formData: any = new FormData();
      this.pagesForm.value.image = this.imageSrc;
      let self = this;
      for (const key in this.pagesForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.pagesForm.value, key)) {
          const element = this.pagesForm.value[key];

          if (key == 'image') {
            if (self.imageSrc) {
              formData.append('file', self.imageSrc);
            } else {
              formData.append('imageUrl', self.imagePreview);
            }
          }

          else {
            if ((key != 'image')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      var endPoint = 'pagesedit/' + this.id
      this.authService.sendRequest('put', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.msg = this.result.content.messageList;
          this.message = this.result.content.messageList.pages;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.pages
          setTimeout(() => {
            this.router.navigate(['/view-pages']);
          }, 3000)
        }
      })
    }
  }

  getpagesData() {
    this.authService.setLoader(true);
    var endPoint = 'pagesbyid/' + this.id;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
        this.reEnterData = this.result.content.dataList;
        this.reEnterValue();
      }
    })
  }

  reEnterValue() {
    this.pagesForm.controls["title"].setValue(this.reEnterData[0].title);
    this.pagesForm.controls["content"].setValue(this.reEnterData[0].content);
    this.pagesForm.controls["type"].setValue(this.reEnterData[0].type);
    this.pagesForm.controls["author"].setValue(this.reEnterData[0].author_id);
    this.pagesForm.controls["url"].setValue(this.reEnterData[0].url);

    if (this.reEnterData[0].image) {
      this.hasPicture = true;
      this.imagePreview = this.reEnterData[0].image;
    }
  }

  diableField(event: any) {
    if (!!event.target.checked) {
      this.pagesForm.controls['url'].disable()
    } else {
      this.pagesForm.controls['url'].enable()
    }
  }



  deletePages(){
    let self = this;
    self.confirmBoxService.confirmThis(this.language.pages.deletepages,this.language.confirmBox.deletePage, function () {

      self.authService.setLoader(true);
      var endPoint = 'pagesdelete/' + self.id
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.msg = self.result.content.messageList;
          self.message = self.result.content.messageList.pages;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.result.content.messageList.pages
          setTimeout(() => {
            self.router.navigate(['/view-pages']);
          }, 3000)
        }
      })
     

    }, function () { }
    )

}

}
