import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-add-pages',
  templateUrl: './add-pages.component.html',
  styleUrls: ['./add-pages.component.css']
})
export class AddPagesComponent implements OnInit {
  pagesForm: FormGroup;
  imageSrc: any;
  isSubmitted: any = false;
  checks = [
    { description: 'Feature', value: 'feature' },
    { description: "Training", value: 'training' },
    { description: "Team", value: 'team' },
    { description: "Event", value: 'event' },
    { description: "Various", value: 'various' },
    { description: "Video", value: 'video' },

  ];
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  message: any;
  language: any;
  msg: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.pagesForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      url: [''],
      type: ['', Validators.required],
      author: [''],
      image: [''],

    });

  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
  }

  get formControls() { return this.pagesForm.controls; }



  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;

      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }


  onSubmit() {
    this.isSubmitted = true;
    let userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    var userId = userData[0].uid;
    this.pagesForm.value.author = userId;
    this.isSubmitted = true;
    if (this.pagesForm.invalid) {
      return;
    } else {
      var formData: any = new FormData();
      this.pagesForm.value.image = this.imageSrc;
      let self = this;
      for (const key in this.pagesForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.pagesForm.value, key)) {
          const element = this.pagesForm.value[key];

          if (key == 'image') {

            formData.append('file', self.imageSrc);
          }

          else {
            if ((key != 'image')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      var endPoint = 'pagescreate'
      this.authService.sendRequest('post', endPoint,formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.msg = this.result.content.messageList;
          this.message = this.result.content.messageList.pages;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.pages
          setTimeout(() => {
            this.router.navigate(['/view-pages']);
          }, 3000)
        }
      })
    }
  }

  diableField(event: any) {
    if (!!event.target.checked) {
      this.pagesForm.controls['url'].disable()
    } else {
      this.pagesForm.controls['url'].enable()
    }
  }


}
