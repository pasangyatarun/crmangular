import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import {ConfirmDialogService} from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.css']
})
export class EditNewsComponent implements OnInit {

  newsForm: FormGroup;
  imageSrc: any;
  isSubmitted: any = false;
  checks = [
    { description: 'Feature', value: 'feature' },
    { description: "Training", value: 'training' },
    { description: "Team", value: 'team' },
    { description: "Event", value: 'event' },
    { description: "Various", value: 'various' },
    { description: "Video", value: 'video' },

  ];
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  message: any;
  language: any;
  id: any;
  role: any;
  permissions: any;
  reEnterData: any;
  msg: any;
  hasPicture = false;
  imagePreview: any;

  constructor(private confirmBoxService:ConfirmDialogService,private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.newsForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      url: [''],
      imageUpload: [''],
      videoUpload: [''],
      youtube_url: [''],
      author: [''],
      tag: new FormArray([]),
    });
    
  }
  
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getNewsData();
  }

  get formControls() { return this.newsForm.controls; }



  onivideoSelected(event: any) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {

      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.newsForm.patchValue({
          videoUpload: reader.result
        });
      };
    }
  }

  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    // $('#textPreview').show();
    // $('#textPreview').text(file.name);
  }

  getNewsData() {
    this.authService.setLoader(true);
    var endPoint = 'newsbyid/' + this.id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.news;
        this.msg = this.result.content.messageList;
      } else if (this.result.success == true) {
        this.reEnterData = this.result.content.dataList;
        this.error = false;
        this.reEnterValue();
      }
    })
  }

  reEnterValue() {
    this.newsForm.controls["title"].setValue(this.reEnterData[0].title);
    this.newsForm.controls["content"].setValue(this.reEnterData[0].content);
    this.newsForm.controls["url"].setValue(this.reEnterData[0].url);
    this.newsForm.controls["imageUpload"].setValue(this.reEnterData[0].image);
    this.newsForm.controls["videoUpload"].setValue(this.reEnterData[0].video);
    this.newsForm.controls["author"].setValue(this.reEnterData[0].author_id);
    this.newsForm.controls["youtube_url"].setValue(this.reEnterData[0].youtube_url);
    if (this.reEnterData[0].image) {
      this.hasPicture = true;
      this.imagePreview = this.reEnterData[0].image;
    }
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.newsForm.invalid) {
      return;
    } else {

      var formData: any = new FormData();
    
      let self = this;
      for (const key in this.newsForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.newsForm.value, key)) {
          const element = this.newsForm.value[key];

          if (key == 'imageUpload') {
            if (self.imageSrc) {
              formData.append('file', self.imageSrc);

            } else {
              formData.append('imageUrl', self.imagePreview);

            }
          }

          else {
            if ((key != 'imageUpload')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      var endPoint = 'newsedit/' + this.id
      this.authService.sendRequest('put', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.news;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.news;
          setTimeout(() => {
            this.router.navigate(['/view-news']);
          }, 3000)
        }
      })
    }
  }

  diableField(event: any) {
    if (!!event.target.checked) {
      this.newsForm.controls['url'].disable()
    } else {
      this.newsForm.controls['url'].enable()
    }
  }
  onCheckChange(event: any) {
    const formArray: FormArray = this.newsForm.get('tag') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;

      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }


  deleteNews(){
    let self = this;
    self.confirmBoxService.confirmThis(this.language.news.deleteNews,this.language.confirmBox.deleteNews, function () {
      self.authService.setLoader(true);
      var endPoint = 'newsdelete/' + self.id
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.news;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.result.content.messageList.news;
          setTimeout(() => {
            self.router.navigate(['/view-news']);
          }, 3000)
        }
      })
     

    }, function () { }
    )

 }

}
