import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-all-news',
  templateUrl: './all-news.component.html',
  styleUrls: ['./all-news.component.css']
})
export class AllNewsComponent implements OnInit {
  userData: any;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  success: any = false;
  error: any = false;
  file: any;
  id: any;
  EnterData: any;
  language: any;
  role: any;
  permissions: any;
  data: any;
  paginator: number = 1;
  displayFlag: any = 'en';
  currentPageNmuber: number = 1;
  itemPerPage = 8;
  limitPerPage = [
    { value: '8' },
    { value: '16' },
    { value: '24' },
    { value: '32' },
    { value: '40' }
  ];

  totalRecord = 0;
  responseMessage: any;

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.data = this.route.snapshot.paramMap.get('data');
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getnewsData();
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.displayFlag = localStorage.getItem('language');
  }

  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    window.location.reload();
  }

  getnewsData() {
    this.authService.setLoader(true);
    var endPoint = 'news';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.news;
      } else if (this.result.success == true) {
        this.error = false;
        this.msg = this.result.content.messageList.Signup;
        this.EnterData = this.result.content.dataList;
        this.totalRecord = this.result.content.dataList.length;
      }
    })
  }


  pageChanged(event: any) {
    this.currentPageNmuber = event;
    this.getnewsData();
  }

  goToPg(eve: any) {
    this.responseMessage = null;
    if (isNaN(eve)) {
      eve = this.currentPageNmuber;
    } else {
      if (eve > Math.round(this.totalRecord / this.itemPerPage)) {
        this.responseMessage = this.language.error_message.invalid_pagenumber;
        setTimeout(() => {
          $('#responseMessage').hide();
          this.responseMessage = "";

        }, 3000);
      } else {
        this.currentPageNmuber = eve;
        this.getnewsData();
      }
    }
  }


  setItemPerPage(limit: any) {
    if (isNaN(limit)) {
      limit = this.itemPerPage;
    }
    this.itemPerPage = limit;
    this.getnewsData()
  }
}
