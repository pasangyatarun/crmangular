import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {


  addNewsForm: FormGroup;
  imageSrc: any;
  isSubmitted: any = false;
  checks = [
    { description: 'Feature', value: 'feature' },
    { description: "Training", value: 'training' },
    { description: "Team", value: 'team' },
    { description: "Event", value: 'event' },
    { description: "Various", value: 'various' },
    { description: "Video", value: 'video' },

  ];
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  message: any;
  language: any;
  msg: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.addNewsForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      url: [''],
      imageUpload: ['', Validators.required],
      videoUpload: [''],
      youtube_url: [''],
      author: [''],

    });

  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.addNewsForm.controls['url'].disable();
  }

  get formControls() { return this.addNewsForm.controls; }



  onivideoSelected(event: any) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.addNewsForm.patchValue({
          videoUpload: reader.result
        });
      };
    }
  }

  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;

      this.addNewsForm.patchValue({
        imageUpload: file
      });
      this.addNewsForm.controls["imageUpload"].updateValueAndValidity();
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    // $('#textPreview').show();
    // $('#textPreview').text(file.name);
  }

  onSubmit() {
    this.isSubmitted = true;
    let userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    var userId = userData[0].uid;
    this.addNewsForm.value.author = userId;
    this.isSubmitted = true;
    if (this.addNewsForm.invalid) {
      return;
    } else {
      var formData: any = new FormData();
      this.authService.setLoader(false);
      this.addNewsForm.value.imageUpload = this.imageSrc;
      let self = this;
      for (const key in this.addNewsForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.addNewsForm.value, key)) {
          const element = this.addNewsForm.value[key];

          if (key == 'imageUpload') {

            formData.append('file', self.imageSrc);
          }

          else {
            if ((key != 'imageUpload')) {
              formData.append(key, element);
            }
          }
        }

      }
      var endPoint = 'newscreate'
      this.authService.setLoader(true);
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.news;
          this.msg = this.result.content.messageList;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.news;
          setTimeout(() => {
            this.router.navigate(['/view-news']);
          }, 3000)
        }
      })
    }
  }

  diableField(event: any) {
    if (!!event.target.checked) {
      this.addNewsForm.controls['url'].enable()
    } else {
      this.addNewsForm.controls['url'].disable()
    }
  }

}
