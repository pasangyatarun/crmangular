import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
@Component({
  selector: 'app-news-description',
  templateUrl: './news-description.component.html',
  styleUrls: ['./news-description.component.css']
})
export class NewsDescriptionComponent implements OnInit {
  userData: any;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  id: any;
  EnterData: any;
  language: any;
  role: any;
  permissions: any;
  data: any;
  paginator: number = 1;
  success: any = false;
  error: any = false;
  displayFlag: any = 'en';

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.data = this.route.snapshot.paramMap.get('data');

  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getnewsData();
    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }
    this.displayFlag = localStorage.getItem('language');
  }

  onLanguageSelect(lan: any) {
    localStorage.setItem('language', lan);
    window.location.reload();
  }

  getnewsData() {
    var endPoint = 'newsbyurl/' + this.data;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.news;
      } else if (this.result.success == true) {
        this.error = false;
        this.msg = this.result.content.messageList.Signup;
        this.EnterData = this.result.content.dataList;
      }
    })
  }

}
