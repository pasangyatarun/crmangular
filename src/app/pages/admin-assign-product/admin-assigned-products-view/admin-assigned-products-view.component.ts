import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-admin-assigned-products-view',
  templateUrl: './admin-assigned-products-view.component.html',
  styleUrls: ['./admin-assigned-products-view.component.css']
})
export class AdminAssignedProductsViewComponent implements OnInit {
  message: any;
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  language: any;
  nameSearch: any;
  displayedColumns: string[] = [
    'customerName',
    'assignedProducts',
    'author',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  msg: any;
  permissions: any;
  role: any;
  userData: any;
  isData: any = true;

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
  }



  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getAssignedProducts();
    this.getuserData();
  }


  getAssignedProducts() {
    var endPoint = 'billwerk-product-adminassigned'
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.role;
        this.isData = false;
      } else if (this.result.success == true) {

        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;

        }
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    })
  }

  getuserData() {
    this.authService.setLoader(true);
    var endPoint = 'users'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.userData = this.result.content.dataList

      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  removeColumn() {
    if ((!this.authService.isAdmin()) && this.permissions.role && !this.permissions.role.includes('EDIT')) {
      this.columnsToDisplay.pop();
    }
  }


}
