import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAssignedProductsViewComponent } from './admin-assigned-products-view.component';

describe('AdminAssignedProductsViewComponent', () => {
  let component: AdminAssignedProductsViewComponent;
  let fixture: ComponentFixture<AdminAssignedProductsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAssignedProductsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAssignedProductsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
