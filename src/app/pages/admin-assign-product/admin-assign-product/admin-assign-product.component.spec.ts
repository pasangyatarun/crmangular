import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAssignProductComponent } from './admin-assign-product.component';

describe('AdminAssignProductComponent', () => {
  let component: AdminAssignProductComponent;
  let fixture: ComponentFixture<AdminAssignProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAssignProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAssignProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
