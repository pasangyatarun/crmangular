import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import { validateVerticalPosition } from '@angular/cdk/overlay';

@Component({
  selector: 'app-admin-assign-product',
  templateUrl: './admin-assign-product.component.html',
  styleUrls: ['./admin-assign-product.component.css']
})
export class AdminAssignProductComponent implements OnInit {

  dropdownList = [
    { item_id: 1, item_text: 'Sales-Partner' },
    { item_id: 2, item_text: 'Sales-Representative' },
  ];
  dropdownSettings = {
    singleSelection: true,
    idField: 'item_id',
    textField: 'item_text',
    enableCheckAll: false,
  };

  selectedItems: any = [];

  customerList: any = [];
  customerItems: any = [];
  customerdownSettings: any;

  productsList: any = [];
  productsItems: any = [];
  productsName: any = [];
  productsdownSettings: any;

  error: any;
  language: any;
  msg: any;
  products: any = false;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  visibility: any = 0;
  distributors: any;
  selectedData: any;
  result: any;
  assignForm: FormGroup;
  author: any;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router) {
    this.assignForm = this.formBuilder.group({
      assignedProducts: ['', Validators.required],
      userId: ['', Validators.required],
      adminId: [''],
      author: [''],
    });
  }

  get formControls() { return this.assignForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getproducts();
    this.getallCustomers();
    this.getUserData();

  }

  customerSelect(item: any) {
    this.customerItems = item.id;
  }

  customerDeSelect(items: any) {
    this.customerItems = ''
  }

  distSelect(items: any) {
    this.products = true;
  }

  productsSelect(items: any) {
    this.productsItems = [{ 'id': items.id, 'name': items.name }];
  }

  productsDeSelect(item: any) {
    this.productsItems = [];
  }

  productsSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.productsItems.push({ 'id': element.id });
      }
    }
  }

  productsDeSelectAll(item: any) {
    this.productsItems = [];
  }

  getproducts() {
    let self = this;
    var endPoint = 'billwerk-product';
    this.authService.setLoader(true);
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.result.content.dataList.forEach((val: any, key: any) => {
          if (val.planDetail.CustomFields.type == 1 || val.planDetail.CustomFields.type == 2 || val.planDetail.CustomFields.type == 0) {
            this.productsList.push({ 'id': val.Id, 'name': val.InternalName });
          }
        });
        self.productsdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          // selectAllText: 'Select All',
          enableCheckAll: true,
          // unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }

  getallCustomers() {
    let self = this;
    let customerData = {
      role: "Customer",
      status: "1"
    }
    var endPoint = 'users'
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;

      } else if (this.result.success == true) {
        this.error = false;
        this.message = this.result.content.messageList.user;
        this.result.content.dataList.forEach((val: any, key: any) => {
          val.role.forEach((element: any) => {
            if (element.role == 'Customer') {
              this.customerList.push({ 'id': val.uid, 'name': val.mail });
            }

          });
        });
        self.customerdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          // selectAllText: 'Select All',
          // enableCheckAll: true,
          // unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }

  getUserData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'userbyid/' + id[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.author = this.result.content.dataList;
      }
    })
  }

  onSubmit() {
    this.assignForm.controls['userId'].setValue(this.customerItems);
    this.assignForm.controls['assignedProducts'].setValue(this.productsItems);
    this.assignForm.controls['adminId'].setValue(this.author[0].uid);
    if (this.author && this.author[0].firstName != '') {
      this.assignForm.controls['author'].setValue(this.author[0].firstName + ' ' + this.author[0].lastName);
    } else {
      this.assignForm.controls['author'].setValue(this.author[0].mail);
    }
    this.isSubmitted = true;
    if (this.assignForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      var endPoint = 'billwerk-product-adminassigntocustomer'
      this.authService.sendRequest('post', endPoint, this.assignForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.role;
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.product
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.product
          setTimeout(() => {
            this.router.navigate(['/assigned-products']);
          }, 1000);

        }
      })
    }
  }

}
