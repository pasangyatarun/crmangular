import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';
import { ActivatedRoute } from '@angular/router';
import { billwerkPaymentMethod } from 'src/environments/environment';
import { mandateText } from 'src/environments/environment';
declare let $: any;

@Component({
  selector: 'app-product-component-details',
  templateUrl: './product-component-details.component.html',
  styleUrls: ['./product-component-details.component.css']
})
export class ProductComponentDetailsComponent implements OnInit {

  language: any;
  result: any;
  error: any;
  productDetails: any;
  productData: any
  msg: any;
  visibility: any = 0;
  customer: any;
  userOrderList: any;
  productId: any;
  isSubmitted: boolean = false;
  orderId: any;
  pdfButton: boolean = false;
  field: boolean = false;
  type: any;
  editProfile: any;
  commitData: any;
  clubOrderError!: string;
  errorMessage: boolean = false;
  successMessage: boolean = false;
  message: any;
  productComponent: any = [];
  paymentForm: FormGroup;
  isValidIban!: string;
  imageSrc: any;
  success: boolean = false;
  userData: any;
  paymentBearer: any;
  constructor(private formBuilder: FormBuilder, private router: Router, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {

    this.paymentForm = this.formBuilder.group({
      account_holder_name: ['', Validators.required],
      association: ['', Validators.required],
      iban: ['', Validators.required],
      termsCon: ['', Validators.required],
      chairman_name: [''],
      coupon_code: [''],
      mandate_text: ['', Validators.required],
      payment_method: [''],
      doc: ['', Validators.required]

    });
  }

  get formControls() { return this.paymentForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.productId = this.route.snapshot.paramMap.get('productId');
    this.getOrderData()
    this.getProductComponents(this.productId);
    this.getCustomersDetails();
    this.paymentForm.controls["payment_method"].setValue(billwerkPaymentMethod);


  }

  getProductComponents(productId: any) {
    var endPoint = 'crmproductsbyid/' + productId;
    // this.authService.setLoader(true);
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      // this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.productComponent = this.result.content.dataList;
      }
    })
  }

  checkAcceptOrNot(object: any) {
    if (this.userOrderList) {
      return this.userOrderList.some((obj: any) => obj.PlanVariantId === object.Id);
    } else {
      return false;
    }
  }


  validIban(event: any) {
    let validateIban = {
      iban: event.target.value
    }
    var endPoint: string = 'validate-iban';
    this.authService.sendRequest('post', endPoint, validateIban).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.isValidIban = this.result.content.messageList.iban;
      } else if (this.result.success == true) {
        this.isValidIban = '';
      }
    })
  }

  fieldVisibility(event: any) {
    if (event.target.checked) {
      this.field = true;
      this.paymentForm.controls['chairman_name'].setValidators([Validators.required]);
      this.paymentForm.controls['chairman_name'].updateValueAndValidity();
    } else if (!event.target.checked) {
      this.field = false;
    }
  }

  getMandateText(event: any) {
    if (event.target.checked) {
      var mandate = mandateText +
        '<p> chairman = ' + this.paymentForm.value.chairman_name + '</p>'
      this.paymentForm.controls["mandate_text"].setValue(mandate);
    } else if (!event.target.checked) {
      this.paymentForm.controls["mandate_text"].setValue('');
    }
  }

  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    this.imageSrc = file;
    this.paymentForm.patchValue({
      doc: file
    });
    this.paymentForm.controls["doc"].updateValueAndValidity();
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      $('#doc').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }

  getCustomersDetails() {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'userbyid/' + this.userData[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.customer = this.result.content.dataList;
        if (this.customer && this.customer[0].ibanValue) {
          this.paymentForm.controls["iban"].setValue(this.customer[0].ibanValue);

        }

      }
    })
  }

  onSubmit() {
    if (this.paymentForm.value.chairman_name) {
      var mandate = mandateText +
        '<p> chairman = ' + this.paymentForm.value.chairman_name + '</p>'
      this.paymentForm.controls["mandate_text"].setValue(mandate);
    }
    this.isSubmitted = true

    if (this.paymentForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      var formData: any = new FormData();
      let self = this;
      for (const key in this.paymentForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.paymentForm.value, key)) {
          const element = this.paymentForm.value[key];

          if (key == 'doc') {

            formData.append('file', self.imageSrc);
          }

          else {
            if ((key != 'image')) {
              formData.append(key, element);
            }
          }
        }

      }
      let uData = JSON.parse(localStorage.getItem('user-data') || '{}');
      var endPoint = 'crmproductOrder/' + uData[0].uid + '/' + this.productId
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        this.authService.setLoader(false);
        if (this.result.success == false) {
          if (this.result.content.messageList.Club) {
            this.clubOrderError = this.result.content.messageList.Club
          } else {
            this.errorMessage = true;
            this.successMessage = false;
            this.message = this.result.content.messageList.product;
          }
        } else if (this.result.success == true) {
          this.errorMessage = false;
          this.successMessage = true;
          this.message = this.result.content.messageList.product;
          setTimeout(() => {
            this.router.navigate(['/order-product-list']);
          }, 3000)
        }
      })
    }
  }

  getOrderData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'billwerk-orders/bycustomer/' + id[0].uid;
    this.authService.setLoader(true);
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.userOrderList = this.result.content.dataList;
        if (this.userOrderList.length > 0) {
          this.pamentBearer();
        }
      }

    })
  }

  pamentBearer() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'billwerk-allpaymentbearer-bcustomer/' + this.userOrderList[0].CustomerId;
    this.authService.setLoader(true);

    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        if(this.result.content.dataList.length > 0){
          this.result.content.dataList.forEach((element: any) => {
            if(element.MandateText){
              var chairman = element.MandateText.split('chairman =');
              element.chairman_name = chairman[1].split('</p>')[0]
            }
        });
        }
        this.paymentBearer = this.result.content.dataList.find((o: any) => o.IBAN == this.customer[0].ibanValue)
        if (this.paymentBearer) {
          this.paymentForm.controls["account_holder_name"].setValue(this.paymentBearer.Holder ? this.paymentBearer.Holder : '');
          this.paymentForm.controls["iban"].setValue(this.paymentBearer.IBAN ? this.paymentBearer.IBAN : '');
          this.paymentForm.controls["chairman_name"].setValue(this.paymentBearer.chairman_name ? this.paymentBearer.chairman_name : '');
        }
      }

    })
  }

}
