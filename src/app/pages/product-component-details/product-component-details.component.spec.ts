import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductComponentDetailsComponent } from './product-component-details.component';

describe('ProductComponentDetailsComponent', () => {
  let component: ProductComponentDetailsComponent;
  let fixture: ComponentFixture<ProductComponentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductComponentDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
