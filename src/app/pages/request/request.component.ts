import { Component, OnInit } from '@angular/core';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  language: any;

  constructor(private langService:LanguageService) {

   }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
  }

}
