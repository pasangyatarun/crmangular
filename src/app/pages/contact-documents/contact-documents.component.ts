import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-contact-documents',
  templateUrl: './contact-documents.component.html',
  styleUrls: ['./contact-documents.component.css']
})
export class ContactDocumentsComponent implements OnInit {


  isDoc: any = false
  language: any;
  documentForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  documents: any;
  extArr: any = [];
  viewImage = [];
  docExt: any = [];
  fileNameArr: any = [];
  doc: any = false;
  preDoc: any;
  documentData: any;
  role: any;

  constructor(private confirmBoxService: ConfirmDialogService, private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.documentForm = this.formBuilder.group({
      doc: [''],

    });
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.getAllDocuments();
  }


  open() {
    this.isDoc = true;
  }

  close() {
    $('#documentModal').modal('hide');
  }

  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    const reader = new FileReader();
    var imagePath = file;
    this.documentForm.patchValue({
      doc: file
    });
    this.documentForm.controls["doc"].updateValueAndValidity();
    reader.readAsDataURL(file);
    var url;
    reader.onload = (_event) => {
      url = reader.result;
    }

    let customer = JSON.parse(localStorage.getItem('user-data') || '{}');    
    this.isSubmitted = true;
    if (this.documentForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);
      var formData: any = new FormData();
      let self = this;
      for (const key in this.documentForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.documentForm.value, key)) {
          const element = this.documentForm.value[key];

          if (key == 'doc') {

            formData.append('file', imagePath);
          }
          else {
            if ((key != 'doc')) {
              formData.append(key, element);
            }
          }
        }
      }
      var endPoint = 'document-upload/'+ customer[0].uid
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.upload; 
          setTimeout(() => {
            this.error = false;
          }, 3000);
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.upload;
          setTimeout(() => {
            this.success = false;
            this.getAllDocuments();
          }, 3000);
        }
      })

    }
  }

  getAllDocuments(){
    this.authService.setLoader(true);
    var endPoint = 'get-user-documents'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.upload; 
        setTimeout(() => {
          this.error = false;
        }, 2000);
      } else if (this.result.success == true) {
        this.documents = this.result.content.dataList;
        this. getType();

      }
    })
  }

  getType() {
    for (const key in this.documents) {
      if (Object.prototype.hasOwnProperty.call(this.documents, key)) {
        const element = this.documents[key];
        var ext = element.path.split(".");
        this.extArr[key] = ext[(ext.length) - 1];
        var fileName = element.path.split("/");
        this.fileNameArr[key] = decodeURIComponent(fileName[(fileName.length) - 1]);
        var docExt = this.extArr[key];
      }
    }

  }

  
  previewImage(id:any) {
    $('#documentModal').modal('show');
    this.authService.setLoader(true);
    var endPoint = 'get-documentbyid/'+ id;
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.documentData = this.result.content.dataList;
       
      }
    })
  }

  download(path:any){
    var link = document.createElement('a');
    link.href = path;
    link.download = path;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  deleteDocs(id:any) {
    let self = this;
    self.confirmBoxService.confirmThis(this.language.common.deleteDocument, this.language.confirmBox.deleteDocumentMsg, function () {
      self.authService.setLoader(true);
      var endPoint = 'document-delete/' + id
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.upload;
          setTimeout(() => {
            self.error = false;
          }, 3000);
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.result.content.messageList.upload;
          setTimeout(() => {
            self.success = false;
            self.getAllDocuments();
          }, 3000);
        }
      });
    }, function () { }
    )
  }

}
