import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-create-email-template',
  templateUrl: './create-email-template.component.html',
  styleUrls: ['./create-email-template.component.css']
})
export class CreateEmailTemplateComponent implements OnInit {

  emailTemplateForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  file: any;
  language: any;
  imageSrc: any;
  templateType = [
    { value: 'Registration' },
    { value: 'Trial Registration' },
    { value: 'Password Reset' },
    { value: 'Survey Create' },
    { value: 'Add User' },
    { value: 'Contact Admin' },
    { value: 'Contact Admin(User)' },
    { value: 'Payment Escalation' },
  ];

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.emailTemplateForm = this.formBuilder.group({
      templateType: ['', Validators.required],
      subject: ['', Validators.required],
      file: ['', Validators.required],
      url: ['', Validators.required],
      headerContent: ['', Validators.required],
      templateBody: ['', Validators.required],
      footerContent: ['', Validators.required],
    });


  }

  get formControls() { return this.emailTemplateForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
  }




  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
      // this.emailTemplateForm.controls["image"].setValue(this.imageSrc);
      this.emailTemplateForm.patchValue({
        file: file
      });
      this.emailTemplateForm.controls["file"].updateValueAndValidity();
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }

  onSubmit() {
    this.authService.setLoader(true);
    this.isSubmitted = true;

    if (this.emailTemplateForm.invalid) {
      this.authService.setLoader(false);

      return;
    } else {
      var formData: any = new FormData();
      let self = this;
      for (const key in this.emailTemplateForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.emailTemplateForm.value, key)) {
          const element = this.emailTemplateForm.value[key];

          if (key == 'file') {

            formData.append('file', self.imageSrc);
          }

          else {
            if ((key != 'file')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      var endPoint = 'emailtemplatecreate'
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.emailTemplate;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.emailTemplate;
          setTimeout(() => {
            this.router.navigate(['/email-template'])
          }, 3000)
        }
      })
    }
  }


}
