import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {ConfirmDialogService} from 'src/app/confirm-dialog/confirm-dialog.service';

declare var $: any;

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.css']
})
export class EmailTemplateComponent implements OnInit {
  currentPageNmuber: number = 1;
  itemPerPage = 8;
  limitPerPage = [
    { value: '8' },
    { value: '16' },
    { value: '24' },
    { value: '32' },
    { value: '40' }
  ];

  totalRecord = 0;
  language: any;
  userDetails: any;
  userRole: any;
  emailTemplateData: any;
  responseMessage: any;
  role: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  message: any;
  templateId: any;
  permissions: any;

  constructor(private confirmBoxService:ConfirmDialogService, public authService: AuthService, private lang: LanguageService) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.userDetails = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');    
    this.authService.setLoader(false);
    this.getEmailTemplate();
  }

  getEmailTemplate() {
      this.authService.setLoader(true);
      this.authService.sendRequest('get', 'emailtemplates', null).subscribe(
        (respData: any) => {
          this.result = respData;
          this.authService.setLoader(false);
            this.emailTemplateData = this.result.content.dataList;
            this.totalRecord = this.result.content.dataList.length;
        }
      )

  }


  pageChanged(event: any) {
    this.currentPageNmuber = event;
    this.getEmailTemplate();
  }

  goToPg(eve: any) {
    this.responseMessage = null;
    if (isNaN(eve)) {
      eve = this.currentPageNmuber;
    } else {
      if (eve > Math.round(this.totalRecord / this.itemPerPage)) {
        this.responseMessage = this.language.productPrice.toOrder;
        setTimeout(() => {
          $('#responseMessage').hide();
          this.responseMessage = "";

        }, 3000);
      } else {
        this.currentPageNmuber = eve;
        this.getEmailTemplate();
      }
    }
  }


  setItemPerPage(limit: any) {
    if (isNaN(limit)) {
      limit = this.itemPerPage;
    }
    this.itemPerPage = limit;
    this.getEmailTemplate()
  }




  deletetemplate(templateId:any){
    let self = this;
    self.confirmBoxService.confirmThis(this.language.emailTemplate.delete_email,this.language.confirmBox.delete_email, function () {

      self.authService.setLoader(true);
    var endPoint = 'emailtemplatedelete/' + templateId
    self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
      self.result = result
      self.authService.setLoader(false);
      if (self.result.success == false) {
        self.error = true;
        self.success = false;
        self.message = self.result.content.messageList.emailTemplate;
      } else if (self.result.success == true) {
        self.error = false;
        self.success = true;
        self.message = self.result.content.messageList.emailTemplate;
        setTimeout(() => {
          self.getEmailTemplate();
        }, 3000)
      }
    })
     

    }, function () { }
    )

}
}
