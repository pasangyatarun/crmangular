import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
declare let $: any;

@Component({
  selector: 'app-member-management',
  templateUrl: './member-management.component.html',
  styleUrls: ['./member-management.component.css']
})
export class MemberManagementComponent implements OnInit {


  memberData: any = [];
  displayedColumnsMember: string[] = [
    'firstName',
    'lastName',
    'Email',
    'DOB',
    'gender',
    'mobile',
    'status',
    'Edit-Member',
  ];

  displayedColumnsAll: string[] = [
    'firstName',
    'lastName',
    'companyName',
    'responsiblePerson',
    'Email',
    'DOB',
    'gender',
    'mobile',
    'status',
    'Edit-Member',
  ];

  displayedColumnsCompany: string[] = [
    'companyName',
    'responsiblePerson',
    'Email',
    'gender',
    'mobile',
    'status',
    'Edit-Member',
  ];

  columnsToDisplay: string[] = this.displayedColumnsMember.slice();
  columnsToDisplayAll: string[] = this.displayedColumnsAll.slice();
  columnsToDisplaycompany: string[] = this.displayedColumnsCompany.slice();

  dataSource !: MatTableDataSource<any>;
  error: any = false;
  result: any;
  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator) matpaginator!: MatPaginator;
  @ViewChild(MatSort) matsort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  isData: any = true;
  displayMembers: boolean = true;
  displayStatistics: boolean = false;
  gender: any = true;
  age: any = true;
  appUse: any = true;
  status: any = true;
  filterType: any = '';
  filterForm: FormGroup;
  filterData: any = 'member';

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
    this.filterForm = this.formBuilder.group({
      type: [''],
    });
  }

  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.filterForm.controls["type"].setValue('member');
    this.getmemberData();
  }

  isCustomer() {
    return this.role.some((o: any) => o.name == 'SGP-Wechsler' || o.role == 'SGP-Wechsler' || o.name == 'Customer' || o.role == 'Customer');
  }

  getmemberData() {
    this.authService.setLoader(true);
    let id = JSON.parse(localStorage.getItem('user-data') || '{}');
    var endPoint = 'customer-member-filter/' + 'member' + '/customer/' + id[0].uid
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
        this.isData = false;
      } else if (this.result.success == true) {
        this.memberData = this.result.content.dataList;
        this.result.content.dataList.forEach((element: any) => {
          element.status = element.status == '1' ? this.language.viewUser.active : 'Inactive'
        });
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;

        }
        this.dataSource.paginator = this.matpaginator;
        this.dataSource.sort = this.matsort;
      }
    })

  }

  onSubmit() {
    this.authService.setLoader(true);
    this.isSubmitted = true;
    if (this.filterForm.invalid) {
      return;
    } else {
      let id = JSON.parse(localStorage.getItem('user-data') || '{}');
      var endPoint = 'customer-member-filter/' + this.filterForm.value.type + '/customer/' + id[0].uid
      this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.user;
          this.dataSource = new MatTableDataSource();
          this.dataSource.paginator = this.matpaginator;
          this.dataSource.sort = this.matsort;
          this.isData = false;
        } else if (this.result.success == true) {
          this.memberData = this.result.content.dataList;
          this.filterForm.controls["type"].setValue(this.filterForm.value.type);
          this.filterData = this.filterForm.value.type
          this.result.content.dataList.forEach((element: any) => {
            element.status = element.status == '1' ? this.language.viewUser.active : 'Inactive'
          });
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          if (this.dataSource.data.length) {
            this.isData = true;
          } else {
            this.isData = false;

          }
          this.dataSource.paginator = this.matpaginator;
          this.dataSource.sort = this.matsort;

        }
      })
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


  onTabChange(id: any) {
    $('.tab-pane').removeClass('active');
    $('.nav-link').removeClass('active');
    if (id == 1) {
      this.displayMembers = true;
      this.displayStatistics = false;
      $('#tabs-1').addClass('active');
      $('#head-allTask').addClass('active');
    }
    if (id == 2) {
      this.displayMembers = false;
      this.displayStatistics = true;
      $('#tabs-2').addClass('active');
      $('#head-personalTask').addClass('active');
    }
  }

  filter(event: any) {
    this.filterType = event.target.value;
    if (event.target.value == 'All') {
      this.filterType = ''
    }

  }

  reset() {
    this.error = false;
    this.success = false;
    this.message = '';
    this.getmemberData();
  }

}
