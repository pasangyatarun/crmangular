import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-departments-list',
  templateUrl: './departments-list.component.html',
  styleUrls: ['./departments-list.component.css']
})
export class DepartmentsListComponent implements OnInit {

  departmentForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  error: any = false;
  success: any = false;
  errorModal: any = false;
  successModal: any = false;
  show: any = false;
  language: any;
  nameSearch: any;
  displayedColumns: string[] = [
    'Name',
    'Edit',
    'Delete',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  msg: any;
  permissions: any;
  role: any;
  userData: any;
  isData:any = true ;
  
  constructor(private confirmBoxService: ConfirmDialogService,private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.departmentForm = this.formBuilder.group({
      department: ['', Validators.required],
      customerId:['']
    });
  }

  get formControls() { return this.departmentForm.controls; }


  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getDepartmentlist();
    this.getuserData();
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.departmentForm.invalid) {
      return;
    } else {
      let uData = JSON.parse(localStorage.getItem('user-data') || '{}');
      this.departmentForm.controls['customerId'].setValue(uData[0].uid);
      this.authService.setLoader(true);
      var endPoint = 'customer-department-add'
      this.authService.sendRequest('post', endPoint, this.departmentForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
            this.errorModal = true;
            this.successModal = false;
            this.message = this.result.content.messageList.department;
        } else if (this.result.success == true) {
          this.errorModal = false;
          this.successModal = true;
          this.message = this.result.content.messageList.department;
          setTimeout(() => {
            this.isSubmitted = false;
            this.successModal =false;
            this.errorModal =false;
            this.departmentForm.reset();
            $('#department').modal('hide');
            this.ngOnInit();
          }, 1000)
        }
      })
    }
  }

  getDepartmentlist() {
    let id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'customer-departments/' + id[0].uid
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        // this.error = true;
        // this.success = false;
        // this.message = this.result.content.messageList.department ;
        this.isData = false;
      } else if (this.result.success == true) {
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if(this.dataSource.data.length){
          this.isData = true;
        }else{
          this.isData = false;
        }
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    })
  }

  getuserData() {
    this.authService.setLoader(true);
    var endPoint = 'users'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.userData = this.result.content.dataList

      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort){
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  
  removeColumn() {
    if (!this.authService.isAdmin() && this.permissions.role && !this.permissions.role.includes('EDIT')) {
      this.columnsToDisplay.pop();
    }
  }

  openModal(){
    $('#department').modal('show');
  }

  closeModal() {
    $('#department').modal('hide');
  }

  deleteDepartment(id:any) {
    let self = this;
    self.confirmBoxService.confirmThis('Delete Department', 'Are you sure you want to delete this department?', function () {

      self.authService.setLoader(true);
      var endPoint = 'customer-departments-delete/' + id
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.department;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.result.content.messageList.department;
          setTimeout(() => {
            self.success =false;
            self.error =false;
            self.getDepartmentlist();
          }, 2000)
        }
      })


    }, function () { }
    )

  }

}
