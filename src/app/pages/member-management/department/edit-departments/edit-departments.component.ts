import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
declare let $: any;

@Component({
  selector: 'app-edit-departments',
  templateUrl: './edit-departments.component.html',
  styleUrls: ['./edit-departments.component.css']
})
export class EditDepartmentsComponent implements OnInit {


  editDepartmentForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  id: any;
  reEnterData: any;
  role: any;
  roleData: any;
  error: any = false;
  success: any = false;
  language: any;
  permissions: any;

  constructor( private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editDepartmentForm = this.formBuilder.group({
      department: [''],
      customerId: [''],

    });

    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
  }


  get formControls() { return this.editDepartmentForm.controls; }

  get f() {
    return this.editDepartmentForm.controls;
  }


  ngOnInit(): void {

    this.language = this.langService.getLanguaageFile();
    this.getDepartment();

  }



  onSubmit() {

    this.isSubmitted = true;
    if (this.editDepartmentForm.invalid) {
      return;
    } else {
      let uData = JSON.parse(localStorage.getItem('user-data') || '{}');
      this.editDepartmentForm.controls['customerId'].setValue(uData[0].uid);
      this.authService.setLoader(true);
      var endPoint = 'customer-department-update/' + this.id
      this.authService.sendRequest('put', endPoint, this.editDepartmentForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.department;
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.department;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.department;
          setTimeout(() => {
            this.router.navigate(['/department-list']);
          }, 3000)
        }
      })
    }
  }


  getDepartment() {
    var endPoint = 'customer-departmentbyid/' + this.id
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.reEnterData = this.result.content.dataList;
        this.reEnterValue();
      }
    })
  }

  reEnterValue() {
    this.editDepartmentForm.controls["department"].setValue(this.reEnterData[0].department_name);

  }


}
