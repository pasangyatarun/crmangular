import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.css']
})
export class AddMemberComponent implements OnInit {

  addMemberForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  file: any;
  id: any;
  language: any;
  imageSrc: any;
  department: any;
  isData: any = true;
  companyName: boolean = false;
  fieldDisable: any = true
  errorDate: any = { isError: false, errorMessage: '' };
  departmentList: any = [];
  departmentItems: any = [];
  departmentSettings: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {

    this.addMemberForm = this.formBuilder.group({
      salutation: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      gender: ['', Validators.required],
      mobile: [''],
      phone: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[0-9]+')])],
      street: ['', Validators.required],
      zipcode: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      city: ['', Validators.required],
      DOB: ['', Validators.required],
      status: ['', Validators.required],
      functionClub: ['', Validators.required],
      department: ['', Validators.required],
      departmentEntryDate: ['', Validators.required],
      clubEntryDate: ['', Validators.required],
      clubExitDate: [''],
      reasonForExit: [''],
      notes: [''],
      appUse: [''],
      image: [''],
      companyName: [''],
      responsiblePerson: [''],
      customerId: ['']
    });


  }


  get formControls() { return this.addMemberForm.controls; }

  get f() {
    return this.addMemberForm.controls;
  }

  ngOnInit(): void {
    this.addMemberForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
    this.language = this.langService.getLanguaageFile();
    this.getAuthors();
    this.getDepartmentlist();
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  getAuthors() {
    let uData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.addMemberForm.controls['customerId'].setValue(uData[0].uid);
  }

  salutationValue(event: any) {
    if (event.target.value == 'Company') {
      this.companyName = true;
      this.addMemberForm.controls['companyName'].setValidators([Validators.required]);
      this.addMemberForm.controls['companyName'].updateValueAndValidity();
      this.addMemberForm.controls['responsiblePerson'].setValidators([Validators.required]);
      this.addMemberForm.controls['responsiblePerson'].updateValueAndValidity();
      this.addMemberForm.controls['firstName'].clearValidators();
      this.addMemberForm.controls['firstName'].updateValueAndValidity();
      this.addMemberForm.controls['lastName'].clearValidators();
      this.addMemberForm.controls['lastName'].updateValueAndValidity();
      this.addMemberForm.controls['DOB'].clearValidators();
      this.addMemberForm.controls['DOB'].updateValueAndValidity();
    } else {
      this.companyName = false;
      this.addMemberForm.controls['companyName'].clearValidators();
      this.addMemberForm.controls['companyName'].updateValueAndValidity();
      this.addMemberForm.controls['responsiblePerson'].clearValidators();
      this.addMemberForm.controls['responsiblePerson'].updateValueAndValidity();
      this.addMemberForm.controls['firstName'].setValidators([Validators.required])
      this.addMemberForm.controls['firstName'].updateValueAndValidity();
      this.addMemberForm.controls['lastName'].setValidators([Validators.required])
      this.addMemberForm.controls['lastName'].updateValueAndValidity();
      this.addMemberForm.controls['DOB'].setValidators([Validators.required])
      this.addMemberForm.controls['DOB'].updateValueAndValidity();
    }

  }

  compareDates(event: any) {
    if (new Date(this.addMemberForm.controls['clubEntryDate'].value) < new Date(this.addMemberForm.controls['departmentEntryDate'].value)) {
      this.errorDate = {
        isError: true, errorMessage: "Club entry date can't before department entry date"
      };

    } else {
      this.errorDate = { isError: false, errorMessage: '' };
    }

    // if (new Date(this.addMemberForm.controls['clubExitDate'].value) < new Date(this.addMemberForm.controls['clubEntryDate'].value)) {
    // 	this.errorDate = {
    // 		isError: true, errorMessage: "Club Exit Date can't before Club entry date"
    // 	};

    // }else {
    // 	this.errorDate = { isError: false, errorMessage: '' };
    // }

  }



  getDepartmentlist() {
    let self = this;
    let id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'customer-departments/' + id[0].uid
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {        
        // this.error = true;
        // this.success = false;
        // this.message = this.result.content.messageList.department ;        
        this.isData = false;
      } else if (this.result.success == true) {
        this.isData = true;
        this.department = this.result.content.dataList;
        if(this.department && this.department.length){
          this.result.content.dataList.forEach((val: any, key: any) => {
            // this.users = this.users.concat({item_id:1,item_text:"test"})
            this.departmentList.push({ 'id': val.id, 'name': val.department_name });
          });
          self.departmentSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            enableCheckAll: true,
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };
        }

      }
    })
  }

  departmentSelect(items: any) {
    this.departmentItems.push(items.name);
  }

  departmentDeSelect(item: any) {

    const index = this.departmentItems.indexOf(item.name);
    if (index > -2 || -1) {
      this.departmentItems.splice(index, 1);
    }
  }

  departmentSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.departmentItems.push(element.name);
      }
    }
  }

  departmentDeSelectAll(item: any) {
    this.departmentItems = [];
  }

  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
      // this.addMemberForm.controls["image"].setValue(this.imageSrc);
      this.addMemberForm.patchValue({
        image: file
      });
      this.addMemberForm.controls["image"].updateValueAndValidity();
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }

  onSubmit() {
    this.isSubmitted = true;    
    if (this.addMemberForm.invalid || this.errorDate.isError == true) {
      return;
    } else {
      this.addMemberForm.controls["department"].setValue(this.departmentItems);
      if(this.addMemberForm.value.salutation == 'Company'){
        this.addMemberForm.controls['firstName'].setValue('');
        this.addMemberForm.controls['lastName'].setValue('');
        this.addMemberForm.controls['DOB'].setValue('');
      }else{
        this.addMemberForm.controls['companyName'].setValue('');
        this.addMemberForm.controls['responsiblePerson'].setValue('');
      }

      this.authService.setLoader(true);
      var formData: any = new FormData();
      let self = this;
      for (const key in this.addMemberForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.addMemberForm.value, key)) {
          const element = this.addMemberForm.value[key];

          if (key == 'image' && element != undefined && element != '') {

            formData.append('file', self.imageSrc);
          }

          if (key == 'department') {

            formData.append('department', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'department')) {
              formData.append(key, element);
            }
          }
        }

      }
      var endPoint = 'customer-members-add'
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.member;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.member;
          setTimeout(() => {
            this.router.navigate(['/member-management']);
          }, 3000)
        }
      })
    }
  }

}
