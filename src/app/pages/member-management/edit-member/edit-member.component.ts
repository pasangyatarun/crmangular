import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-edit-member',
  templateUrl: './edit-member.component.html',
  styleUrls: ['./edit-member.component.css']
})
export class EditMemberComponent implements OnInit {

  editMemberForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  id: any;
  reEnterData: any;
  role: any;
  roleData: any;
  error: any = false;
  success: any = false;
  language: any;
  domain: any;
  getRole: any;
  status: any;
  checked: any;
  permissions: any;
  imageSrc: any;
  hasPicture = false;
  imagePreview: any;
  checkedAppUse: any;
  department: any;
  companyName: boolean = false;
  departmentList: any = [];
  departmentItems: any = [];
  departmentSettings: any;
  deptList: any = [];
  constructor(private confirmBoxService: ConfirmDialogService, private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editMemberForm = this.formBuilder.group({
      salutation: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      gender: ['', Validators.required],
      mobile: [''],
      phone: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[0-9]+')])],
      street: ['', Validators.required],
      zipcode: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      city: ['', Validators.required],
      DOB: ['', Validators.required],
      status: ['', Validators.required],
      functionClub: ['', Validators.required],
      department: ['', Validators.required],
      departmentEntryDate: ['', Validators.required],
      clubEntryDate: ['', Validators.required],
      clubExitDate: [''],
      reasonForExit: [''],
      notes: [''],
      appUse: [''],
      image: [''],
      companyName: [''],
      responsiblePerson: [''],
      customerId: ['']
    });
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
  }

  get formControls() { return this.editMemberForm.controls; }

  get f() {
    return this.editMemberForm.controls;
  }


  ngOnInit(): void {
    this.mustMatch();
    this.getUserData();
    this.language = this.langService.getLanguaageFile();
  }

  mustMatch() {
    this.editMemberForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  salutationValue(event: any) {
    if (event.target.value == 'Company') {
      this.companyName = true;
      this.editMemberForm.controls['companyName'].setValidators([Validators.required]);
      this.editMemberForm.controls['companyName'].updateValueAndValidity();
      this.editMemberForm.controls['responsiblePerson'].setValidators([Validators.required]);
      this.editMemberForm.controls['responsiblePerson'].updateValueAndValidity();
      this.editMemberForm.controls['firstName'].clearValidators();
      this.editMemberForm.controls['firstName'].updateValueAndValidity();
      this.editMemberForm.controls['lastName'].clearValidators();
      this.editMemberForm.controls['lastName'].updateValueAndValidity();
      this.editMemberForm.controls['DOB'].clearValidators();
      this.editMemberForm.controls['DOB'].updateValueAndValidity();
    } else {
      this.companyName = false;
      this.editMemberForm.controls['companyName'].clearValidators();
      this.editMemberForm.controls['companyName'].updateValueAndValidity();
      this.editMemberForm.controls['responsiblePerson'].clearValidators();
      this.editMemberForm.controls['responsiblePerson'].updateValueAndValidity();
      this.editMemberForm.controls['firstName'].setValidators([Validators.required])
      this.editMemberForm.controls['firstName'].updateValueAndValidity();
      this.editMemberForm.controls['lastName'].setValidators([Validators.required])
      this.editMemberForm.controls['lastName'].updateValueAndValidity();
      this.editMemberForm.controls['DOB'].setValidators([Validators.required])
      this.editMemberForm.controls['DOB'].updateValueAndValidity();
    }

  }

  getDepartmentlist() {
    let self = this;
    let id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'customer-departments/' + id[0].uid
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        // this.error = true;
        // this.success = false;
        // this.message = this.result.content.messageList.department ;        
      } else if (this.result.success == true) {
        this.department = this.result.content.dataList;
        this.result.content.dataList.forEach((val: any, key: any) => {
          // this.users = this.users.concat({item_id:1,item_text:"test"})
          this.departmentList.push({ 'id': val.id, 'name': val.department_name });
        });
        self.departmentSettings = {
          singleSelection: false,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: true,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };


        this.departmentList.forEach((element: any) => {
          JSON.parse(this.reEnterData[0].department).forEach((ele: any) => {
            if (element.name == ele) {
              this.departmentItems.push(element.name)
              this.deptList.push(element)

              this.editMemberForm.controls["department"].setValue(this.deptList);
            }
          });
        });
      }
    })
  }

  departmentSelect(items: any) {
    this.departmentItems.push(items.name);
  }

  departmentDeSelect(item: any) {

    const index = this.departmentItems.indexOf(item.name);
    if (index > -2 || -1) {
      this.departmentItems.splice(index, 1);
    }
  }

  departmentSelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.departmentItems.push(element.name);
      }
    }
  }

  departmentDeSelectAll(item: any) {
    this.departmentItems = [];
  }


  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.editMemberForm.invalid) {
      return;
    } else {

      if (this.departmentItems && this.departmentItems.length) {
        this.editMemberForm.controls["department"].setValue(this.departmentItems);

      } else if (this.deptList) {
        this.deptList.forEach((val: any) => {
          this.deptList.push(val.name)
        })
        this.editMemberForm.controls["department"].setValue(this.deptList);
      }

      if(this.editMemberForm.value.salutation == 'Company'){
        this.editMemberForm.controls['firstName'].setValue('');
        this.editMemberForm.controls['lastName'].setValue('');
        this.editMemberForm.controls['DOB'].setValue('');
      }else{
        this.editMemberForm.controls['companyName'].setValue('');
        this.editMemberForm.controls['responsiblePerson'].setValue('');
      }


      var formData: any = new FormData();
      this.authService.setLoader(true);
      this.editMemberForm.value.image = this.imageSrc;
      let self = this;
      for (const key in this.editMemberForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.editMemberForm.value, key)) {
          const element = this.editMemberForm.value[key];

          if (key == 'image' && element != undefined && element != '') {
            if (self.imageSrc) {
              formData.append('file', self.imageSrc);
            } else {
              formData.append('imageUrl', self.imagePreview);
            }
          }
          if (key == 'department') {

            formData.append('department', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'department')) {
              formData.append(key, element);
            }
          }
        }
      }
      var endPoint = 'customer-members-update/' + this.id
      this.authService.sendRequest('put', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);

        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          if (this.msg == null || this.msg == "") {
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.member;
          }
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.member;
          setTimeout(() => {
            this.router.navigate(['/member-management']);
          }, 1000)

        }
      })
    }
  }


  getUserData() {
    this.authService.setLoader(true);
    var endPoint = 'customer-members/' + this.id;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.reEnterData = this.result.content.dataList;
        this.getDepartmentlist();
        this.reEnterValue();
      }
    })
  }

  reEnterValue() {
    if (this.reEnterData && this.reEnterData[0].company_name) {
      this.companyName = true;
      this.editMemberForm.controls['companyName'].setValidators([Validators.required]);
      this.editMemberForm.controls['companyName'].updateValueAndValidity();
      this.editMemberForm.controls['responsiblePerson'].setValidators([Validators.required]);
      this.editMemberForm.controls['responsiblePerson'].updateValueAndValidity();
      this.editMemberForm.controls['firstName'].clearValidators();
      this.editMemberForm.controls['firstName'].updateValueAndValidity();
      this.editMemberForm.controls['lastName'].clearValidators();
      this.editMemberForm.controls['lastName'].updateValueAndValidity();
      this.editMemberForm.controls['DOB'].clearValidators();
      this.editMemberForm.controls['DOB'].updateValueAndValidity();
    } else {
      this.companyName = false;
      this.editMemberForm.controls['companyName'].clearValidators();
      this.editMemberForm.controls['companyName'].updateValueAndValidity();
      this.editMemberForm.controls['responsiblePerson'].clearValidators();
      this.editMemberForm.controls['responsiblePerson'].updateValueAndValidity();
      this.editMemberForm.controls['firstName'].setValidators([Validators.required])
      this.editMemberForm.controls['firstName'].updateValueAndValidity();
      this.editMemberForm.controls['lastName'].setValidators([Validators.required])
      this.editMemberForm.controls['lastName'].updateValueAndValidity();
    }
    this.editMemberForm.controls["salutation"].setValue(this.reEnterData[0].salutation);
    this.editMemberForm.controls["firstName"].setValue(this.reEnterData[0].firstname);
    this.editMemberForm.controls["lastName"].setValue(this.reEnterData[0].lastname);
    this.editMemberForm.controls["companyName"].setValue(this.reEnterData[0].company_name);
    this.editMemberForm.controls["responsiblePerson"].setValue(this.reEnterData[0].responsible_person);
    this.editMemberForm.controls["email"].setValue(this.reEnterData[0].email);
    this.editMemberForm.controls["DOB"].setValue(this.reEnterData[0].birth_date);
    this.editMemberForm.controls["gender"].setValue(this.reEnterData[0].gender);
    this.editMemberForm.controls["mobile"].setValue(this.reEnterData[0].mobile);
    this.editMemberForm.controls["zipcode"].setValue(this.reEnterData[0].zipcode);
    this.editMemberForm.controls["city"].setValue(this.reEnterData[0].city);
    this.editMemberForm.controls["street"].setValue(this.reEnterData[0].street);
    this.editMemberForm.controls["phone"].setValue(this.reEnterData[0].phone);
    this.editMemberForm.controls["notes"].setValue(this.reEnterData[0].notes);
    this.editMemberForm.controls["functionClub"].setValue(this.reEnterData[0].function);
    this.editMemberForm.controls["reasonForExit"].setValue(this.reEnterData[0].reason_for_exit);
    this.editMemberForm.controls["clubEntryDate"].setValue(this.reEnterData[0].club_entry_date);
    this.editMemberForm.controls["clubExitDate"].setValue(this.reEnterData[0].club_exit_date);
    this.editMemberForm.controls["clubExitDate"].setValue(this.reEnterData[0].club_exit_date);
    this.editMemberForm.controls["departmentEntryDate"].setValue(this.reEnterData[0].department_entry_date);
    this.editMemberForm.controls["status"].setValue(this.reEnterData[0].status);
    this.editMemberForm.controls["appUse"].setValue(this.reEnterData[0].app_in_use);
    this.checked = this.reEnterData[0].status;    
    this.checkedAppUse = this.reEnterData[0].app_in_use;
    if (this.reEnterData[0].image && this.reEnterData[0].image != 'undefined') {
      this.hasPicture = true;
      this.imagePreview = this.reEnterData[0].image;
    }

  }

  deleteUser() {
    let self = this;
    self.confirmBoxService.confirmThis(this.language.memberManagement.deleteMemeber, this.language.confirmBox.deleteConfMsg, function () {
      self.authService.setLoader(true);
      var endPoint = 'customer-members-delete/' + self.id
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.member;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.result.content.messageList.member;
          setTimeout(() => {
            self.router.navigate(['/member-management'])
          }, 3000)
        }
      });
    }, function () { }
    )
  }


}
