import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-product-assign',
  templateUrl: './product-assign.component.html',
  styleUrls: ['./product-assign.component.css']
})
export class ProductAssignComponent implements OnInit {
  dropdownList = [
    { item_id: 1, item_text: 'Sales-Partner' },
    { item_id: 2, item_text: 'Sales-Representative' },
  ];
  dropdownSettings = {
    singleSelection: true,
    idField: 'item_id',
    textField: 'item_text',
    enableCheckAll: false,
  };

  selectedItems: any = [];

  salesList: any = [];
  salesItems: any = [];
  salesdownSettings: any;

  distributerList: any = [];
  distributerItems: any = [];
  distributerdownSettings: any;


  productsList: any = [];
  productsItems: any = [];
  productsdownSettings: any;

  error: any;
  language: any;
  msg: any;
  products: any = false;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  visibility: any = 0;
  distributors: any;
  selectedData: any;
  result: any;
  assignForm: FormGroup;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router) {
    this.assignForm = this.formBuilder.group({
      assignedProducts: [''],
      distributor_id: [''],
      sales_id: [''],

    });
  }

  get formControls() { return this.assignForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getSales();
    this.getDistributors();
    this.getproducts();
  }

  onItemSelect(item: any) {
    if (item.item_text == "Sales-Representative") {
      this.visibility = 1
      this.assignForm.controls['sales_id'].setValidators([Validators.required])
      this.assignForm.controls['sales_id'].updateValueAndValidity()
      this.assignForm.controls['distributor_id'].clearValidators();
      this.assignForm.controls['distributor_id'].updateValueAndValidity();
    } else if (item.item_text == "Sales-Partner") {
      this.visibility = 2
      this.assignForm.controls['distributor_id'].setValidators([Validators.required])
      this.assignForm.controls['distributor_id'].updateValueAndValidity()
      this.assignForm.controls['sales_id'].clearValidators();
      this.assignForm.controls['sales_id'].updateValueAndValidity();
    }
  }

  onDeSelect(items: any) {
    this.visibility = 0
  }

  salesSelect(item: any) {
    this.products = true;
    this.assignForm.controls['assignedProducts'].setValidators([Validators.required])
    this.assignForm.controls['assignedProducts'].updateValueAndValidity()
    this.salesItems.push(item.id);
  }

  salesDeSelect(items: any) {
    this.products = false;
    const index = this.salesItems.indexOf(items.id);
    if (index > -1) {
      this.salesItems.splice(index, 1);
    }
  }

  distSelect(items: any) {
    this.products = true;
    this.assignForm.controls['assignedProducts'].setValidators([Validators.required])
    this.assignForm.controls['assignedProducts'].updateValueAndValidity()
    this.distributerItems.push(items.id);
  }


  distDeSelect(item: any) {
    this.products = false;
    const index = this.distributerItems.indexOf(item.id);
    if (index > -1) {
      this.distributerItems.splice(index, 1);
    }
  }

  productsSelect(items: any) {
    this.productsItems.push({ 'id': items.id });
  }


  productsDeSelect(item: any) {
    this.assignForm.controls['assignedProducts'].setValidators([Validators.required])
    this.assignForm.controls['assignedProducts'].updateValueAndValidity()
    const index = this.productsItems.indexOf(item.id);
    if (index > -2 || -1) {
      this.productsItems.splice(index, 1);
    }
  }


  productsSelectAll(item: any) {
    this.assignForm.controls['assignedProducts'].clearValidators();
    this.assignForm.controls['assignedProducts'].updateValueAndValidity();
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.productsItems.push({ 'id': element.id });
      }
    }
  }


  productsDeSelectAll(item: any) {
    this.assignForm.controls['assignedProducts'].setValidators([Validators.required])
    this.assignForm.controls['assignedProducts'].updateValueAndValidity()
    this.productsItems = [];
  }

  getDistributors() {
    let self = this;
    var endPoint = 'distributors';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.distributerList.push({ 'id': val.uid, 'name': val.mail });
        });
        
        self.distributerdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

      }
    })
  }

  getSales() {
    let self = this;
    var endPoint = 'salesusers';
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.salesList.push({ 'id': val.uid, 'name': val.mail });
        });
        self.salesdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }

  getproducts() {
    let self = this;
    var endPoint = 'products';
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.productsList.push({ 'id': val.pid, 'name': val.productName });
        });
        self.productsdownSettings = {
          singleSelection: false,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: true,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.assignForm.invalid) {
      return;
    } else {
      if (this.selectedItems[0].item_text == 'Sales-Partner') {
        this.assignForm.controls["sales_id"].setValue(this.salesItems);
        this.assignForm.controls["assignedProducts"].setValue(this.productsItems);
        var endPoint = 'productassignto/sales'
        this.authService.sendRequest('post', endPoint, this.assignForm.value).subscribe((result: any) => {
          this.result = result;
          if (this.result.success == false) {
            this.msg = this.result.content.messageList.role;
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.product;
          } else if (this.result.success == true) {
            this.error = false;
            this.success = true;
            this.message = this.result.content.messageList.product;
            this.router.navigate(['/view-assign-products']);
          }
        })
      } else {
        this.assignForm.controls["distributor_id"].setValue(this.distributerItems);
        this.assignForm.controls["assignedProducts"].setValue(this.productsItems);
        var endPoint = 'productassignto/distributor'
        this.authService.sendRequest('post', endPoint, this.assignForm.value).subscribe((result: any) => {
          this.result = result;
          if (this.result.success == false) {
            this.msg = this.result.content.messageList.role;
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList
          } else if (this.result.success == true) {
            this.error = false;
            this.success = true;
            this.message = this.result.content.messageList
            this.router.navigate(['/view-assign-products']);
          }
        })
      }

    }

  }
}
