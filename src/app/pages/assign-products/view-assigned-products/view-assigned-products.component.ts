import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
declare let $: any;

@Component({
  selector: 'app-view-assigned-products',
  templateUrl: './view-assigned-products.component.html',
  styleUrls: ['./view-assigned-products.component.css']
})
export class ViewAssignedProductsComponent implements OnInit {


  dropdownList = [
    { item_id: 1, item_text: 'Sales Partner' },
    // { item_id: 2, item_text: 'Sales Representative' },
  ];
  dropdownSettings = {
    singleSelection: true,
    idField: 'item_id',
    textField: 'item_text',
    enableCheckAll: false,
  };

  selectedItems: any = [];

  salesList: any = [];
  salesItems: any = [];
  salesdownSettings: any;

  distributerList: any = [];
  distributerItems: any = [];
  distributerdownSettings: any;


  customersList: any = [];
  customersItems: any = [];
  customersdownSettings: any;

  error: any;
  language: any;
  msg: any;
  customers: any = false;
  isSubmitted: any = false;
  tableShow: any = false;

  message: any;
  success: any = false;
  visibility: any = 0;
  distributors: any;
  selectedData: any;
  result: any;
  displayedColumns: string[] = [
    'productId',
    'Product-Name',
    'Product-Component',
    'Product-Details',
    'Description',
    'Publish',
    'Price',
    'Delete-Product',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;

  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  confirm: any;
  roleData: any;
  id: any;
  role: any;
  permissions: any;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router) {
  }
  
  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getSales();
    this.getDistributors();
    this.removeColumn();
  }

  onItemSelect(item: any) {
    if (item.item_text == "Sales Representative") {
      this.visibility = 1

    } else if (item.item_text == "Sales Partner") {
      this.visibility = 2
      // this.getSales();
    }
  }

  onDeSelect(items: any) {
    this.visibility = 0
  }



  salesSelect(item: any) {
    this.tableShow = true;
    this.customers = true;
    this.authService.setLoader(true);
    var endPoint = 'productby/sales/' + item.id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.product;
        this.tableShow = false;
      } else if (this.result.success == true) {
        this.error = false;
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    })
  }

  salesDeSelect(items: any) {
    this.customers = false;
    const index = this.salesItems.indexOf(items.id);
    if (index > -1) {
      this.salesItems.splice(index, 1);
    }
  }

  distSelect(items: any) {
    this.tableShow = true;
    this.customers = true;
    this.distributerItems.push(items.id);
    this.customers = true;
    var endPoint = 'productby/distributor/' + items.id;
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.product;
        this.tableShow = false;
      } else if (this.result.success == true) {
        this.error = false;
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    })
  }


  distDeSelect(item: any) {
    this.customers = false;
    const index = this.distributerItems.indexOf(item.id);
    if (index > -1) {
      this.distributerItems.splice(index, 1);
    }
  }

  customerSelect(items: any) {
    this.customersItems.push({ 'id': items.id });
  }


  customerDeSelect(item: any) {
    const index = this.customersItems.indexOf(item.id);
    if (index > -1) {
      this.customersItems.splice(index, 1);
    }
  }

  getDistributors() {
    let self = this;
    var endPoint = 'distributors';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.product;
      } else if (this.result.success == true) {
        this.error = false;
        this.msg = this.result.content.messageList.Signup;
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.distributerList.push({ 'id': val.uid, 'name': val.mail });
        });
        self.distributerdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

      }
    })
  }

  getSales() {
    let self = this;
    var endPoint = 'salesusers';
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.product;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.error = false;
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.salesList.push({ 'id': val.uid, 'name': val.mail });
        });
        self.salesdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  removeColumn() {
    if ((!this.authService.isAdmin()) && this.permissions.productAssign && !this.permissions.productAssign.includes('DELETE')) {
      this.columnsToDisplay.pop();
    }
  }

  modal(event: any) {
    if (event == 'delete') {
      $('#exampleModal').modal('show')
    } else if (event == 'no') {
      $('#exampleModal').modal('hide')
    } else if (event == 'yes') {
      $('#exampleModal').modal('hide')
      this.deleteUser();
    }
  }

  getproductId(id: any) {
    this.id = id;
  }

  deleteUser() {
    if (this.selectedItems[0].item_text == 'Sales') {
      this.authService.setLoader(true);
      var endPoint = 'sales/productdelete/' + this.id
      this.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        this.result = result
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.product;
        } else if (this.result.success == true) {
          window.location.reload();
        }
      })
    } else {
      this.authService.setLoader(true);
      var endPoint = 'distributor/productdelete/' + this.id
      this.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        this.result = result
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.product;
        } else if (this.result.success == true) {
          window.location.reload();
        }
      })
    }

  }

}
