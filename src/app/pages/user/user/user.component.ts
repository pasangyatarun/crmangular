import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { User } from 'src/app/models/user.model';
import { Permission } from 'src/app/models/permission.model';
import { Role } from 'src/app/models/role.model';
declare let $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {

  userData: User[] = [];
  displayedColumns: string[] = [
    'Email',
    'Club-Name',
    'Role',
    'Status',
    'Membar-Since',
    'View-Profile',
    'Edit-User',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource !: MatTableDataSource<any>;
  result: any;
  @ViewChild(MatPaginator) matpaginator!: MatPaginator;
  @ViewChild(MatSort) matsort!: MatSort;
  error: boolean = false;
  language: any;
  msg: any;
  roleData: any;
  userListForm: FormGroup;
  isSubmitted: boolean = false;
  message: any;
  success: boolean = false;
  permissions!: Permission;
  role!: Role[];
  isData: boolean = true;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.userListForm = this.formBuilder.group({
      role: [''],
      status: [''],
    });
  }

  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getuserData();
    this.getRoll();
    this.removeColumn();
  }

  getuserData() {
    var endPoint: string;
    endPoint = 'users';
    this.authService.setLoader(true);
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.isData = false;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {
          element.status = element.status == '1' ? this.language.viewUser.active : this.language.viewUser.locked
          if (element.role) {
            const roles: string[] = []
            element.role.forEach((ele: any) => {
              roles.push(ele.role)
              element.role = roles

            });
          }
        });
        this.userData = this.result.content.dataList;
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;
        }
        this.dataSource.paginator = this.matpaginator;
        this.dataSource.sort = this.matsort;
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  onSubmit() {
    this.authService.setLoader(true);
    this.isSubmitted = true;
    if (this.userListForm.invalid) {
      return;
    } else {
      var endPoint: string = 'filter'
      this.authService.sendRequest('post', endPoint, this.userListForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.user;
          this.dataSource = new MatTableDataSource();
          this.dataSource.paginator = this.matpaginator;
          this.dataSource.sort = this.matsort;
          this.isData = false;
        } else if (this.result.success == true) {
          this.error = false;
          this.message = this.result.content.messageList.user;
          this.result.content.dataList.forEach((element: any) => {
            element.status = element.status == '1' ? this.language.viewUser.active : this.language.viewUser.locked
            if (element.role) {
              const roles: string[] = []
              element.role.forEach((ele: any) => {
                roles.push(ele.role)
                element.role = roles

              });
            }
          });
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          this.dataSource.paginator = this.matpaginator;
          this.dataSource.sort = this.matsort;

          if (this.dataSource.data.length) {
            this.isData = true;
          } else {
            this.isData = false;

          }

        }
      })
    }
  }

  removeColumn() {
    if ((!this.authService.isAdmin()) && this.permissions.user && !this.permissions.user.includes('EDIT')) {
      this.columnsToDisplay.pop();
    }
  }

  getRoll() {
    var endPoint: string = 'rolesByroleWeight';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.roleData = this.result.content.dataList;
      }
    })
  }

  reset() {
    this.error = false;
    this.success = false;
    this.message = '';
    this.userListForm.reset();
    this.getuserData();
  }

}

