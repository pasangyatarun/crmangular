import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { Role } from 'src/app/models/role.model';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  addUserForm: FormGroup;
  isSubmitted: boolean = false;
  message!: string;
  result: any;
  msg: any;
  error: boolean = false;
  success: boolean = false;
  language: any;
  role!: Role;
  roleData!: Role[];
  imageSrc!: string;
  domain: any;
  isValidIban!:string;
  file: any;
  id: any;



  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.addUserForm = this.formBuilder.group({

      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      firstName: [''],
      lastName: [''],
      society: ['', Validators.required],
      addressLine1: [''],
      zipcode: ["", Validators.compose([Validators.minLength(5), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      place: [''],
      accountNumber: ["", Validators.compose([Validators.pattern('[0-9]+')])],
      bankCode: [""],
      contactNumber: ["", Validators.compose([Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[- +()0-9]+')])],
      address2: [''],
      status: ['', Validators.required],
      country: [''],
      image: [''],
      iban: [''],
      domain: new FormArray([]),
      userRole: new FormArray([], Validators.required),
      AuthorRole: new FormArray([]),
    });
  }

  get formControls() { return this.addUserForm.controls; }
  
  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getDomainName();
    this.getRoll();
    this.getAuthors();
  }

  getAuthors() {
    let uData = JSON.parse(localStorage.getItem('user-data') || '{}');
    uData[0].role.forEach((element: any) => {
      const formArray: FormArray = this.addUserForm.get('AuthorRole') as FormArray;
      formArray.push(new FormControl(JSON.parse(element.rid)));
    });
  }


  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
      // this.addUserForm.controls["image"].setValue(this.imageSrc);
      this.addUserForm.patchValue({
        image: file
      });
      this.addUserForm.controls["image"].updateValueAndValidity();
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }

  validIban(event: any) {
    if(event.target.value){
      let validateIban = {
        iban: event.target.value
      }
      var endPoint = 'validate-iban';
      this.authService.sendRequest('post', endPoint, validateIban).subscribe(result => {
        this.result = result;
        if (this.result.success == false) {
          this.isValidIban = this.result.content.messageList.iban;
        } else if (this.result.success == true) {
          this.isValidIban = '';
        }
      })
    }else{
      this.isValidIban = '';
    }
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.addUserForm.invalid || this.isValidIban) {
      return;
    } else {

      this.authService.setLoader(true);
      var formData: any = new FormData();
      let self = this;
      for (const key in this.addUserForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.addUserForm.value, key)) {
          const element = this.addUserForm.value[key];

          if (key == 'image') {

            formData.append('file', self.imageSrc);
          }

          if (key == 'userRole') {
            formData.append('userRole', JSON.stringify(element));
          }

          if (key == 'AuthorRole') {
            formData.append('AuthorRole', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'userRole') && (key != 'AuthorRole')) {
              formData.append(key, element);
            }
          }
        }

      }
      var endPoint = 'useradd'
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.userAdd;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.userAdd;
          setTimeout(() => {
            this.router.navigate(['/user']);
          }, 3000)
        }
      })
    }
  }

  getRoll() {
    this.authService.setLoader(true);
    // var endPoint = 'roles';
    var endPoint = 'rolesByroleWeight ';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.roleData = this.result.content.dataList;        
        let uData = JSON.parse(localStorage.getItem('user-data') || '{}');
        uData[0].role.forEach((element: any) => {          
        });        
      }
    })
  }

  getDomainName() {
    this.authService.setLoader(true);
    var endPoint = 'domains';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
        this.domain = this.result.content.dataList;

      }
    })
  }

  onCheckRole(event: any) {
    const formArray: FormArray = this.addUserForm.get('userRole') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(JSON.parse(event.target.value)));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }


  onCheckDomain(event: any) {
    const formArray: FormArray = this.addUserForm.get('domain') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

}
