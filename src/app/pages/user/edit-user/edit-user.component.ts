import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/models/role.model';
import { Permission } from 'src/app/models/permission.model';
import { CustomPasswordValidators } from 'src/app/providers/customPasswordValidators';

declare let $: any;


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})

export class EditUserComponent implements OnInit {

  editUserForm: FormGroup;
  isSubmitted: boolean = false;
  message!: string;
  result: any;
  msg: any;
  show: boolean = false;
  id: any;
  reEnterData!: User;
  role!: Role[];
  error: boolean = false;
  success: boolean = false;
  language: any;
  checked!: number;
  permissions!: Permission;
  roleData!: Role[];
  imageSrc!: string;
  hasPicture: boolean = false;
  imagePreview!: string;
  isValidIban!: string;

  constructor(private confirmBoxService: ConfirmDialogService, private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editUserForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      password: ["", Validators.compose([Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{5,}')])],
      confirmPassword: ["",],
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      society: ['', Validators.required],
      addressLine1: [''],
      zipcode: ["", Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      place: ["", Validators.required],
      accountNumber: ["", Validators.compose([Validators.pattern('[0-9]+')])],
      contactNumber: ["", Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[- +()0-9]+')])],
      country: [''],
      bankCode: [''],
      addressLine2: [''],
      image: [''],
      iban: [''],
      status: [''],
      userRole: new FormArray([]),

    },
      {
        validators: [CustomPasswordValidators.mustMatch('password', 'confirmPassword')],
      });
  }


  get formControls() { return this.editUserForm.controls; }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.getUserData();
  }

  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files;
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }

  validIban(event: any) {
    let validateIban = {
      iban: event.target.value
    }
    var endPoint: string = 'validate-iban';
    this.authService.sendRequest('post', endPoint, validateIban).subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.isValidIban = this.result.content.messageList.iban;
      } else if (this.result.success == true) {
        this.isValidIban = '';
      }
    })
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.editUserForm.invalid) {
      return;
    } else {
      var formData: any = new FormData();
      this.authService.setLoader(true);
      this.editUserForm.value.image = this.imageSrc;
      let self = this;
      for (const key in this.editUserForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.editUserForm.value, key)) {
          const element = this.editUserForm.value[key];

          if (key == 'image') {
            if (self.imageSrc) {
              formData.append('file', self.imageSrc);
            } else {
              formData.append('imageUrl', self.imagePreview);
            }
          }
          if (key == 'userRole') {
            formData.append('userRole', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'userRole')) {
              formData.append(key, element);
            }
          }
        }

      }

      var endPoint: string = 'profile-edit/' + this.id
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);

        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          if (this.msg == null || this.msg == "") {
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.Signup;
          }
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.Signup;
          setTimeout(() => {
            this.router.navigate(['/user']);
          }, 1000)

        }
      })
    }
  }


  getUserData() {
    this.authService.setLoader(true);
    var endPoint: string = 'userbyid/' + this.id;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.reEnterData = this.result.content.dataList[0];
        this.reEnterValue();
        this.getRoll();
      }
    })
  }

  reEnterValue() {
    this.editUserForm.controls["email"].setValue(this.reEnterData.mail);
    this.editUserForm.controls["firstName"].setValue(this.reEnterData.firstName);
    this.editUserForm.controls["lastName"].setValue(this.reEnterData.lastName);
    this.editUserForm.controls["society"].setValue(this.reEnterData.society);
    this.editUserForm.controls["addressLine1"].setValue(this.reEnterData.addressLine1);
    this.editUserForm.controls["zipcode"].setValue(this.reEnterData.zipcode);
    this.editUserForm.controls["place"].setValue(this.reEnterData.place);
    this.editUserForm.controls["accountNumber"].setValue(this.reEnterData.accountNumber);
    this.editUserForm.controls["country"].setValue(this.reEnterData.country);
    this.editUserForm.controls["contactNumber"].setValue(this.reEnterData.contactNumber);
    this.editUserForm.controls["status"].setValue(this.reEnterData.status);
    this.checked = this.reEnterData.status;
    this.editUserForm.controls["addressLine2"].setValue(this.reEnterData.addressLine2);
    this.editUserForm.controls["bankCode"].setValue(this.reEnterData.bankCode);
    this.editUserForm.controls["iban"].setValue(this.reEnterData.ibanValue);
    if (this.reEnterData.image && this.reEnterData.image != 'undefined') {
      this.hasPicture = true;
      this.imagePreview = this.reEnterData.image;
    }

  }

  getRoll() {
    this.authService.setLoader(true);
    let self = this;
    var endPoint: string = 'rolesByroleWeight';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
        this.roleData = this.result.content.dataList;
        this.roleData.forEach((element: any, index: any) => {
          let r_id = this.reEnterData.role.find((o: any) => o.rid === element.rid);
          if (r_id) {
            const formArray: FormArray = this.editUserForm.get('userRole') as FormArray;
            formArray.push(new FormControl(JSON.parse(element.rid)));
            self.roleData[index]['r_id'] = element.rid;
          } else {
            self.roleData[index]['r_id'] = '';
          }
        });
      }
    })

  }

  onCheckRole(event: any) {
    const formArray: FormArray = this.editUserForm.get('userRole') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(JSON.parse(event.target.value)));

    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  deleteUser() {
    let self = this;
    self.confirmBoxService.confirmThis(this.language.editUser.deleteAccount, this.language.confirmBox.delete_user, function () {
      self.authService.setLoader(true);
      var endPoint: string = 'userdelete/' + self.id
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.userList;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.result.content.messageList.userList;
          setTimeout(() => {
            self.router.navigate(['/user'])
          }, 3000)
        }
      });
    }, function () { }
    )
  }

}
