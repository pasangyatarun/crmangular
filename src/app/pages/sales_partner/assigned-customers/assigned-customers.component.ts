import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
import { AuthService } from 'src/app/service/auth.service';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-assigned-customers',
  templateUrl: './assigned-customers.component.html',
  styleUrls: ['./assigned-customers.component.css']
})
export class AssignedCustomersComponent implements OnInit {

  id: any;
  role: any;
  permissions: any;
  result: any;
  dataSource!: MatTableDataSource<any>;
  error: boolean = false;
  success: boolean = false;
  message: any;
  language: any;
  selectedItems: any = [];
  displayedColumns: string[] = [
    'Email',
    'Status',
    'Club-Name',
    'Member-Since',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  isData: boolean = true;

  constructor(private route: ActivatedRoute, private confirmBoxService: ConfirmDialogService, public authService: AuthService, private langService: LanguageService,
    private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.id = this.route.snapshot.paramMap.get('id');
    this.language = this.langService.getLanguaageFile();
    this.getCustomers();
  }

  getCustomers() {    
    this.authService.setLoader(true);
    var endPoint = 'customerby/distributor/' + this.id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.isData = false;
      } else if (this.result.success == true) {
        this.error = false;
        this.result.content.dataList.forEach((element: any) => {
          element.status = element.status == '1' ? this.language.viewUser.active : this.language.viewUser.locked
          if (element.role) {
            const roles: string[] = []
            element.role.forEach((ele: any) => {
              roles.push(ele.role)
              element.role = roles

            });
          }
        });
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteUser(id: any) {
    let self = this;
    self.confirmBoxService.confirmThis(this.language.customerAssign.deleteUser, this.language.confirmBox.deleteAssignCustomer, function () {

      if (self.selectedItems[0].item_text == 'Sales') {
        self.authService.setLoader(true);
        var endPoint = 'sales/customerdelete/' + id
        self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
          self.result = result
          self.authService.setLoader(false);
          if (self.result.success == false) {
            self.error = true;
            self.success = false;
            self.message = self.result.content.messageList.user;
          } else if (self.result.success == true) {

            window.location.reload();
          }
        })
      } else {
        self.authService.setLoader(true);
        var endPoint = 'distributor/customerdelete/' + self.id
        self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
          self.result = result
          self.authService.setLoader(false);
          if (self.result.success == false) {
            self.error = true;
            self.success = false;
            self.message = self.result.content.messageList.user;
          } else if (self.result.success == true) {
            window.location.reload();
          }
        })
      }


    }, function () { }
    )

  }

}
