import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPartnerComponent } from './sales-partner.component';

describe('SalesPartnerComponent', () => {
  let component: SalesPartnerComponent;
  let fixture: ComponentFixture<SalesPartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesPartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
