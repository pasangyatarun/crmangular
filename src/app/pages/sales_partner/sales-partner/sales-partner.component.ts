import { Component, OnInit } from '@angular/core';
import { LanguageService } from 'src/app/service/language.service';
import { AuthService } from 'src/app/service/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
declare let $: any;

@Component({
  selector: 'app-sales-partner',
  templateUrl: './sales-partner.component.html',
  styleUrls: ['./sales-partner.component.css']
})
export class SalesPartnerComponent implements OnInit {

  userData: any = [];
  displayedColumns: string[] = [
    'Email',
    'Status',
    'Membar-Since',
    'View-Profile',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource !: MatTableDataSource<any>;
  error: boolean = false;
  result: any;
  @ViewChild(MatPaginator) matpaginator!: MatPaginator;
  @ViewChild(MatSort) matsort!: MatSort;
  language: any;
  isSubmitted: boolean = false;
  message!: string;
  success: boolean = false;
  permissions: any;
  role: any;
  isData: boolean = true;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
  }


  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getuserData();
  }

  getuserData() {
    this.authService.setLoader(true);
    var endPoint = 'distributors'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
        this.isData = false;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {
          element.status = element.status == '1' ? this.language.viewUser.active : this.language.viewUser.locked
        });
        this.userData = this.result.content.dataList;
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;
        }
        this.dataSource.paginator = this.matpaginator;
        this.dataSource.sort = this.matsort;
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

}
