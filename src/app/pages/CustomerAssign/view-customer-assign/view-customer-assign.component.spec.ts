import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCustomerAssignComponent } from './view-customer-assign.component';

describe('ViewCustomerAssignComponent', () => {
  let component: ViewCustomerAssignComponent;
  let fixture: ComponentFixture<ViewCustomerAssignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewCustomerAssignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCustomerAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
