import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';

declare let $: any;

@Component({
  selector: 'app-view-customer-assign',
  templateUrl: './view-customer-assign.component.html',
  styleUrls: ['./view-customer-assign.component.css']
})
export class ViewCustomerAssignComponent implements OnInit {


  dropdownList = [
    { item_id: 1, item_text: 'Sales Partner' },
    // { item_id: 2, item_text: 'Sales Representative' },
  ];
  dropdownSettings = {
    singleSelection: true,
    idField: 'item_id',
    textField: 'item_text',
    enableCheckAll: false,
  };

  selectedItems: any = [];

  salesList: any = [];
  salesItems: any = [];
  salesdownSettings: any;

  distributerList: any = [];
  distributerItems: any = [];
  distributerdownSettings: any;


  customersList: any = [];
  customersItems: any = [];
  customersdownSettings: any;

  error: any;
  language: any;
  msg: any;
  customers: any = false;
  isSubmitted: any = false;
  tableShow: any = false;

  message: any;
  success: any = false;
  visibility: any = 0;
  distributors: any;
  selectedData: any;
  result: any;
  displayedColumns: string[] = [
    'Email',
    'Status',
    'Club-Name',
    'Role',
    'Member-Since',
    'Delete-User',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;

  userData: any[] = [];


  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  confirm: any;
  roleData: any;
  id: any;
  permissions: any;
  role: any;

  constructor(private confirmBoxService: ConfirmDialogService, public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router) {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getSales();
    this.getDistributors();
    this.removeColumn();
  }

  onItemSelect(item: any) {
    if (item.item_text == "Sales Representative") {
      this.visibility = 1

    } else if (item.item_text == "Sales Partner") {
      this.visibility = 2
      // this.getSales();
    }
  }

  onDeSelect(items: any) {
    this.visibility = 0
  }



  salesSelect(item: any) {
    this.tableShow = true;
    this.customers = true;
    this.authService.setLoader(true);
    var endPoint = 'customerby/sales/' + item.id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result

      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
        this.tableShow = false;
      } else if (this.result.success == true) {
        this.error = false;
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    })
  }

  salesDeSelect(items: any) {
    this.customers = false;
    const index = this.salesItems.indexOf(items.id);
    if (index > -1) {
      this.salesItems.splice(index, 1);
    }
  }

  distSelect(items: any) {
    this.tableShow = true;
    this.customers = true;
    this.distributerItems.push(items.id);
    this.customers = true;
    this.authService.setLoader(true);
    var endPoint = 'customerby/distributor/' + items.id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result

      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
        this.tableShow = false;
      } else if (this.result.success == true) {
        this.error = false;
        this.result.content.dataList.forEach((element: any) => {
          element.status = element.status == '1' ? this.language.viewUser.active : this.language.viewUser.locked
          if (element.role) {
            const roles: string[] = []
            element.role.forEach((ele: any) => {
              roles.push(ele.role)
              element.role = roles

            });
          }
        });
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    })
  }


  distDeSelect(item: any) {
    this.customers = false;
    const index = this.distributerItems.indexOf(item.id);
    if (index > -1) {
      this.distributerItems.splice(index, 1);
    }
  }

  customerSelect(items: any) {

    this.customersItems.push({ 'id': items.id });
  }


  customerDeSelect(item: any) {

    const index = this.customersItems.indexOf(item.id);
    if (index > -1) {
      this.customersItems.splice(index, 1);
    }
  }

  getDistributors() {
    let self = this;
    var endPoint = 'distributors';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.error = false;
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.distributerList.push({ 'id': val.uid, 'name': val.mail });
        });
        self.distributerdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

      }
    })
  }

  getSales() {
    let self = this;
    var endPoint = 'salesusers';
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.error = false;
        this.authService.setLoader(false);
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.salesList.push({ 'id': val.uid, 'name': val.mail });
        });
        self.salesdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  removeColumn() {

    if ((!this.authService.isAdmin()) && this.permissions.customerAssign && !this.permissions.customerAssign.includes('DELETE')) {

      this.columnsToDisplay.pop();
    }
  }


  deleteUser(id: any) {
    let self = this;
    self.confirmBoxService.confirmThis(this.language.customerAssign.deleteUser, this.language.confirmBox.deleteAssignCustomer, function () {

      if (self.selectedItems[0].item_text == 'Sales') {
        self.authService.setLoader(true);
        var endPoint = 'sales/customerdelete/' + id
        self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
          self.result = result
          self.authService.setLoader(false);
          if (self.result.success == false) {
            self.error = true;
            self.success = false;
            self.message = self.result.content.messageList.user;
          } else if (self.result.success == true) {

            window.location.reload();
          }
        })
      } else {
        self.authService.setLoader(true);
        var endPoint = 'distributor/customerdelete/' + self.id
        self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
          self.result = result
          self.authService.setLoader(false);
          if (self.result.success == false) {
            self.error = true;
            self.success = false;
            self.message = self.result.content.messageList.user;
          } else if (self.result.success == true) {
            window.location.reload();
          }
        })
      }


    }, function () { }
    )

  }
}
