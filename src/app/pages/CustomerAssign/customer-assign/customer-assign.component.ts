import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-customer-assign',
  templateUrl: './customer-assign.component.html',
  styleUrls: ['./customer-assign.component.css']
})
export class CustomerAssignComponent implements OnInit {

  dropdownList = [
    { item_id: 1, item_text: 'Sales-Partner' },
    // { item_id: 2, item_text: 'Sales-Representative' },
  ];
  dropdownSettings = {
    singleSelection: true,
    idField: 'item_id',
    textField: 'item_text',
    enableCheckAll: false,
  };

  selectedItems: any = [];

  salesList: any = [];
  salesItems: any = [];
  salesdownSettings: any;

  salesPartnerList: any = [];
  salesPartnerItems: any = [];
  salespartnerSettings: any;


  customersList: any = [];
  customersItems: any = [];
  customersdownSettings: any;

  error: any = false;
  language: any;
  msg: any;
  customers: any = false;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  visibility: any = 0;
  distributors: any;
  selectedData: any;
  result: any;
  assignForm: FormGroup;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router) {
    this.assignForm = this.formBuilder.group({
      assignedCustomers: [''],
      distributor_id: [''],
      sales_id: [''],

    });
  }

  get formControls() { return this.assignForm.controls; }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getSales();
    this.getDistributors();
    this.getCustomers();
  }

  onItemSelect(item: any) {
    if (item.item_text == "Sales-Representative") {
      this.visibility = 1

      this.assignForm.controls['sales_id'].setValidators([Validators.required])
      this.assignForm.controls['sales_id'].updateValueAndValidity()
      this.assignForm.controls['distributor_id'].clearValidators();
      this.assignForm.controls['distributor_id'].updateValueAndValidity();

    } else if (item.item_text == "Sales-Partner") {
      this.visibility = 2
      this.assignForm.controls['distributor_id'].setValidators([Validators.required])
      this.assignForm.controls['distributor_id'].updateValueAndValidity()
      this.assignForm.controls['sales_id'].clearValidators();
      this.assignForm.controls['sales_id'].updateValueAndValidity();
    }
  }

  onDeSelect(items: any) {
    this.visibility = 0
  }



  salesSelect(item: any) {
    this.customers = true;
    this.assignForm.controls['assignedCustomers'].setValidators([Validators.required])
    this.assignForm.controls['assignedCustomers'].updateValueAndValidity()
    this.salesItems.push(item.id);
  }

  salesDeSelect(items: any) {
    this.customers = false;
    const index = this.salesItems.indexOf(items.id);
    if (index > -1) {
      this.salesItems.splice(index, 1);
    }
  }

  distSelect(items: any) {
    this.customers = true;
    this.assignForm.controls['assignedCustomers'].setValidators([Validators.required])
    this.assignForm.controls['assignedCustomers'].updateValueAndValidity()
    this.salesPartnerItems.push(items.id);
  }


  distDeSelect(item: any) {
    this.customers = false;
    const index = this.salesPartnerItems.indexOf(item.id);
    if (index > -1) {
      this.salesPartnerItems.splice(index, 1);
    }
  }

  customerSelect(items: any) {

    this.customersItems.push({ 'id': items.id });
  }


  customerDeSelect(item: any) {
    this.assignForm.controls['assignedCustomers'].setValidators([Validators.required])
    this.assignForm.controls['assignedCustomers'].updateValueAndValidity()

    const index = this.customersItems.indexOf(item.id);

    if (index > -2 || -1) {
      this.customersItems.splice(index, 1);
    }
  }


  customerSelectAll(item: any) {
    this.assignForm.controls['assignedCustomers'].clearValidators();
    this.assignForm.controls['assignedCustomers'].updateValueAndValidity();
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.customersItems.push({ 'id': element.id });
      }
    }
  }


  customerDeSelectAll(item: any) {
    this.assignForm.controls['assignedCustomers'].setValidators([Validators.required])
    this.assignForm.controls['assignedCustomers'].updateValueAndValidity()
    this.customersItems = [];
  }

  getDistributors() {
    let self = this;
    var endPoint = 'distributors';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.error = false;        
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.salesPartnerList.push({ 'id': val.uid, 'name': val.mail });
        });

        self.salespartnerSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };

      }
    })
  }

  getSales() {
    let self = this;
    var endPoint = 'salesusers';
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.error = false;
        this.result.content.dataList.forEach((val: any, key: any) => {
          this.salesList.push({ 'id': val.uid, 'name': val.mail });
        });
        self.salesdownSettings = {
          singleSelection: true,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: false,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }

  getCustomers() {
    let self = this;
    var endPoint = 'users';
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.authService.setLoader(false);
        this.error = false;
        this.result.content.dataList.forEach((val: any, key: any) => {          
          val.role.forEach((element:any) => {
            if( element.role == 'Customer' ){
              this.customersList.push({ 'id': val.uid, 'name': val.mail });
             }
            
          });
          
        });

        self.customersdownSettings = {
          singleSelection: false,
          idField: 'id',
          textField: 'name',
          selectAllText: 'Select All',
          enableCheckAll: true,
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true
        };
      }
    })
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.assignForm.invalid) {
      return;
    } else {
      if (this.selectedItems[0].item_text == 'Sales-Representative') {
        this.assignForm.controls["sales_id"].setValue(this.salesItems);
        this.assignForm.controls["assignedCustomers"].setValue(this.customersItems);
        var endPoint = 'assignto/sales'
        this.authService.sendRequest('post', endPoint, this.assignForm.value).subscribe((result: any) => {
          this.result = result;

          if (this.result.success == false) {
            this.msg = this.result.content.messageList.role;
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.user;

          } else if (this.result.success == true) {
            this.error = false;
            this.success = true;
            this.message = this.result.content.messageList.user;
            setTimeout(() => {
              this.router.navigate(['/view-assign-customer']);
            }, 3000)
          }
        })
      } else {
        this.assignForm.controls["distributor_id"].setValue(this.salesPartnerItems);
        this.assignForm.controls["assignedCustomers"].setValue(this.customersItems);
        var endPoint = 'assignto/distributor'
        this.authService.sendRequest('post', endPoint, this.assignForm.value).subscribe((result: any) => {
          this.result = result;

          if (this.result.success == false) {
            this.msg = this.result.content.messageList.user;
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.user;
          } else if (this.result.success == true) {
            this.error = false;
            this.success = true;
            this.message = this.result.content.messageList.user;
            setTimeout(() => {
              this.router.navigate(['/view-assign-customer']);
            }, 3000)
          }
        })
      }

    }

  }

}
