import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  addProductForm: FormGroup;
  imageSrc!: string;
  isSubmitted: boolean = false;
  result: any;
  error: boolean = false;
  success: boolean = false;
  message!: string;
  language: any;
  msg: any;
  roleCategoryData: any;
  roleCategoryList: any = [];
  roleCategoryItems: any = [];
  roleCategorySettings: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.addProductForm = this.formBuilder.group({
      product_name: ['', Validators.required],
      description: ['', Validators.required],
      imageUpload: [''],
      price: ['', Validators.compose([Validators.required, Validators.min(0)])],
      tax_policy: [''],
      component_type: [''],
      nonVisibleRoleCategory: ['']
    });
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getRollCategories();
  }

  get formControls() { return this.addProductForm.controls; }

  getRollCategories() {
    let self = this;
    var endPoint = 'roleCategory';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.roleCategoryData = this.result.content.dataList;
        if (this.roleCategoryData && this.roleCategoryData.length) {
          this.result.content.dataList.forEach((val: any, key: any) => {
            this.roleCategoryList.push({ 'id': val.id, 'name': val.category_name });
          });
          self.roleCategorySettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            enableCheckAll: true,
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };
        }
      }
    })
  }

  roleCategorySelect(items: any) {
    this.roleCategoryItems.push(items.id);
  }

  roleCategoryDeSelect(item: any) {

    const index = this.roleCategoryItems.indexOf(item.id);
    if (index > -2 || -1) {
      this.roleCategoryItems.splice(index, 1);
    }
  }

  roleCategorySelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.roleCategoryItems.push(element.id);
      }
    }
  }

  roleCategoryDeSelectAll(item: any) {
    this.roleCategoryItems = [];
  }

  errorImage: { isError: boolean, errorMessage: string } = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { isError: false, errorMessage: '' };
      this.imageSrc = file;

      this.addProductForm.patchValue({
        imageUpload: file
      });
      this.addProductForm.controls["imageUpload"].updateValueAndValidity();
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    // $('#textPreview').show();
    // $('#textPreview').text(file.name);
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.addProductForm.invalid) {
      return;
    } else {
      this.addProductForm.controls["nonVisibleRoleCategory"].setValue(this.roleCategoryItems);

      var formData: any = new FormData();
      this.authService.setLoader(false);
      this.addProductForm.value.imageUpload = this.imageSrc;
      let self = this;
      for (const key in this.addProductForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.addProductForm.value, key)) {
          const element = this.addProductForm.value[key];
          if (key == 'imageUpload') {
            formData.append('file', self.imageSrc);
          }

          if (key == 'nonVisibleRoleCategory') {
            formData.append('nonVisibleRoleCategory', JSON.stringify(element));
          }
          else {
            if ((key != 'imageUpload') && (key != 'nonVisibleRoleCategory')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      var endPoint: string = 'crmproductcreate'
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          if (this.result.content.messageList.product) {
            this.msg = ''
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.product;
          } else {
            this.msg = this.result.content.messageList;
          }
          this.msg = this.result.content.messageList;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.product;
          setTimeout(() => {
            this.router.navigate(['/view-product']);
          }, 3000)
        }
      })
    }
  }

}
