import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  editProductForm: FormGroup;
  imageSrc!: string;
  isSubmitted: boolean = false;
  result: any;
  error: boolean = false;
  success: boolean = false;
  show: boolean = false;
  message!: string;
  language: any;
  id: any;
  role: any;
  permissions: any;
  reEnterData: any;
  msg: any;
  hasPicture: boolean = false;
  imagePreview!: string;
  roleCategoryData: any;
  roleCategoryList: any = [];
  roleCategoryItems: any = [];
  roleCategorySettings: any;
  deptList: any = [];


  constructor(private confirmBoxService: ConfirmDialogService, private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editProductForm = this.formBuilder.group({
      product_name: ['', Validators.required],
      description: ['', Validators.required],
      imageUpload: [''],
      price: ['', Validators.compose([Validators.required, Validators.min(0)])],
      tax_policy: [''],
      component_type: [''],
      nonVisibleRoleCategory: ['']
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getProductData();

  }

  get formControls() { return this.editProductForm.controls; }

  getRollCategories() {
    let self = this;
    var endPoint = 'roleCategory';
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;

      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.roleCategoryData = this.result.content.dataList;
        if (this.roleCategoryData && this.roleCategoryData.length) {
          this.result.content.dataList.forEach((val: any, key: any) => {
            this.roleCategoryList.push({ 'id': val.id, 'name': val.category_name });
          });
          self.roleCategorySettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Select All',
            enableCheckAll: true,
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true
          };

          if (this.reEnterData && this.reEnterData[0].non_visible_role_category) {
            this.result.content.dataList.forEach((element: any) => {
              JSON.parse(this.reEnterData[0].non_visible_role_category).forEach((ele: any) => {
                if (element.id == ele) {
                  this.roleCategoryItems.push(element.id)
                  this.deptList.push({ 'id': element.id, 'name': element.category_name })
                  this.editProductForm.controls["nonVisibleRoleCategory"].setValue(this.deptList);
                }
              });
            });

          }
        }
      }
    })
  }

  roleCategorySelect(items: any) {
    this.roleCategoryItems.push(items.id);
  }

  roleCategoryDeSelect(item: any) {

    const index = this.roleCategoryItems.indexOf(item.id);
    if (index > -2 || -1) {
      this.roleCategoryItems.splice(index, 1);
    }
  }

  roleCategorySelectAll(item: any) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        const element = item[key];
        this.roleCategoryItems.push(element.id);
      }
    }
  }

  roleCategoryDeSelectAll(item: any) {
    this.roleCategoryItems = [];
  }

  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    // $('#textPreview').show();
    // $('#textPreview').text(file.name);
  }

  getProductData() {
    this.authService.setLoader(true);
    var endPoint = '/crmproductsbyid/' + this.id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.news;
        this.msg = this.result.content.messageList;
      } else if (this.result.success == true) {
        this.reEnterData = this.result.content.dataList;
        this.error = false;
        this.reEnterValue();
        this.getRollCategories();

      }
    })
  }

  reEnterValue() {
    this.editProductForm.controls["product_name"].setValue(this.reEnterData[0].product_name);
    this.editProductForm.controls["description"].setValue(this.reEnterData[0].description);
    this.editProductForm.controls["price"].setValue(this.reEnterData[0].price);
    this.editProductForm.controls["imageUpload"].setValue(this.reEnterData[0].image);
    this.editProductForm.controls["tax_policy"].setValue(this.reEnterData[0].tax_policy);
    this.editProductForm.controls["component_type"].setValue(this.reEnterData[0].component_type);
    if (this.reEnterData[0].image) {
      this.hasPicture = true;
      this.imagePreview = this.reEnterData[0].image;
    }

  }

  onSubmit() {
    this.editProductForm.controls["nonVisibleRoleCategory"].setValue(this.roleCategoryItems);
    this.isSubmitted = true;
    if (this.editProductForm.invalid) {
      return;
    } else {

      var formData: any = new FormData();

      let self = this;
      for (const key in this.editProductForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.editProductForm.value, key)) {
          const element = this.editProductForm.value[key];

          if (key == 'imageUpload') {
            if (self.imageSrc) {
              formData.append('file', self.imageSrc);

            } else {
              formData.append('imageUrl', self.imagePreview);

            }
          }
          if (key == 'nonVisibleRoleCategory') {
            formData.append('nonVisibleRoleCategory', JSON.stringify(element));
          }

          else {
            if ((key != 'imageUpload') && (key != 'nonVisibleRoleCategory')) {
              formData.append(key, element);
            }
          }
        }

      }
      this.authService.setLoader(true);
      var endPoint = 'crmproductedit/' + this.id
      this.authService.sendRequest('put', endPoint, formData).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          if (this.result.content.messageList.products) {            
            this.msg = ''
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.products;
          } else {
            this.msg = this.result.content.messageList;
          }
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.products;
          setTimeout(() => {
            this.router.navigate(['/view-product']);
          }, 3000)
        }
      })
    }
  }

  diableField(event: any) {
    if (!!event.target.checked) {
      this.editProductForm.controls['url'].disable()
    } else {
      this.editProductForm.controls['url'].enable()
    }
  }
  onCheckChange(event: any) {
    const formArray: FormArray = this.editProductForm.get('tag') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;

      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }


}
