import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { billwerkAccountUrl } from 'src/environments/environment';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  userData: any;
  error: any = false;
  result: any;
  language: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  userOrders: any;
  imagePreview: any;
  hasPicture = false;
  userId: any;

  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;

  invoices: any;
  displayedinvoicesColumns: string[] = [
    'Document-Number',
    'Name',
    'Contract',
    'Date',
    'Currency',
    'Net-Total',
    'Gross-Total',
    'Download',

  ];
  columnsToDisplayInvoice: string[] = this.displayedinvoicesColumns.slice();
  filterForm: FormGroup;
  completedOrders: any;
  productData: any = [];
  orderData: any;
  isData: any = true;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.filterForm = this.formBuilder.group({
      status: ['Completed'],
    });
  }

  ngOnInit(): void {
    this.userId = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getUserData();
    this.getOrderData();

  }

  getUserData() {


    var endPoint = 'userbyid/' + this.userId[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.authService.setLoader(false);
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.userData = this.result.content.dataList;
        if (this.userData[0].image != '' && this.userData[0].image != 'undefined') {
          this.hasPicture = true;
          this.imagePreview = this.userData[0].image;
        }
      }
    })

  }

  getOrderData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')

    var endPoint = 'billwerk-orders/bycustomer/' + id[0].uid;
    this.authService.setLoader(true);
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.authService.setLoader(false);

      this.result = result

      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.orderData = this.result.content.dataList
        this.completedOrders = this.result.content.dataList;
        if (this.completedOrders.length) {
          this.isData = true;
        } else {
          this.isData = false;
        }
        this.getInvoicesData();
        this.getAllProducts();

      }
    })
  }






  getInvoicesData() {
    if (this.completedOrders && this.completedOrders.length > 0) {
      var endPoint = 'billwerk-invoice-bycustomer/' + this.completedOrders[0].CustomerId
      this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
        this.result = result;
        if (this.result.success == false) {
          this.authService.setLoader(false);
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.billwerk;
          this.isData = false;
        } else if (this.result.success == true) {
          this.error = false;
          this.message = this.result.content.content
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          if (this.dataSource.data.length) {
            this.isData = true;
          } else {
            this.isData = false;
          }
        }
      })
    }
  }


  applyinvoiceFilter(event: Event) {

    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    /**set a new filterPredicate function*/
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm: any, key: any) => {
        return this.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      /**  Transform the filter by converting it to lowercase and removing whitespace. */
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    }
  }

  /** also add this nestedFilterCheck class function */
  nestedFilterCheck(search: any, data: { [x: string]: any; }, key: string) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);

        }
      }
    } else {
      search += data[key];
    }
    return search;
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  downloadInvoice(invoiceId: any) {

    this.authService.setLoader(true);
    var endPoint = 'billwerk-invoice-downloadlink/' + invoiceId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.billwerk;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.billwerk;
        this.invoices = this.result.content.dataList;
        this.download(this.invoices[0].Url, this.invoices[0].Filename);


      }
    })

  }

  download(url: any, filename: any) {

    var link = document.createElement('a');
    link.href = billwerkAccountUrl + url;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link)

  }


  getAllProducts() {
    this.productData = [];

    var endPoint = 'billwerk-product'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.authService.setLoader(false);
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        const a1 = this.result.content.dataList;
        const a2 = this.completedOrders;
        const a3 = a1.filter((entry1: any) => !a2.some((entry2: any) => entry1.Id === entry2.PlanVariantId));


        a3.forEach((element: any) => {
          if (this.role.some((value: any) => value.name === 'Customer')) {
            if (element.planDetail.CustomFields.type == 1 || element.planDetail.CustomFields.type == 2 || element.planDetail.CustomFields.type == 0) {
              this.productData.push(element);
            }
          } else {
            this.productData.push(element)
          }
        });


      }
    })
  }

}
