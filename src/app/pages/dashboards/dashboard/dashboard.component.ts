import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userData: any;
  error: any = false;
  result: any;
  language: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  userOrders: any;
  imagePreview: any;
  hasPicture = false;
  userId: any;
  displayedColumns: string[] = [
    'Timestamp',
    'Customer',
    'Plan',
    'Type',
    'Amount',
    'Payment-Provider',
    'Method',
    'Status',
    'Order-Details',
    

  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;

  invoices: any;
  dataSourceInvoice!: MatTableDataSource<any>;
  displayedinvoicesColumns: string[] = [
    'Document-Number',
    'External_Id-Debitor_Id',
    'Name',
    'Contract',
    'Date',
    'Currency',
    'Net-Total',
    'Gross-Total',
    'Download',

  ];
  columnsToDisplayInvoice: string[] = this.displayedinvoicesColumns.slice();
  filterForm: FormGroup;
  completedOrders: any;
  productData: any;
  orderData: any;


  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.filterForm = this.formBuilder.group({
      status: ['Completed'],
    });
  }

  ngOnInit(): void {
    this.userId = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
  
  }

  
}
