import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  displayedColumns: string[] = [
    'Timestamp',
    'Customer',
    'Plan',
    'Type',
    'Amount',
    'Payment-Provider',
    'Method',
    'Status',
    'Order-Details',

  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource!: MatTableDataSource<any>;

  userData: any[] = [];
  error: any = false;
  result: any;

  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  userListForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  news: any
  userOrders: any;
  survey: any;
  isData:any = true;
  userDetail: any;

  constructor(public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder, private router: Router) {
    this.userListForm = this.formBuilder.group({
      role: [''],
      status: [''],
    });
  }
  
  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
    this.language = this.langService.getLanguaageFile();
    this.getorderedProducts();
    this.getSurveyData();
    this.getNewsData();
  }

  getSurveyData(){
      this.authService.sendRequest('get', 'activesurvey', null).subscribe(
        (respData: any) => {
          this.result = respData;
          if (this.result.success == true) {
            this.survey = this.result.content.dataList;            
          }
        }
      )
  }

  getorderedProducts() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.authService.setLoader(true);
    var endPoint = 'billwerk-orders';
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
        this.isData = false;
      } else if (this.result.success == true) {
        this.userOrders =  this.result.content.dataList ;        
        this.dataSource = new MatTableDataSource(this.userOrders);
        if(this.dataSource.data.length){
          this.isData = true;
        }else{
          this.isData = false;
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    })
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   
    /**set a new filterPredicate function*/
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm: any, key: any) => {  
        return this.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      /**  Transform the filter by converting it to lowercase and removing whitespace. */
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    }
  }

   /** also add this nestedFilterCheck class function */
  nestedFilterCheck(search: any, data: { [x: string]: any; }, key: string) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
          
        }
      }
    } else {
      search += data[key];
    }
    return search;
  }


  sortData(sort: Sort){
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  getNewsData() {
    var endPoint = 'news'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.news;
      } else if (this.result.success == true) {
        this.error = false;
        this.news = this.result.content.dataList;
      }
    })
  }

  userDetails(id:string){
   var endPoint = 'userbycustomerid/'+id
   this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
     this.result = result;
     if (this.result.success == false) {
       this.error = true;
       this.success = false;
       this.message = this.result.content.messageList.user;
     } else if (this.result.success == true) {
       this.error = false;
       this.userDetail = this.result.content.dataList
       this.router.navigate(['/view-profile/'+this.userDetail[0].uid]);
     }
   })
  }

}
