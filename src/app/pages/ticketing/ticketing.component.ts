import { Component, OnInit } from '@angular/core';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-ticketing',
  templateUrl: './ticketing.component.html',
  styleUrls: ['./ticketing.component.css']
})
export class TicketingComponent implements OnInit {


  language: any;

  constructor(private langService:LanguageService) {

   }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
  }

}
