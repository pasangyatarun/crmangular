import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
declare let $: any;

@Component({
  selector: 'app-product-price',
  templateUrl: './product-price.component.html',
  styleUrls: ['./product-price.component.css']
})
export class ProductPriceComponent implements OnInit {

  userData: any;
  language: any;
  result: any;
  error: any;
  productDetails: any;
  productData: any
  msg: any;
  visibility: any = 0;
  customer: any;
  userOrderList: any;
  currentPageNmuber: number = 1;
  itemPerPage = 8;
  limitPerPage = [
    { value: '8' },
    { value: '16' },
    { value: '24' },
    { value: '32' },
    { value: '40' }
  ];

  totalRecord = 0;
  responseMessage: any;
  clubTypeProducts: any = [];
  financialTypeProducts: any = [];
  vereinCloudTypeProduct: any = [];
  vereinspgTypeProduct: any = [];
  productComponent: any = [];
  role: any;

  constructor(private router: Router, public authService: AuthService, private langService: LanguageService) {

  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getAllProducts();
    this.getProductComponent();
  }

  getCustomersDetails() {
    this.authService.setLoader(true);
    var endPoint = 'userbyid/' + this.userData[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.customer = this.result.content.dataList;

      }
    })
  }

  getAllProducts() {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-product'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.productData = this.result.content.dataList;
        this.clubTypeProducts = [];
        this.financialTypeProducts = [];
        this.productData.forEach((element: any) => {
          if (element.planDetail.CustomFields.type == 1) {
            this.clubTypeProducts.push(element);
          } else if (element.planDetail.CustomFields.type == 2) {
            this.financialTypeProducts.push(element);
          } else if (element.planDetail.CustomFields.type == 0) {
            this.vereinCloudTypeProduct.push(element);
          } else if (element.planDetail.CustomFields.type == 3) {
            this.vereinspgTypeProduct.push(element);
          }
        });
        this.totalRecord = this.result.content.dataList.length;
        this.getOrderData();
      }
    })
  }

  getProductComponent() {
    var endPoint = 'crmproducts'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        let isRole = this.role.some((obj: any) => obj.role == 'SGP-Wechsler' || obj.name == 'SGP-Wechsler');
        this.productComponent = this.result.content.dataList;
      }
    })
  }



  getOrderData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.authService.setLoader(true);
    var endPoint = 'billwerk-orders/bycustomer/' + id[0].uid;
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.userOrderList = this.result.content.dataList;
      }
    })
  }

  pageChanged(event: any) {
    this.currentPageNmuber = event;
    this.getAllProducts();
  }

  goToPg(eve: any) {
    this.responseMessage = null;
    if (isNaN(eve)) {
      eve = this.currentPageNmuber;
    } else {
      if (eve > Math.round(this.totalRecord / this.itemPerPage)) {
        this.responseMessage = this.language.productPrice.toOrder;
        setTimeout(() => {
          $('#responseMessage').hide();
          this.responseMessage = "";

        }, 3000);
      } else {
        this.currentPageNmuber = eve;
        this.getAllProducts();
      }
    }
  }


  setItemPerPage(limit: any) {
    if (isNaN(limit)) {
      limit = this.itemPerPage;
    }
    this.itemPerPage = limit;
    this.getAllProducts()
  }

  checkUserForSPG(object: any) {
    return this.role.some((obj: any) => this.authService.isAdmin() || obj.role == 'SGP-Wechsler' || obj.name == 'SGP-Wechsler');
  }

  checkUserForNormal(object: any) {
    return this.role.some((obj: any) => obj.role != 'SGP-Wechsler' && obj.name != 'SGP-Wechsler');
  }

}
