import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { billwerkAccountUrl } from 'src/environments/environment';

@Component({
  selector: 'app-user-invoices-details',
  templateUrl: './user-invoices-details.component.html',
  styleUrls: ['./user-invoices-details.component.css']
})
export class UserInvoicesDetailsComponent implements OnInit {
  userData: any;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  customerId: any;
  EnterData: any;
  language: any;
  customers: any;
  invoices: any;
  contractId: string | null;

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.customerId = this.route.snapshot.paramMap.get('customerId');
    this.contractId = this.route.snapshot.paramMap.get('contractId');

  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getinvoices();
  }

  getinvoices() {
    this.authService.setLoader(true);    
    var endPoint = 'billwerk-invoice-bycustomer/' + this.customerId +'/contract/'+this.contractId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.billwerk;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.billwerk;
        this.EnterData = this.result.content.dataList;
      }
    })

  }



  downloadInvoice(){

    this.authService.setLoader(true);
    var endPoint = 'billwerk-invoice-downloadlink/' + this.EnterData[0].Id;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result      
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.invoices = this.result.content.dataList;
        this.download(this.invoices[0].Url,this.invoices[0].Filename);
      
        
      }
    })
    
  }

   download(url:any, filename:any) {

    var link = document.createElement('a');
    link.href = billwerkAccountUrl+url;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link)

    }


}
