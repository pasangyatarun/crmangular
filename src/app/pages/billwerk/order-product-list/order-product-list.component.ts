import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-order-product-list',
  templateUrl: './order-product-list.component.html',
  styleUrls: ['./order-product-list.component.css']
})
export class OrderProductListComponent implements OnInit {

  displayedColumns: string[] = [
    'Timestamp',
    'Customer',
    'Plan',
    'Status',
    'Type',
    'Amount',
    'Payment-Provider',
    'Method',
    'Order-Details',
    'Order-Delete',

  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;
  error: any = false;
  result: any;

  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  filterForm: FormGroup;
  productStatus: any;
  id: any;
  isData: any = true;
  userDetail: any;

  constructor(private confirmBoxService: ConfirmDialogService, private router: Router, public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.filterForm = this.formBuilder.group({
      status: [''],
    });
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}')
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getOrderData();

  }

  getOrderData() {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-orders'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
        this.isData = false;
      } else if (this.result.success == true) {
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;

        }
        this.getProductStatus();
      }
    })
  }

  userDetails(id:string){
    var endPoint = 'userbycustomerid/'+id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.error = false;
        this.userDetail = this.result.content.dataList
        this.router.navigate(['/view-profile/'+this.userDetail[0].uid]);
      }
    })
   }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    /**set a new filterPredicate function*/
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm: any, key: any) => {
        return this.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      /**  Transform the filter by converting it to lowercase and removing whitespace. */
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    }
  }

  /** also add this nestedFilterCheck class function */
  nestedFilterCheck(search: any, data: { [x: string]: any; }, key: string) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);

        }
      }
    } else {
      search += data[key];
    }
    return search;
  }


  sortData(sort: Sort) {
    /** a Sort sorts the current list, but it wasnt updating it unless i reassigned.*/
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  getProductStatus() {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-order-status'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.productStatus = this.result.content.dataList

      }
    })
  }

  onSubmit() {
    this.authService.setLoader(true);
    this.isSubmitted = true;
    if (this.filterForm.invalid) {
      return;
    } else {
      var endPoint = 'billwerk-order-bystatus'
      this.authService.sendRequest('post', endPoint, this.filterForm.value).subscribe((result: any) => {
        this.authService.setLoader(false
        );
        this.result = result;
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.order;
          this.dataSource = new MatTableDataSource();
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.isData = false;
        } else if (this.result.success == true) {
          this.error = false;
          this.message = this.result.content.content
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          if (this.dataSource.data.length) {
            this.isData = true;
          } else {
            this.isData = false;

          }

        }
      })
    }
  }

  reset() {
    this.filterForm.reset();
    this.getOrderData();
  }

  deleteOrder(orderId: any) {
    let self = this;
    self.confirmBoxService.confirmThis(this.language.billwerk.orderDelete, this.language.confirmBox.deleteOrder, function () {
      self.authService.setLoader(true);

      var endPoint = 'billwerk-order-delete/' + orderId
      self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
        self.result = result
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.order;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = self.language.productPrice.productOrderDelete;
          setTimeout(() => {
            window.location.reload();
          }, 3000)
        }
      })


    }, function () { }
    )

  }

  goTo(id: any) {
    localStorage.setItem('backItem', JSON.stringify('allOrders'));
    this.router.navigate(['/order-details/' + id])
  }

}
