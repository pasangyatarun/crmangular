import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentProductOrderDetailsComponent } from './component-product-order-details.component';

describe('ComponentProductOrderDetailsComponent', () => {
  let component: ComponentProductOrderDetailsComponent;
  let fixture: ComponentFixture<ComponentProductOrderDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentProductOrderDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentProductOrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
