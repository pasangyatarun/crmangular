import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  userData: any;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  orderId: any;
  EnterData: any;
  language: any;
  backUrl: any;

  constructor(private _location: Location,private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {

  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.orderId = this.route.snapshot.paramMap.get('order-id');
    this.backUrl = JSON.parse(localStorage.getItem('backItem') || '{}');    
    this.language = this.langService.getLanguaageFile();
    this.getOrderDetails();
  }
  
  getOrderDetails() {
    this.authService.setLoader(true);
    var endPoint = 'billwerk-orders/' + this.orderId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.EnterData = this.result.content.dataList;

      }
    })

  }



  approvedProduct(){
    // this.authService.setLoader(true);
    // var endPoint = 'billwerk-orders/' + this.orderId;
    // this.authService.sendRequest('get', endPoint, '').subscribe(result => {
    //   this.authService.setLoader(false);
    //   this.result = result
    //   if (this.result.success == false) {
    //     this.msg = this.result.content.messageList.order;
    //   } else if (this.result.success == true) {
    //     this.msg = this.result.content.messageList.order;
    //     this.EnterData = this.result.content.dataList;

    //   }
    // })
    
  }

  declineProduct(){
    // this.authService.setLoader(true);
    // var endPoint = 'billwerk-orders/' + this.orderId;
    // this.authService.sendRequest('get', endPoint, '').subscribe(result => {
    //   this.authService.setLoader(false);
    //   this.result = result
    //   if (this.result.success == false) {
    //     this.msg = this.result.content.messageList.order;
    //   } else if (this.result.success == true) {
    //     this.msg = this.result.content.messageList.order;
    //     this.EnterData = this.result.content.dataList;

    //   }
    // })
    
  }

  commitProduct(){
    this.authService.setLoader(true);
    var endPoint = 'billwerk-order/commit/' + this.orderId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        window.location.reload();
      }
    })
    
  }

  goBack() {
		this._location.back();
	}

}
