import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
declare let $: any;

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  displayedColumns: string[] = [
    'Timestamp',
    'Customer',
    'Plan',
    'Type',
    'Amount',
    'Payment-Provider',
    'Method',
    'Status',
    'Order-Details',

  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;
  error: any = false;
  result: any;

  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  filterForm: FormGroup;
  productStatus: any;
  userData: any;
  isData:any = true ;


  constructor(private router:Router, public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.filterForm = this.formBuilder.group({
      status: [''],
    });
  }
  
  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getOrderData();
   
  }

  getOrderData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.authService.setLoader(true);
    var endPoint = 'billwerk-orders/bycustomer/' + id[0].uid;
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.order;
          this.dataSource = new MatTableDataSource();
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.isData = false;
      } else if (this.result.success == true) {
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if(this.dataSource.data.length){
          this.isData = true;
        }else{
          this.isData = false;

        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.getProductStatus();
      }
    })
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   
    /**set a new filterPredicate function*/
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm: any, key: any) => {  
        return this.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      /**  Transform the filter by converting it to lowercase and removing whitespace. */
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    }
  }

   /** also add this nestedFilterCheck class function */
  nestedFilterCheck(search: any, data: { [x: string]: any; }, key: string) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
          
        }
      }
    } else {
      search += data[key];
    }
    return search;
  }


  sortData(sort: Sort){
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  
  getProductStatus(){
    this.authService.setLoader(true);
    var endPoint = 'billwerk-order-status'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.productStatus = this.result.content.dataList

      }
    })
  }
 
  onSubmit(){
    this.authService.setLoader(true);
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.isSubmitted = true;
    if (this.filterForm.invalid) {
      return;
    } else {
      var endPoint = 'billwerk-bystatususer/' + id[0].uid
      this.authService.sendRequest('post', endPoint, this.filterForm.value).subscribe((result: any) => {
        this.authService.setLoader(false
        );
        this.result = result;
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.billwerk;
          this.dataSource = new MatTableDataSource();
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        } else if (this.result.success == true) {
          this.error = false;
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;


        }
      })
    }
  }

  reset(){
    this.filterForm.reset();
    this.getOrderData();
  }

  goTo(id:any){
    localStorage.setItem('backItem', JSON.stringify('orderHistory'));
    this.router.navigate(['/order-details/'+id])
  }

}
