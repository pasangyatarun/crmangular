import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
import { billwerkAccountUrl } from 'src/environments/environment';
declare let $: any;


@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  displayedColumns: string[] = [
    'Document-Number',
    // 'External_Id-Debitor_Id',
    'Name',
    'Contract',
    'Date',
    'Currency',
    'Net-Total',
    'Gross-Total',
    'Download',

  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource!: MatTableDataSource<any>;
  error: any = false;
  result: any;

  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  productStatus: any;
  invoices: any;
  extArr: any = [];
  fileNameArr: any = [];
  extensions:any;
  isData:any = true ;

  errorUser: any = false;
  respMessage:string = '';
  
  constructor(public authService: AuthService,private router: Router,
     private langService: LanguageService, private formBuilder: FormBuilder) {
  }
  
  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getInvoicesData();

  }

  getInvoicesData(){
    this.authService.setLoader(true);
      var endPoint = 'billwerk-invoices'
      this.authService.sendRequest('get', endPoint, '').subscribe(
        (result: any) => {
        this.authService.setLoader(false
        );
        this.result = result;
        
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.billwerk;
        } else if (this.result.success == true) {
          this.error = false;
          this.message = this.result.content.content
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;          
          if(this.dataSource.data.length){
            this.isData = true;
          }else{
            this.isData = false;

          }
        }
      })
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  sortData(sort: Sort){
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  
  downloadInvoice(invoiceId:any){
    this.authService.setLoader(true);
    var endPoint = 'billwerk-invoice-downloadlink/' + invoiceId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.billwerk;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.billwerk;
        this.invoices = this.result.content.dataList;
        this.download(this.invoices[0].Url,this.invoices[0].Filename);
      }
    })
    
  }

   download(url:any, filename:any) {

    var link = document.createElement('a');
    link.href = billwerkAccountUrl+url;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link)

    }

    profileInfo(custId:any){
      this.authService.setLoader(true);
      var endPoint = 'userbycustomerid/' + custId
      this.authService.sendRequest('get', endPoint, '').subscribe(
        (result: any) => {
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.errorUser = true;
          this.respMessage = result.description;
        } else if (this.result.success == true) {
          if(result.content.dataList.length > 0){
            this.errorUser = false;
            this.result = result.content.dataList[0].uid;
            this.router.navigate(['/view-profile/' + this.result]);
          }else{
            this.errorUser = true;
            this.respMessage = result.content.messageList['user'];            
            setTimeout(() => {
              this.respMessage = '';
              this.errorUser = false;
            }, 2000);
          }
        }
      })
    }
}
