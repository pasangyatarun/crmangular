import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditioncomponentordersComponent } from './additioncomponentorders.component';

describe('AdditioncomponentordersComponent', () => {
  let component: AdditioncomponentordersComponent;
  let fixture: ComponentFixture<AdditioncomponentordersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditioncomponentordersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditioncomponentordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
