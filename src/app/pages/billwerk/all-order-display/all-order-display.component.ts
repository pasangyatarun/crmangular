import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { LanguageService } from 'src/app/service/language.service';
@Component({
  selector: 'app-all-order-display',
  templateUrl: './all-order-display.component.html',
  styleUrls: ['./all-order-display.component.css']
})
export class AllOrderDisplayComponent implements OnInit {

  language: any;
  userDetails: any;
  role: any;
  msg: any;
  displayAllOrderedList: boolean = true;
  displayAdditionalComponents: boolean = false;
  permissions: any;

  constructor(public authService: AuthService, private lang: LanguageService) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.userDetails = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');

  }

  
onSelectCategory(id: any) {
    $('.tab-pane').removeClass('active');
    $('.nav-link').removeClass('active');
    if (id == 1) {
      this.displayAllOrderedList = true;
      this.displayAdditionalComponents = false;
      $('#tabs-1').addClass('active');
      $('#head-allTask').addClass('active');
    }
    if (id == 2) {
      this.displayAllOrderedList = false;
      this.displayAdditionalComponents = true;
      $('#tabs-2').addClass('active');
      $('#head-personalTask').addClass('active');
    }
  }

}
