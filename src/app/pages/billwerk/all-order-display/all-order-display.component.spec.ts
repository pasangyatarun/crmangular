import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllOrderDisplayComponent } from './all-order-display.component';

describe('AllOrderDisplayComponent', () => {
  let component: AllOrderDisplayComponent;
  let fixture: ComponentFixture<AllOrderDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllOrderDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllOrderDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
