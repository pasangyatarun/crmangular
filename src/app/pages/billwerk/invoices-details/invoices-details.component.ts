import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { LoginComponent } from 'src/app/common/login/login.component';
import { billwerkAccountUrl } from 'src/environments/environment';

@Component({
  selector: 'app-invoices-details',
  templateUrl: './invoices-details.component.html',
  styleUrls: ['./invoices-details.component.css']
})
export class InvoicesDetailsComponent implements OnInit {
  userData: any;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  invoiceId: any;
  EnterData: any;
  language: any;
  invoices: any;

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.invoiceId = this.route.snapshot.paramMap.get('invoiceId');
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.getInvoiceDetails();
  }

  getInvoiceDetails() {
    this.authService.setLoader(true);    
    var endPoint = 'billwerk-invoicebyid/' + this.invoiceId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result
       
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.EnterData = this.result.content.dataList;

      }
    })

  }



  downloadInvoice(invoiceId:any){

    this.authService.setLoader(true);
    var endPoint = 'billwerk-invoice-downloadlink/' + invoiceId;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.authService.setLoader(false);
      this.result = result
      
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.invoices = this.result.content.dataList;
        this.download(this.invoices[0].Url,this.invoices[0].Filename);
      
        
      }
    })
    
  }

   download(url:any, filename:any) {

    var link = document.createElement('a');
    link.href = billwerkAccountUrl+url;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link)

    }

}
