import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCompletedOrdersComponent } from './user-completed-orders.component';

describe('UserCompletedOrdersComponent', () => {
  let component: UserCompletedOrdersComponent;
  let fixture: ComponentFixture<UserCompletedOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserCompletedOrdersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCompletedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
