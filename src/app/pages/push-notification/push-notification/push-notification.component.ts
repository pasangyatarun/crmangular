import { Component, OnInit } from '@angular/core';
import { Firestore, collectionData, collection, doc, onSnapshot, addDoc } from '@angular/fire/firestore';
import { AuthService } from 'src/app/service/auth.service';
import { LanguageService } from 'src/app/service/language.service';
declare let $: any;
@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.css']
})
export class PushNotificationComponent implements OnInit {

  result: any;
  userData: any;
  filteredArray: any = [];
  snap: any = [];
  displayNotification: any;
  language: any;
  constructor(private firestore: Firestore, public authService: AuthService, private langService: LanguageService) {

  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.userData = localStorage.getItem('user-data');    
    localStorage.getItem('user-data');
    const unsubscribe = onSnapshot(
      collection(this.firestore, "crmPushNotification"),
      (snapshot) => {
        this.snap = [];        
        snapshot.forEach(element => {
          this.snap.push({ 'id': element.id, 'data': element.data() })

        })

          this.result = this.snap;
          this.filteredArray = [];
          this.result.forEach((element: any) => {
            element.data.toUser.forEach((ele: any) => {
              if (ele.mail == JSON.parse(this.userData)[0].email && ele.status == 0) {
                this.filteredArray.push(element);
                this.filteredArray.sort((a: any, b: any) => b.data.timestamp - a.data.timestamp)
                this.displayNotification = this.filteredArray[0];
                $('#push-notification').modal({
                  backdrop: 'static'
                });
                $('#push-notification').modal('show')
              }

            });
          });
      },
      (error) => {
      });
    }

    readSurvey(id: any) {
     
    var endPoint = 'surveyread'
    
    let read = {"id":id, "loginEmail": JSON.parse(this.userData)[0].email}
    

    this.authService.sendRequest('post', endPoint, read).subscribe((result: any) => {
      this.result = result;

      this.authService.setLoader(false);
      if (this.result.success == false) {
        // this.msg = this.result.content.messageList
        // this.error = true;
        // this.success = false;
        // this.message = this.result.content.messageList.userAdd;
      } else if (this.result.success == true) {
        $('#push-notification').modal('hide')
        // this.error = false;
        // this.success = true;
        // this.message = this.result.content.messageList.userAdd;
        // setTimeout(() => {
        //   this.router.navigate(['/user'])
        // }, 3000)
      }
    })
     
  
    }
}
