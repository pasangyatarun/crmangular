import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
declare var $: any;

@Component({
  selector: 'app-active-survey',
  templateUrl: './active-survey.component.html',
  styleUrls: ['./active-survey.component.css']
})
export class ActiveSurveyComponent implements OnInit {
  currentPageNmuber: number = 1;
  itemPerPage = 8;
  limitPerPage = [
    { value: '8' },
    { value: '16' },
    { value: '24' },
    { value: '32' },
    { value: '40' }
  ];

  totalRecord = 0;
  totalActiveSurvey = 0;
  language: any;
  userDetails: any;
  userRole: any;
  activeSurvey: any;
  responseMessage: any;
  role: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  message: any;
  permissions: any;
  
  constructor(public authService: AuthService, private lang: LanguageService) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.userDetails = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.authService.setLoader(false);
    this.getSurveyData();
  }

  getSurveyData() {
    if (this.authService.isAdmin()) {
      this.authService.setLoader(true);
      this.authService.sendRequest('get', 'activesurvey', null).subscribe(
        (respData: any) => {
          this.result = respData;
          this.authService.setLoader(false);
          if (this.result.success == true) {
            this.result.content.dataList.forEach((element: any) => {
              let cudate = new Date();
              if(cudate.toISOString().split('T')[0] >= element.survey_start_date.split('T')[0]){                 
                let cuday = cudate.getDate().toString().padStart(2, "0");
                let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
                let cuyear = cudate.getFullYear();
                var date1 = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
                var d1 = new Date(date1.split('T')[0]);
                var date2 = new Date(element.survey_end_date.split('T')[0]);
                var time = (d1.getTime() - date2.getTime()) / 1000;
                var days = Math.abs(Math.round(time / (3600 * 24)));
                element.dayCount = days;
                element.remain = this.language.Survey.dayLeft;
              }else{
                var d1 = new Date(element.survey_start_date.split('T')[0]);
                var date2 = new Date(element.survey_end_date.split('T')[0]);
                var time = (d1.getTime() - date2.getTime()) / 1000;
                var days = Math.abs(Math.round(time / (3600 * 24)));
                element.dayCount = days;
                element.remain = this.language.Survey.dayLeft;
              }
              

            });
            this.activeSurvey = this.result.content.dataList;
            this.totalActiveSurvey = this.result.content.dataList.length;
          }
        }
      )
    } else if (!this.authService.isAdmin()) {
      this.authService.setLoader(true);
      this.authService.sendRequest('get', 'activesurvey/' + this.userDetails[0].uid, null).subscribe(
        (respData: any) => {
          this.result = respData;
          this.authService.setLoader(false);
          if (this.result.success == true) {
            this.result.content.dataList.forEach((element: any) => {
              let cudate = new Date()
              let cuday = cudate.getDate().toString().padStart(2, "0");
              let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
              let cuyear = cudate.getFullYear();
              var date1 = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
              var d1 = new Date(date1.split('T')[0]);
              var date2 = new Date(element.survey_end_date.split('T')[0]);
              var time = (d1.getTime() - date2.getTime()) / 1000;
              var year = Math.abs(Math.round((time / (60 * 60 * 24)) / 365.25));
              var month = Math.abs(Math.round(time / (60 * 60 * 24 * 7 * 4)));
              var days = Math.abs(Math.round(time / (3600 * 24)));
              element.dayCount = days;
              element.remain = this.language.Survey.dayLeft;

            });
            this.activeSurvey = this.result.content.dataList;
            this.totalActiveSurvey = this.result.content.dataList.length;

          }
        }
      )
    }
  }

  surveyClose(id: string) {
    this.authService.setLoader(true);
    this.authService.sendRequest('get', 'surveyactiveclose/' + id, null)
      .subscribe(
        (respData: any) => {
          this.result = respData;
          this.authService.setLoader(false);
          if (this.result.success == false) {
            this.msg = this.result.content.messageList
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.survey;
          } else if (this.result.success == true) {
            this.error = false;
            this.success = true;
            this.message = this.result.content.messageList.survey;
            setTimeout(() => {
              this.getSurveyData();
            }, 2000)
          }
        });
  }

  pageChanged(event: any) {
    this.currentPageNmuber = event;
    this.getSurveyData();
  }

  goToPg(eve: any) {
    this.responseMessage = null;
    if (isNaN(eve)) {
      eve = this.currentPageNmuber;
    } else {
      if (eve > Math.round(this.totalActiveSurvey / this.itemPerPage)) {
        this.responseMessage =  this.language.productPrice.toOrder;
        setTimeout(() => {
          $('#responseMessage').hide();
          this.responseMessage = "";

        }, 3000);
      } else {
        this.currentPageNmuber = eve;
        this.getSurveyData();
      }
    }
  }


  setItemPerPage(limit: any) {
    if (isNaN(limit)) {
      limit = this.itemPerPage;
    }
    this.itemPerPage = limit;
    this.getSurveyData()
  }
}
