import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-create-survey',
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.css']
})
export class CreateSurveyComponent implements OnInit {


  surveyForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  language: any;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    maxHeight: '15rem',
    translate: 'no',
    sanitize: true,
    toolbarPosition: 'top',
    defaultFontName: 'Arial',
    defaultFontSize: '2',
    defaultParagraphSeparator: 'p',
    toolbarHiddenButtons: [
      [
        'link',
        'unlink',
        'subscript',
        'superscript',
        'insertUnorderedList',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode',
        'insertImage',
        'insertVideo',
        'bold'
      ]
    ]
  };

  choiceData = [
    { name: 'Web Notification', value: '1' },
    { name: 'Email Notification', value: '2' },
  ];
  userData: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.surveyForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      votingOption: ['', Validators.required],
      surveyEndDate: ['', Validators.required],
      surveyType: ['', Validators.required],
      author: [''],
      surveyStartDate: ['', Validators.required],
      surveyViewOption: [''],
      additionalAnonymousVoting: [''],
      additionalCastVote: [''],
      surveyNotificationOption: this.formBuilder.array([],),
      surveyAnswers: this.formBuilder.array([this.formBuilder.control('', Validators.required)]),

    });

  }




  get surveyAnswers() {
    return this.surveyForm.get('surveyAnswers') as FormArray
  }

  get formControls() { return this.surveyForm.controls; }

  get f() {
    return this.surveyForm.controls;
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.addQuantity();
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}')

  }



  addQuantity() {
    this.surveyAnswers.push(this.formBuilder.control('', Validators.required));
  }

  removeQuantity(i: number) {
    this.surveyAnswers.removeAt(i);
  }


  onCheckSurvey(event: any) {
    const formArray: FormArray = this.surveyForm.get('surveyNotificationOption') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  onSubmit() {

    this.surveyForm.controls["author"].setValue(this.userData[0].uid);
    this.isSubmitted = true;
    if (this.surveyForm.invalid) {
      return;
    } else {
      var anonymous: string = this.surveyForm.value.additionalAnonymousVoting;
      if (anonymous) {
        this.surveyForm.value.additionalAnonymousVoting = anonymous.toString();
      }
      else {
        this.surveyForm.value.additionalAnonymousVoting = 'false' ;
      }
  
      var cast_vote: string = this.surveyForm.value.additionalCastVote;
      if (cast_vote) {
        this.surveyForm.value.additionalCastVote = cast_vote.toString();
      } else {
        this.surveyForm.value.additionalCastVote = 'false';
      }
      var endPoint = 'surveycreate'
      this.authService.setLoader(true);
      this.authService.sendRequest('post', endPoint, this.surveyForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.survey;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.survey;
          setTimeout(() => {
            this.router.navigate(['/survey']);
          }, 3000)
        }
      })
    }
  }


}
