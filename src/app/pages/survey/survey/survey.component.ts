import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';


declare var $: any;
@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {
  language: any;
  userDetails: any;
  role: any;
  msg: any;
  displayActiveSurvey: boolean = true;
  displayMySurvey: boolean = false;
  displayCompletedSurvey: boolean = false;
  permissions: any;

  constructor(public authService: AuthService, private lang: LanguageService, private confirmDialogService: ConfirmDialogService) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.userDetails = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');

  }

  onSurvey(id: any) {
    $('.tab-pane').removeClass('active');
    $('.nav-link').removeClass('active');
    if (id == 1) {
      this.displayActiveSurvey = true;
      this.displayMySurvey = false;
      this.displayCompletedSurvey = false;
      $('#tabs-1').addClass('active');
      $('#head-allTask').addClass('active');
    }
    if (id == 2) {
      this.displayActiveSurvey = false;
      this.displayMySurvey = true;
      this.displayCompletedSurvey = false;
      $('#tabs-2').addClass('active');
      $('#head-personalTask').addClass('active');
    }
    if (id == 3) {
      this.displayActiveSurvey = false;
      this.displayMySurvey = false;
      this.displayCompletedSurvey = true;
      $('#tabs-3').addClass('active');
      $('#head-groupTask').addClass('active');
    }
  }
  
}
