import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ConfirmDialogService } from '../../../confirm-dialog/confirm-dialog.service';


declare var $: any;
@Component({
  selector: 'app-my-votes',
  templateUrl: './my-votes.component.html',
  styleUrls: ['./my-votes.component.css']
})
export class MyVotesComponent implements OnInit {

  showButton: boolean = false;
  language: any;
  surveyData: any;
  activeSurvey: any;
  myVotes: any ;
  Completed: any;
  visibility: any = 1;
  userDetails: any;
  userRole: any;
  responseMessage: any;
  p: number[] = [];
  currentPageNmuber: number = 1;
  itemPerPage = 8;
  limitPerPage = [
    { value: '8' },
    { value: '16' },
    { value: '24' },
    { value: '32' },
    { value: '40' }
  ];
  totalRecord = 0;
  totalActiveSurvey = 0;
  totalCompletedSurvey = 0;
  totalMyVotes = 0;
  result: any;
  role: any;
  msg: any;
  error: any = false;
  success: any = false;
  message: any;
  surveId: any;


  constructor(public authService: AuthService, private lang: LanguageService, private confirmDialogService:ConfirmDialogService) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.userDetails =  JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
  
    // this.userRole = this.userDetails.roles[0];
    this.authService.setLoader(false);

    this.getMyVotes();
  }

  getMyVotes(){
   
      this.authService.setLoader(true);
      this.authService.sendRequest('get', 'myvotedsurvey/'+ this.userDetails[0].uid, null).subscribe(
        (respData:any) => {          
          this.result = respData;
          this.authService.setLoader(false);
          if (this.result.success == true) {
            this.result.content.dataList.forEach((element:  any) => {
              let cudate = new Date()
              let cuday = cudate.getDate().toString().padStart(2, "0");
              let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
              let cuyear = cudate.getFullYear();
              var date1 = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
              var d1 = new Date(date1.split('T')[0]);
              var date2 = new Date(element.survey_end_date.split('T')[0]);
              
              var time = (d1.getTime() - date2.getTime()) / 1000;
              var year = Math.abs(Math.round((time / (60 * 60 * 24)) / 365.25));
              var month = Math.abs(Math.round(time / (60 * 60 * 24 * 7 * 4)));
              var days = Math.abs(Math.round(time / (3600 * 24)));
              element.dayCount = days;
              element.remain =  this.language.Survey.dayLeft;

            });
            this.myVotes = this.result.content.dataList;            
            this.totalMyVotes = this.myVotes.length;
           
          }
        }
      )
  

  }

  goToPg(eve: any) {
    this.responseMessage = null;

    if (isNaN(eve)) {
        eve = this.currentPageNmuber;
    }
    else {
        if (eve > Math.round(this.totalMyVotes / this.itemPerPage)) {
            this.responseMessage = this.language.error_message.invalid_pagenumber;
        }
        else {
            this.currentPageNmuber = eve;
        }
    }
}

setItemPerPage(limit: any) {
  if (isNaN(limit)) {
      limit = this.itemPerPage;
  }
  this.itemPerPage = limit;

  this.getMyVotes();
}

pageChanged(event: any ) {
    this.currentPageNmuber = event;
    this.getMyVotes();
}




}
