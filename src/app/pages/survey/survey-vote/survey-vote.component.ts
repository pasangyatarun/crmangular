import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
	selector: 'app-survey-vote',
	templateUrl: './survey-vote.component.html',
	styleUrls: ['./survey-vote.component.css']
})
export class SurveyVoteComponent implements OnInit {

	surveyId: any;
	surveyData: any;
	surveyVoteForm: FormGroup;
	formSubmit = false;
	responseMessage: any;
	surveyAnswerId: any = [];
	userDetails: any;
	language: any;
	currentDate: Date | undefined;
	finalAns: any;
	ansArray: any = [];
	btnDisable: any;
	result: any;
	role: any;
	msg: any;
	error: any = false;
	success: any = false;
	show: any = false;
	message: any;
	alreadyVoted: any = 0;
	votesurveyanswer: any;
	showall: any;

	constructor(public authService: AuthService, private router: Router, public formBuilder: FormBuilder,
		private lang: LanguageService, private route: ActivatedRoute,) {
		this.surveyVoteForm = this.formBuilder.group({
			// surveyId: [''],
			userId: [''],
			surveyType: ['allUser'],

		});
	}

	ngOnInit(): void {
		this.language = this.lang.getLanguaageFile();
		this.surveyId = this.route.snapshot.paramMap.get('surveyId');
		this.role = JSON.parse(localStorage.getItem('role') || '{}');
		this.userDetails = JSON.parse(localStorage.getItem('user-data') || '{}');
		this.getSurveyData();

		this.getVoteResult();

	}

	getSurveyData() {
		this.authService.setLoader(true);
		this.authService.sendRequest('get', 'surveybyid/' + this.surveyId, null).subscribe(
			(respData: any) => {
				this.result = respData;
				this.authService.setLoader(false);
				if (this.result.success == true) {
					this.result.content.dataList.forEach((element: any) => {
						let cudate = new Date()
						let cuday = cudate.getDate().toString().padStart(2, "0");
						let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
						let cuyear = cudate.getFullYear();
						var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
						var todayDate = new Date(date.split('T')[0]);
						var date2 = new Date(element.created_at.split('T')[0]);
						var time = (todayDate.getTime() - date2.getTime()) / 1000;
						var month = Math.abs(Math.round(time / (60 * 60 * 24 * 7 * 4)));
						var days = Math.abs(Math.round(time / (3600 * 24)));
						if (days > 31) {
							element.month = month + this.language.Survey.month_ago;
						} else if (days < 31) {
							element.month = days + this.language.Survey.day_ago;
						}
						var survey_end_date = new Date(element.survey_end_date.split('T')[0]);
						var Days = (todayDate.getTime() - survey_end_date.getTime()) / 1000;
						if (cudate.toISOString().split('T')[0] >= element.survey_start_date.split('T')[0]) {
							var dayLeft = Math.abs(Math.round(Days / (3600 * 24)))
							element.dayLeft = dayLeft + this.language.Survey.dayLeft;
						} else {							
							var d1 = new Date(element.survey_start_date.split('T')[0]);
							var date2 = new Date(element.survey_end_date.split('T')[0]);
							var time = (d1.getTime() - date2.getTime()) / 1000;
							var days = Math.abs(Math.round(time / (3600 * 24)));
							element.dayLeft = days + this.language.Survey.dayLeft;
						}

						if (element.survey_view_option == 0) {
							if (this.authService.isAdmin()) {
								element.showVote = true;
							} else {
								element.showVote = true;
							}
						} else if (element.survey_view_option == 2) {
							if (this.authService.isAdmin()) {
								element.showVote = true;
							} else {
								element.showVote = false;
							}
						}
						else if (element.survey_view_option == 1) {
							if (this.authService.isAdmin()) {
								element.showVote = true;
							} else {

								let cudate = new Date()
								let cuday = cudate.getDate().toString().padStart(2, "0");
								let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
								let cuyear = cudate.getFullYear();
								var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
								var todayDate = new Date(date.split('T')[0]);
								var date2 = new Date(element.survey_end_date.split('T')[0]);



								if (date2 > todayDate) {
									element.showVote = false;
								} else if (date2 < todayDate) {
									element.showVote = true;
								}
							}
						}
					});
					this.surveyData = this.result.content.dataList;
					this.getVotedOnSurvey();
					var currentDate;
					currentDate = new Date(); // Today's date
					var startDate = new Date(this.surveyData[0].survey_start_date);
					if (startDate <= currentDate) {
						this.btnDisable = true;
					} else {
						this.btnDisable = false;
					}
				}
			}
		)
	}

	getVotedOnSurvey() {
		this.authService.setLoader(true);
		this.authService.sendRequest('get', 'myvotedsurvey/' + this.userDetails[0].uid, null)
			.subscribe(
				(respData: any) => {
					this.result = respData;
					this.authService.setLoader(false);
					if (this.result.success == false) {
						this.msg = this.result.content.messageList
						this.error = true;
						this.success = false;
						this.message = this.result.content.messageList.survey;
					} else if (this.result.success == true) {

						this.result.content.dataList.forEach((element: any) => {

							if (element.id == this.surveyId && this.surveyData[0].additional_cast_vote == 'false') {
								this.alreadyVoted = 1;
							} else if (element.id == this.surveyId && this.surveyData[0].additional_cast_vote == 'true') {
								this.alreadyVoted = 2;
								this.getSurveysurveyvotebyuser();
							}
						});
					}
				}
			)
	}

	getVoteResult() {
		this.authService.setLoader(true);
		this.authService.sendRequest('get', 'votebysurveyanswer/' + this.surveyId, null)
			.subscribe(
				(respData: any) => {
					this.result = respData;


					this.authService.setLoader(false);
					if (this.result.success == false) {
						this.msg = this.result.content.messageList
						this.error = true;
						this.success = false;
						this.message = this.result.content.messageList.survey;
					} else if (this.result.success == true) {

						this.votesurveyanswer = this.result.content.dataList;
						this.votesurveyanswer[1].forEach((element: any) => {
							let ansInfo = element.votedUsers;
							if (ansInfo.length > 0) {
								ansInfo.forEach((ele: any, index: any) => {
									if (this.votesurveyanswer[0].TotalVoteCount != 0) {

										element.per = (((ansInfo.length) / this.votesurveyanswer[0].TotalVoteCount) * 100);

									} else {

										element.per = 0;
									}

								});
							} else {
								element.per = 0;
							}


						});
					}
				}
			)
	}

	getSurveysurveyvotebyuser() {
		this.authService.setLoader(true);
		this.authService.sendRequest('get', 'surveyvotebyuser/' + this.surveyId + '/user/' + this.userDetails[0].uid, null)
			.subscribe(
				(respData: any) => {
					this.result = respData;
					this.authService.setLoader(false);
					if (this.result.success == false) {
						this.msg = this.result.content.messageList
						this.error = true;
						this.success = false;
						this.message = this.result.content.messageList.survey;
					} else if (this.result.success == true) {
						this.result.content.dataList.forEach((element: any) => {
							if (this.surveyData[0].votingOption == 0) {
								var inputs = document.querySelectorAll('.input-radio');
								this.surveyData[0].surveyAnswerOption.forEach((elem: any, index: any) => {
									if (element.survey_answer_id == elem.id) {
										$(inputs[index]).prop("checked", true);
										this.surveyAnswerId.push(element.survey_answer_id);
									}
								});

							} else if (this.surveyData[0].votingOption == 1) {
								var inputs = document.querySelectorAll('.input-checkbox');
								this.surveyData[0].surveyAnswerOption.forEach((elem: any, index: any) => {
									if (element.survey_answer_id == elem.id) {
										$(inputs[index]).prop("checked", true);
										this.surveyAnswerId.push(element.survey_answer_id);
									}
								});
							}


						});

						this.surveyVoteForm.value['surveyAnswerId'] = this.surveyAnswerId;
					}
				}
			)
	}


	onradioChange(e: any) {
		if (e.target.checked) {
			this.surveyAnswerId[0] = e.target.value;
		}

	}

	onCheckboxChange(e: any) {
		if (e.target.checked) {
			this.surveyAnswerId.push(e.target.value);
		} else {
			let i: number = 0;
			this.surveyAnswerId.forEach((item: any) => {
				if (item == e.target.value) {
					this.surveyAnswerId.splice(i, 1);
					return;
				}
				i++;
			});
		}
	}
	errorAnswer: any = { isError: false, errorMessage: '' };

	serveyProcess() {

		if (this.surveyAnswerId && this.surveyAnswerId.length < 1) {
			this.errorAnswer = { isError: true, errorMessage: this.language.error_message.surveyError };
		} else {
			this.errorAnswer = { isError: false, errorMessage: '' };

			this.formSubmit = true;
			if (this.surveyVoteForm.valid && (!this.errorAnswer.isError)) {



				// this.surveyVoteForm.controls["surveyType"].setValue(this.surveyData[0].surveyType);
				this.surveyVoteForm.controls["userId"].setValue(this.userDetails[0].uid);


				if (this.alreadyVoted == 2) {

					if (this.surveyAnswerId) {
						this.surveyVoteForm.value.surveyAnswerId = this.surveyAnswerId;
					}
					this.surveyVoteForm.value.surveyId = JSON.parse(this.surveyId);

					this.authService.sendRequest('put', 'surveyupdatevote', this.surveyVoteForm.value)
						.subscribe(
							(respData: any) => {
								this.result = respData;
								this.authService.setLoader(false);
								if (this.result.success == false) {
									this.msg = this.result.content.messageList
									this.error = true;
									this.success = false;
									this.message = this.result.content.messageList.survey;
								} else if (this.result.success == true) {
									this.error = false;
									this.success = true;
									this.message = this.result.content.messageList.survey;
									setTimeout(() => {
										this.router.navigate(['/survey']);
									}, 3000)
								}
							});
				} else if (this.alreadyVoted == 0) {
					if (this.surveyAnswerId) {
						this.surveyVoteForm.value.surveyAnswerId = this.surveyAnswerId;
					}
					this.surveyVoteForm.value.surveyId = this.surveyData[0].id;
					this.authService.sendRequest('post', 'surveyaddvote', this.surveyVoteForm.value)
						.subscribe(
							(respData: any) => {
								this.result = respData;
								this.authService.setLoader(false);
								if (this.result.success == false) {
									this.msg = this.result.content.messageList
									this.error = true;
									this.success = false;
									this.message = this.result.content.messageList.survey;
								} else if (this.result.success == true) {
									this.error = false;
									this.success = true;
									this.message = this.result.content.messageList.survey;
									setTimeout(() => {
										this.router.navigate(['/survey']);
									}, 3000)
								}
							});
				}

			}
		}
	}



	showAll(ans_id: any) {
		$('#showAll').modal('show');
		this.votesurveyanswer[1].forEach((element: any) => {
			if (element.answerId == ans_id) {
				this.showall = element
			}
		});
	}


}
