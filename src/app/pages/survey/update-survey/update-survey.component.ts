import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { LoginComponent } from 'src/app/common/login/login.component';

@Component({
  selector: 'app-update-survey',
  templateUrl: './update-survey.component.html',
  styleUrls: ['./update-survey.component.css']
})
export class UpdateSurveyComponent implements OnInit {
  surveyForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  error: any = false;
  success: any = false;
  show: any = false;
  language: any;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    maxHeight: '15rem',
    translate: 'no',
    sanitize: true,
    toolbarPosition: 'top',
    defaultFontName: 'Arial',
    defaultFontSize: '2',
    defaultParagraphSeparator: 'p',
    toolbarHiddenButtons: [
      [
        'link',
        'unlink',
        'subscript',
        'superscript',
        'insertUnorderedList',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode',
        'insertImage',
        'insertVideo',
        'bold'
      ]
    ]
  };

  choiceData = [
    { name: 'Web Notification', value: '1', noti_id: '' },
    { name: 'Email Notification', value: '2', noti_id: '' },
  ];
  userData: any;
  surveyId: any;
  surveyData: any;
  btnDisable: any;
  type: any;
  survey_Answers: any;
  surveynotify: any;
  finalSurveyNotify: any;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.surveyForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      votingOption: ['', Validators.required],
      surveyEndDate: ['', Validators.required],
      surveyType: ['', Validators.required],
      author: [''],
      surveyStartDate: ['', Validators.required],
      surveyViewOption: [''],
      additionalAnonymousVoting: [''],
      additionalCastVote: [''],
      surveyNotificationOption: this.formBuilder.array([],),
      // surveyNotificationOption: new FormArray([]),
      surveyAnswers: this.formBuilder.array([this.formBuilder.control('', Validators.required)]),

    });

  }




  get surveyAnswers() {
    return this.surveyForm.get('surveyAnswers') as FormArray
  }

  get formControls() { return this.surveyForm.controls; }

  get f() {
    return this.surveyForm.controls;
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
    this.addQuantity();
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.surveyId = this.route.snapshot.paramMap.get('surveyId');
    this.getSurvey();
  }



  addQuantity() {
    this.surveyAnswers.push(this.formBuilder.control('', Validators.required));
  }

  removeQuantity(i: number) {
    this.surveyAnswers.removeAt(i);
  }


  onCheckSurvey(event: any) {

    const formArray: FormArray = this.surveyForm.get('surveyNotificationOption') as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm

      formArray.push(new FormControl(event.target.value));

      this.finalSurveyNotify = formArray.value;
    }
    /* unselected */
    else {

      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: any) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
        i++;
      });


      this.finalSurveyNotify = formArray.value;
    }

  }

  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }

  getSurvey() {
    this.authService.setLoader(true);
    var endPoint = 'surveybyid/' + this.surveyId;
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result

      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.survey;
      } else if (this.result.success == true) {
        this.surveyData = this.result.content.dataList[0];
        this.setSurveyData();

      }
    })
  }


  setSurveyData() {
    var currentDate;

    currentDate = new Date(); // Today's date
    var startDate = new Date(this.surveyData.survey_start_date);
    if (startDate <= currentDate) {
      this.btnDisable = true;
    } else {
      this.btnDisable = false;
    }


    this.type = [];
    // this.survey_Answers.reset();
    if (this.surveyData.surveyAnswerOption) {
      for (let i = 0; i < this.surveyData.surveyAnswerOption.length; i++) {
        this.surveyAnswers.removeAt(i);
      }
      this.surveyAnswers.removeAt(0);
      this.surveyData.surveyAnswerOption.forEach((element: any) => {
        this.surveyAnswers.push(this.formBuilder.control(element.survey_answer))
      });
    }





    var survey_date;
    var survey_start_date;
    if (this.surveyData.survey_start_date) {
      survey_start_date = this.surveyData.survey_start_date.split("T");
    }

    if (this.surveyData.survey_end_date) {
      survey_date = this.surveyData.survey_end_date.split("T");
    }

    if (this.surveyData.survey_notification_option) {
      this.surveynotify = JSON.parse(this.surveyData.survey_notification_option);
      let self = this;
      this.choiceData.forEach((element: any, index: any) => {
        let noti_id = this.surveynotify.find((o: any) => o === element.value);
        if (noti_id) {
          const formArray: FormArray = this.surveyForm.get('surveyNotificationOption') as FormArray;
          formArray.push(new FormControl(noti_id));
          self.choiceData[index]['noti_id'] = noti_id;
        } else {
          self.choiceData[index]['noti_id'] = '';
        }
      });

    }

    this.surveyForm.controls["title"].setValue(this.surveyData.title);
    this.surveyForm.controls["content"].setValue(this.surveyData.description);
    this.surveyForm.controls["surveyViewOption"].setValue(this.surveyData.survey_view_option);
    this.surveyForm.controls["votingOption"].setValue(this.surveyData.votingOption);

    this.surveyForm.controls["surveyStartDate"].setValue(survey_start_date[0]);
    this.surveyForm.controls["surveyEndDate"].setValue(survey_date[0]);

    this.surveyForm.controls["surveyType"].setValue(this.surveyData.survey_type);
    if (this.surveyData.additional_anonymous_voting == 'true') {
      this.surveyForm.controls["additionalAnonymousVoting"].setValue(this.surveyData.additional_anonymous_voting);
    } else {
      this.surveyForm.controls["additionalAnonymousVoting"].setValue('');

    }
    if (this.surveyData.additional_cast_vote == 'true') {
      this.surveyForm.controls["additionalCastVote"].setValue(this.surveyData.additional_cast_vote);
    } else {
      this.surveyForm.controls["additionalCastVote"].setValue('');

    }

  }



  onSubmit() {
    this.surveyForm.controls["author"].setValue(this.userData[0].uid);
    this.isSubmitted = true;
    if (this.surveyForm.invalid) {
      return;
    } else {
      var anonymous: string = this.surveyForm.value.additionalAnonymousVoting;
      if (anonymous) {
        this.surveyForm.value.additionalAnonymousVoting = anonymous.toString();
      }
      else {
        this.surveyForm.value.additionalAnonymousVoting = 'false';
      }

      var cast_vote: string = this.surveyForm.value.additionalCastVote;
      if (cast_vote) {
        this.surveyForm.value.additionalCastVote = cast_vote.toString();
      } else {
        this.surveyForm.value.additionalCastVote = 'false';
      }
      this.authService.setLoader(true);
      var endPoint = 'surveyupdate/' + this.surveyId
      this.authService.sendRequest('put', endPoint, this.surveyForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.survey;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.survey;
          setTimeout(() => {
            this.router.navigate(['/survey']);
          }, 3000)
        }
      })
    }
  }

}
