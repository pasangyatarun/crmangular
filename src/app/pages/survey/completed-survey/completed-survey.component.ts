import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {ConfirmDialogService} from 'src/app/confirm-dialog/confirm-dialog.service';

declare var $: any;

@Component({
  selector: 'app-completed-survey',
  templateUrl: './completed-survey.component.html',
  styleUrls: ['./completed-survey.component.css']
})
export class CompletedSurveyComponent implements OnInit {

  language: any;
  surveyData: any;
  activeSurvey: any;
  myVotes: any;
  Completed: any;
  visibility: any = 1;
  userDetails: any;
  userRole: any;
  responseMessage: any;
  p: number[] = [];
  currentPageNmuber: number = 1;
  itemPerPage = 8;
  limitPerPage = [
    { value: '8' },
    { value: '16' },
    { value: '24' },
    { value: '32' },
    { value: '40' }
  ];
  totalRecord = 0;
  totalActiveSurvey = 0;
  totalCompletedSurvey = 0;
  totalMyVotes = 0;
  result: any;
  role: any;
  msg: any;
  error: any = false;
  success: any = false;
  message: any;
  surveId: any;
  permissions: any;


  constructor(private confirmBoxService:ConfirmDialogService,public authService: AuthService, private lang: LanguageService) { }

  ngOnInit(): void {
    this.language = this.lang.getLanguaageFile();
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.userDetails = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.authService.setLoader(false);
    this.getCompletedData();

  }

  getCompletedData() {
    if (this.authService.isAdmin()) {
      this.authService.setLoader(true);
      this.authService.sendRequest('get', 'completedsurvey', null).subscribe(
        (respData: any) => {
          this.result = respData;
          this.authService.setLoader(false);
          if (this.result.success == true) {
            this.result.content.dataList.forEach((element: { survey_end_date: string; dayCount: string; }, index: any) => {
              let cudate = new Date()
              let cuday = cudate.getDate().toString().padStart(2, "0");
              let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
              let cuyear = cudate.getFullYear();
              var date1 = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
              var d1 = new Date(date1.split('T')[0]);
              var date2 = new Date(element.survey_end_date.split('T')[0]);
              var time = (d1.getTime() - date2.getTime()) / 1000;
              var year = Math.abs(Math.round((time / (60 * 60 * 24)) / 365.25));
              var month = Math.abs(Math.round(time / (60 * 60 * 24 * 7 * 4)));
              var days = Math.abs(Math.round(time / (3600 * 24)));
              if (days > 31) {
                element.dayCount = this.language.Survey.expired + month + this.language.Survey.month_ago;
              } else if (days < 31) {
                element.dayCount = this.language.Survey.expired  + " " + days + " " + this.language.Survey.day_ago;
              }

            });
            this.Completed = this.result.content.dataList;
            this.totalCompletedSurvey = this.Completed.length;

          }
        }
      )
    } else if (!this.authService.isAdmin()) {
      this.authService.setLoader(true);
      this.authService.sendRequest('get', 'completedsurvey/' + this.userDetails[0].uid, null).subscribe(
        (respData: any) => {
          this.result = respData;
          this.authService.setLoader(false);
          if (this.result.success == true) {
            this.result.content.dataList.forEach((element: { survey_end_date: string; dayCount: string; }, index: any) => {
              let cudate = new Date()
              let cuday = cudate.getDate().toString().padStart(2, "0");
              let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
              let cuyear = cudate.getFullYear();
              var date1 = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
              var d1 = new Date(date1.split('T')[0]);
              var date2 = new Date(element.survey_end_date.split('T')[0]);
              var time = (d1.getTime() - date2.getTime()) / 1000;
              var year = Math.abs(Math.round((time / (60 * 60 * 24)) / 365.25));
              var month = Math.abs(Math.round(time / (60 * 60 * 24 * 7 * 4)));
              var days = Math.abs(Math.round(time / (3600 * 24)));
              if (days > 31) {
                element.dayCount = this.language.Survey.expired + month + this.language.Survey.month_ago;
              } else if (days < 31) {
                element.dayCount = this.language.Survey.expired  + " " + days + " " + this.language.Survey.day_ago;
              }

            });
            this.Completed = this.result.content.dataList;
            this.totalCompletedSurvey = this.Completed.length;

          }
        }
      )
    }
  }



  getId(id: any) {
    this.surveId = id;
  }

 

  deleteSurvey(surveId: any){
    let self = this;
    self.confirmBoxService.confirmThis(this.language.Survey.delete,this.language.confirmBox.delete_survey, function () {

      self.authService.setLoader(true);
    var endPoint = 'surveydelete/' + surveId
    self.authService.sendRequest('delete', endPoint, "").subscribe((result: any) => {
      self.result = result
      self.authService.setLoader(false);
      if (self.result.success == false) {
        self.error = true;
        self.success = false;
        self.message = self.result.content.messageList.survey;
      } else if (self.result.success == true) {
        self.error = false;
        self.success = true;
        self.message = self.result.content.messageList.survey;
        setTimeout(() => {
          self.getCompletedData();
        }, 3000)
      }
    })
     

    }, function () { }
    )

}

  pageChanged(event: any) {
    this.currentPageNmuber = event;
    this.getCompletedData();
  }

  goToPg(eve: any) {
    this.responseMessage = null;
    if (isNaN(eve)) {
      eve = this.currentPageNmuber;
    }
    else {
      if (eve > Math.round(this.totalCompletedSurvey / this.itemPerPage)) {
        this.responseMessage = this.language.error_message.invalid_pagenumber;
        setTimeout(() => {
          $('#responseMessage').hide();
          this.responseMessage = "";

        }, 3000);
      }
      else {
        this.currentPageNmuber = eve;
        this.getCompletedData()
      }
    }
  }

  setItemPerPage(limit: any) {
    if (isNaN(limit)) {
      limit = this.itemPerPage;
    }
    this.itemPerPage = limit;
    this.getCompletedData()
  }

}
