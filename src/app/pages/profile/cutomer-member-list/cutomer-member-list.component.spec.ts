import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CutomerMemberListComponent } from './cutomer-member-list.component';

describe('CutomerMemberListComponent', () => {
  let component: CutomerMemberListComponent;
  let fixture: ComponentFixture<CutomerMemberListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CutomerMemberListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CutomerMemberListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
