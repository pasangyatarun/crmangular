import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/app/service/language.service';
declare let $: any;

@Component({
  selector: 'app-cutomer-member-list',
  templateUrl: './cutomer-member-list.component.html',
  styleUrls: ['./cutomer-member-list.component.css']
})
export class CutomerMemberListComponent implements OnInit {

  memberData: any = [];
  displayedColumnsMember: string[] = [
    'firstName',
    'lastName',
    'Email',
    'DOB',
    'gender',
    'mobile',
    'status',
    'view-details',
  ];

  displayedColumnsAll: string[] = [
    'firstName',
    'lastName',
    'companyName',
    'responsiblePerson',
    'Email',
    'DOB',
    'gender',
    'mobile',
    'status',
    'view-details',
  ];

  displayedColumnsCompany: string[] = [
    'companyName',
    'responsiblePerson',
    'Email',
    'gender',
    'mobile',
    'status',
    'view-details',
  ];
  columnsToDisplay: string[] = this.displayedColumnsMember.slice();
  columnsToDisplayAll: string[] = this.displayedColumnsAll.slice();
  columnsToDisplaycompany: string[] = this.displayedColumnsCompany.slice();
  dataSource !: MatTableDataSource<any>;
  error: any = false;
  result: any;
  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator) matpaginator!: MatPaginator;
  @ViewChild(MatSort) matsort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  roleData: any;
  isSubmitted: any = false;
  message: any;
  success: any = false;
  permissions: any;
  role: any;
  isData: any = true;
  customerId: any;
  EnterData: any;
  imagePreview: any;
  hasPicture = false;
  userData: any;
  view_member_data: any;
  filterForm: FormGroup;
  filterData: any = 'member';
  constructor(public authService: AuthService, private router: Router, private route: ActivatedRoute, private langService: LanguageService, private formBuilder: FormBuilder) {
    this.filterForm = this.formBuilder.group({
      type: [''],
    });
  }

  ngOnInit(): void {
    this.customerId = this.route.snapshot.paramMap.get('customerId');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.filterForm.controls["type"].setValue('member');
    this.getmemberData();
    this.getUserData();
  }

  getUserData() {
    if (this.customerId) {
      this.authService.setLoader(true);
      var endPoint = 'userbyid/' + this.customerId;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.authService.setLoader(false);
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;

          // if(this.EnterData && this.EnterData[0].toogleAccess == 1){
          //   this.accessSupport = true;
          // }else{
          //   this.accessSupport =false;
          // }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    } else {
      this.authService.setLoader(true);
      var endPoint = 'userbyid/' + this.userData[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.authService.setLoader(false);
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;
          // if(this.EnterData && this.EnterData[0].toogleAccess == 1){
          //   this.accessSupport = true;
          // }else{
          //   this.accessSupport =false;
          // }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    }

  }

  getmemberData() {
    this.authService.setLoader(true);
    var endPoint = 'customer-member-filter/' + 'member' + '/customer/' + this.customerId
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
        this.isData = false;
      } else if (this.result.success == true) {
        this.memberData = this.result.content.dataList;
        this.result.content.dataList.forEach((element: any) => {
          element.status = element.status == '1' ? this.language.viewUser.active : 'Inactive'
        });
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;

        }
        this.dataSource.paginator = this.matpaginator;
        this.dataSource.sort = this.matsort;
      }
    })

  }

  onSubmit() {
    this.authService.setLoader(true);
    this.isSubmitted = true;
    if (this.filterForm.invalid) {
      return;
    } else {
      var endPoint = 'customer-member-filter/' + this.filterForm.value.type + '/customer/' + this.customerId
      this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.user;
          this.dataSource = new MatTableDataSource();
          this.dataSource.paginator = this.matpaginator;
          this.dataSource.sort = this.matsort;
          this.isData = false;
        } else if (this.result.success == true) {
          this.memberData = this.result.content.dataList;
          this.filterForm.controls["type"].setValue(this.filterForm.value.type);
          this.filterData = this.filterForm.value.type
          this.result.content.dataList.forEach((element: any) => {
            element.status = element.status == '1' ? this.language.viewUser.active : 'Inactive'
          });
          this.dataSource = new MatTableDataSource(this.result.content.dataList);
          if (this.dataSource.data.length) {
            this.isData = true;
          } else {
            this.isData = false;

          }
          this.dataSource.paginator = this.matpaginator;
          this.dataSource.sort = this.matsort;

        }
      })
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  viewMemberData(id: any) {
    this.authService.setLoader(true);
    var endPoint = 'customer-members/' + id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
        this.isData = false;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element: any) => {

          element.department = JSON.parse(element.department);

          element.status = element.status == '1' ? this.language.viewUser.active : 'Inactive'
        });
        this.view_member_data = this.result.content.dataList;
        $('#member-details').modal('show');

      }
    })
  }

  close() {
    $('#member-details').modal('hide');
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

}
