import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, Color } from 'ng2-charts';

@Component({
  selector: 'app-members-statistics',
  templateUrl: './members-statistics.component.html',
  styleUrls: ['./members-statistics.component.css']
})
export class MembersStatisticsComponent implements OnInit {

  public pieChartOptions1: ChartOptions = {
    responsive: true,


  };
  public pieChartLabels1: Label[] = [['Male'], ['Female'], ['Neuter']];
  public pieChartData1: SingleDataSet = [0, 0, 0];
  public pieChartType1: ChartType = 'pie';
  public pieChartLegend1 = true;
  // public pieChartPlugins1 = [];
  public pieChartPlugins1 = [
  //   {
  //   afterLayout: function (chart:any) {
  //     chart.legend.legendItems.forEach(
  //       (label:any) => {
          
  //         let value = chart.data.datasets[0];
          

  //         label.text += ' ' + value;
  //         return label;
  //       }
  //     )
  //   }
  // }
];


  public pieChartOptions2: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels2: Label[] = [['Active'], ['Inactive']];
  public pieChartData2: SingleDataSet = [0, 0];
  public pieChartType2: ChartType = 'pie';
  public pieChartLegend2 = true;
  public pieChartPlugins2 = [];


  public pieChartOptions3: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels3: Label[] = [['Less then 18'], ['18 to 60'], ['60 above']];
  public pieChartData3: SingleDataSet = [0, 0, 0];
  public pieChartType3: ChartType = 'pie';
  public pieChartLegend3 = true;
  public pieChartPlugins3 = [];


  public pieChartOptions4: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels4: Label[] = [['Yes'], ['No']];
  public pieChartData4: SingleDataSet = [0, 0];
  public pieChartType4: ChartType = 'pie';
  public pieChartLegend4 = true;
  public pieChartPlugins4 = [];

  public pieChartOptions5: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels5: Label[] = [['January'], ['February'], ['March'], ['April'], ['May'], ['June'], ['July'], ['August'], ['September'], ['October'], ['November'], ['December']];
  public pieChartData5: SingleDataSet = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public pieChartType5: ChartType = 'pie';
  public pieChartLegend5 = true;
  public pieChartPlugins5 = [];

  public pieChartOptions6: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels6: Label[] = [['January'], ['February'], ['March'], ['April'], ['May'], ['June'], ['July'], ['August'], ['September'], ['October'], ['November'], ['December']];
  public pieChartData6: SingleDataSet = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public pieChartType6: ChartType = 'pie';
  public pieChartLegend6 = true;
  public pieChartPlugins6 = [];

  public pieChartOptions7: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels7: Label[] = [];
  public pieChartData7: SingleDataSet = [0];
  public pieChartType7: ChartType = 'pie';
  public pieChartLegend7 = true;
  public pieChartPlugins7 = [];

  // publicly define bar Chart
  public barChartOptions1: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    }

  };
  public barChartLabels1: Label[] = ['Gender'];
  public barChartType1: ChartType = 'bar';
  public barChartLegend1 = true;
  public barChartPlugins1 = [];

  public barChartData1: ChartDataSets[] = [
    { data: [0], label: 'Male' },
    { data: [0], label: 'Female' },
    { data: [0], label: 'Neuter' }
  ];

  public barChartOptions2: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    }
  };
  public barChartLabels2: Label[] = ['Status'];
  public barChartType2: ChartType = 'bar';
  public barChartLegend2 = true;
  public barChartPlugins2 = [];

  public barChartData2: ChartDataSets[] = [
    { data: [0], label: 'Active' },
    { data: [0], label: 'Inactive' }];


  public barChartOptions3: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    }
  };
  public barChartLabels3: Label[] = ['Age'];
  public barChartType3: ChartType = 'bar';
  public barChartLegend3 = true;
  public barChartPlugins3 = [];
  public barChartData3: ChartDataSets[] = [
    { data: [0], label: 'Less then 18' },
    { data: [0], label: '18 to 60' },
    { data: [0], label: '60 above' }
  ];



  public barChartOptions4: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    }
  };
  public barChartLabels4: Label[] = ['App in Use'];
  public barChartType4: ChartType = 'bar';
  public barChartLegend4 = true;
  public barChartPlugins4 = [];

  public barChartData4: ChartDataSets[] = [
    { data: [10], label: 'Yes' },
    { data: [20], label: 'No' }];


  public barChartOptions5: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    }
  };
  public barChartLabels5: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartType5: ChartType = 'bar';
  public barChartLegend5 = true;
  public barChartPlugins5 = [];
  monthName: any = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  public barChartData5: ChartDataSets[] = [];

  public barChartOptions6: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    }


  };
  // public chartColors: Array<any> = [
  //   {
  //     backgroundColor: ['rgba(63, 191, 127,  0.4)','rgba(191, 191, 63, 0.4)'],
    
  //      borderColor: ['rgba(63, 191, 127, 0.8)', 'rgba(63, 191, 191, 0.8)'],
  //     hoverBackgroundColor: ['rgba(63, 191, 127, 0.6)', 'rgba(63, 191, 191, 0.6)'],
  //     borderWidth: 2
  //   }];
  public barChartLabels6: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartType6: ChartType = 'bar';
  public barChartLegend6 = true;
  public barChartPlugins6 = [];

  public barChartData6: ChartDataSets[] = [
    { label: 'Entries', data: [0] },
    { label: 'Exists', data: [0] },
  ];

  public barChartOptions7: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true,
         }
      }]
    }

  };
  public barChartLabels7: Label[] = [];
  public barChartType7: ChartType = 'bar';
  public barChartLegend7 = true;
  public barChartPlugins7 = [];

  public barChartData7: ChartDataSets[] = [
    { label: 'Departments Members', data: [0] },
  ];


  // line  charts 
  public lineChartData: ChartDataSets[] = [
    { label: 'Entries', data: [0] },
    { label: 'Exists', data: [0] },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public lineChartOptions: (ChartOptions & { annotation?: any }) = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    }
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

  userData: any;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  customerId: any;
  EnterData: any;
  language: any;
  imagePreview: any;
  hasPicture = false;
  role: any;
  error: boolean = false;
  success: boolean = false;
  memberFilterData: any;
  accessSupport: boolean = false;
  filterType: any = '';
  month: any = [];
  entries: any = [];
  exists: any = [];
  departments: any;
  departmentsName: any = [];
  departmentsMembers: any = [];

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
  }

  ngOnInit(): void {
    this.customerId = this.route.snapshot.paramMap.get('customerId');
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getUserData();
    // this.graphLabelsLangugae();

  }

  getUserData() {
    if (this.customerId) {
      this.getDepartmentlist(this.customerId)
      this.getmemberData(this.customerId);
      this.authService.setLoader(true);
      var endPoint = 'userbyid/' + this.customerId;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.authService.setLoader(false);
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;
          if (this.EnterData && this.EnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    } else {
      this.authService.setLoader(true);
      var endPoint = 'userbyid/' + this.userData[0].uid;
      this.getDepartmentlist(this.userData[0].uid)
      this.getmemberData(this.userData[0].uid);
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.authService.setLoader(false);
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;
          if (this.EnterData && this.EnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    }

  }

  graphLabelsLangugae() {
    this.pieChartLabels1 = [[this.language.memberManagement.male], [this.language.memberManagement.female], [this.language.memberManagement.neuter]];
    this.pieChartLabels2 = [[this.language.viewUser.active], [this.language.memberManagement.inActive]];
    this.pieChartLabels3 = [[this.language.memberManagement.male], [this.language.memberManagement.female], [this.language.memberManagement.neuter]];
    this.pieChartLabels4 = [[this.language.memberManagement.male], [this.language.memberManagement.female], [this.language.memberManagement.neuter]];
    this.pieChartLabels5 = [[this.language.memberManagement.male], [this.language.memberManagement.female], [this.language.memberManagement.neuter]];
    this.pieChartLabels6 = [[this.language.memberManagement.male], [this.language.memberManagement.female], [this.language.memberManagement.neuter]];
    this.pieChartLabels7 = [[this.language.memberManagement.male], [this.language.memberManagement.female], [this.language.memberManagement.neuter]];

  }

  getDepartmentlist(customerId: any) {
    var endPoint = 'customer-departments/' + customerId
    this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {

      } else if (this.result.success == true) {
        this.departments = this.result.content.dataList;
        this.departments.forEach((element: any) => {
          this.departmentsName.push(element.department_name)
        });
        this.departMembers();

        this.barChartLabels7 = this.departmentsName;
        this.pieChartLabels7 = this.departmentsName;
      }
    })
  }

  getmemberData(customerId: any) {
    this.authService.setLoader(true);
    var endPoint = 'membersby-customer/' + customerId
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.memberFilterData = this.result.content.dataList;
        const count = this.memberFilterData.filter((obj: any) => obj.gender === 'male').length;
        this.pieChartData1 = [this.memberFilterData.filter((obj: any) => obj.gender === 'male').length, this.memberFilterData.filter((obj: any) => obj.gender === 'female').length, this.memberFilterData.filter((obj: any) => obj.gender === 'neuter').length];
        this.pieChartData2 = [this.memberFilterData.filter((obj: any) => obj.status === 1).length, this.memberFilterData.filter((obj: any) => obj.status === 0).length];
        this.pieChartData3 = [this.memberFilterData.filter((obj: any) => obj.age < 18).length, this.memberFilterData.filter((obj: any) => obj.age >= 18 && obj.age <= 60).length, this.memberFilterData.filter((obj: any) => obj.age > 60).length];
        this.pieChartData4 = [this.memberFilterData.filter((obj: any) => obj.app_in_use === 'yes').length, this.memberFilterData.filter((obj: any) => obj.app_in_use === 'no').length];
        for (let index = 1; index <= 12; index++) {
          let ind: any
          if (index <= 9) {
            ind = '0' + index
          } else {
            ind = index
          }
          this.month.push(this.memberFilterData.filter((o: any) => JSON.stringify(o.created_at).split('-')[1] == ind).length);
          this.entries.push(this.memberFilterData.filter((o: any) => JSON.stringify(o.club_entry_date).split('-')[1] == ind).length);
          this.exists.push(this.memberFilterData.filter((o: any) => JSON.stringify(o.club_exit_date).split('-')[1] == ind).length);
        }

        this.pieChartData5 = this.month;

        this.barChartData1 = [
          { data: [this.memberFilterData.filter((obj: any) => obj.gender === 'male').length], label: 'Male' },
          { data: [this.memberFilterData.filter((obj: any) => obj.gender === 'female').length], label: 'Female' },
          { data: [this.memberFilterData.filter((obj: any) => obj.gender === 'neuter').length], label: 'Neuter' }

        ];

        this.barChartData2 = [
          { data: [this.memberFilterData.filter((obj: any) => obj.status === 1).length], label: 'Active' },
          { data: [this.memberFilterData.filter((obj: any) => obj.status === 0).length], label: 'Inactive' }

        ];

        this.barChartData3 = [
          { data: [this.memberFilterData.filter((obj: any) => obj.age < 18).length], label: 'Less then 18' },
          { data: [this.memberFilterData.filter((obj: any) => obj.age >= 18 && obj.age <= 60).length], label: '18 to 60' },
          { data: [this.memberFilterData.filter((obj: any) => obj.age > 60).length], label: '60 above' }

        ];

        this.barChartData4 = [
          { data: [this.memberFilterData.filter((obj: any) => obj.app_in_use === 'yes').length], label: 'Yes' },
          { data: [this.memberFilterData.filter((obj: any) => obj.app_in_use === 'no').length], label: 'No' }

        ];

        this.barChartData5 = [
          { data: this.month, label: 'Members' },
        ];


        this.barChartData6 = [
          { data: this.entries, label: 'Entries' },
          { data: this.exists, label: 'Exits' },
        ];

        this.lineChartData = [
          { data: this.entries, label: 'Entries' },
          { data: this.exists, label: 'Exits' },
        ];

        this.barChartData7 = [
          { data: this.entries, label: 'Department Members' },
          { data: this.exists, label: 'Exits' },
        ];
        this.departMembers();
      }
    })

  }

  departMembers() {
    this.departmentsMembers = []
    if (this.memberFilterData && this.memberFilterData.length) {
      this.departmentsName.forEach((element: any) => {
        this.departmentsMembers.push(this.memberFilterData.filter((obj: any) => JSON.parse(obj.department).includes(element)).length);
      });

    }

    this.barChartData7 = [
      { data: this.departmentsMembers, label: 'departments' },

    ];
    this.pieChartData7 = this.departmentsMembers;
  }

  filter(event: any) {
    this.filterType = event.target.value;
    if (event.target.value == 'All') {
      this.filterType = ''
    }

  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }




}
