import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersStatisticsComponent } from './members-statistics.component';

describe('MembersStatisticsComponent', () => {
  let component: MembersStatisticsComponent;
  let fixture: ComponentFixture<MembersStatisticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MembersStatisticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
