import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
declare let $: any;

@Component({
  selector: 'app-user-personalized-docs',
  templateUrl: './user-personalized-docs.component.html',
  styleUrls: ['./user-personalized-docs.component.css']
})
export class UserPersonalizedDocsComponent implements OnInit {

  userData: any;
  language: any;
  result: any;
  error: any = false;
  success: any = false;
  productDetails: any;
  productData: any
  msg: any;
  visibility: any = 0;
  customer: any;
  userOrderList: any;
  imagePreview: any;
  hasPicture = false;
  clubTypeProducts: any = [];
  subTypeProducts: any = [];
  message: any;
  downloadButton: any = false;
  contract: any;
  role: any;
  documents: any =[];
  extArr: any = [];
  viewImage = [];
  docExt: any = [];
  fileNameArr: any = [];
  extAllArr: any = [];
  viewAllImage = [];
  docAllExt: any = [];
  fileNameAllArr: any = [];
  userId: any;
  contactDocuments: any;
  url:any = [];

  constructor(private confirmBoxService: ConfirmDialogService, private router: Router, public authService: AuthService, private langService: LanguageService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');        
    this.userId = this.route.snapshot.paramMap.get('id');    
    this.language = this.langService.getLanguaageFile();
    this.getCustomersDetails();
    this.userDocs();
    this.getAllDocuments();
  }

  getCustomersDetails() {
      var endPoint:string;
    if (this.userId) {
      let userId: any
      userId = this.userId;
      endPoint = 'userbyid/' + userId;
    } else {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}');
      endPoint = 'userbyid/' + id[0].uid;
    }
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.customer = this.result.content.dataList;        
        if (this.customer[0].image != '' && this.customer[0].image != 'undefined') {
          this.hasPicture = true;
          this.imagePreview = this.customer[0].image;
        }
      }
    })
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

  userDocs() {
    
    var endPoint
    if(this.userId){  
      let userId: any    
      userId = this.userId;
      endPoint = 'get-documentbyuserid/' + userId;
    }else{
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}');
      endPoint = 'get-documentbyuserid/' + id[0].uid;
    }    
      this.authService.setLoader(true);
      this.authService.sendRequest('get', endPoint, '').subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.billwerk;
        } else if (this.result.success == true) {
          this.error = false;
          this.result.content.dataList.forEach((element:any) => {
            if(element.user_personalized_pdf_url != ''){
              this.documents.push(element)
            }
          });          
          this. getType();
        }
      })
  }

  getType() {
    for (const key in this.documents) {
      if (Object.prototype.hasOwnProperty.call(this.documents, key)) {
        const element = this.documents[key];
        var ext = element.user_personalized_pdf_url.split(".");
        this.extArr[key] = ext[(ext.length) - 1];
        var fileName = element.user_personalized_pdf_url.split("/");
        this.fileNameArr[key] = decodeURIComponent(fileName[(fileName.length) - 1]);
        var docExt = this.extArr[key];
      }
    }

  }



 previewDocs(url:any){
  this.url = [] ;
   this.url.push(url);
  $('#userDocs').modal({
    backdrop: 'static'
  });
  $('#userDocs').modal('show');
  
  
 }

  close() {
    $('#userDocs').modal('hide');
  }

  download(path:any){
    var link = document.createElement('a');
    link.href = path;
    link.download = path;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
 
  getAllDocuments(){
    this.authService.setLoader(true);
    var endPoint = 'get-user-documents'
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.upload; 
        setTimeout(() => {
          this.error = false;
        }, 2000);
      } else if (this.result.success == true) {
        this.contactDocuments = this.result.content.dataList;        
        this.getAllType();

      }
    })
  }

  getAllType() {    
    for (const key in this.contactDocuments) {

      if (Object.prototype.hasOwnProperty.call(this.contactDocuments, key)) {
        const element =  this.contactDocuments[key];
        var ext = element.path.split(".");
        this.extAllArr[key] = ext[(ext.length) - 1];
        var fileName = element.path.split("/");        
        this.fileNameAllArr[key] = decodeURIComponent(fileName[(fileName.length) - 1]);
        var docExt = this.extAllArr[key];
      }
    }

  }



  // /billwerk-contract-bycustomer/:customer_id

}
