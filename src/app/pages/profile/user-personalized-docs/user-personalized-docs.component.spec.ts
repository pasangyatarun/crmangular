import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPersonalizedDocsComponent } from './user-personalized-docs.component';

describe('UserPersonalizedDocsComponent', () => {
  let component: UserPersonalizedDocsComponent;
  let fixture: ComponentFixture<UserPersonalizedDocsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPersonalizedDocsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPersonalizedDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
