import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
declare let $: any;

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  editUserForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  errorMsg: any;
  show: any = false;
  file: any;
  id: any;
  reEnterData: any;
  language: any;
  error: any = false;
  success: any = false;
  domain: any;
  imageSrc: any;
  hasPicture = false;
  imagePreview: any;
  user: any;
  customer_id: any;
  role: any;
  accessSupport: boolean = false;
  isValidIban!:string;

  constructor(private router: Router, private formBuilder: FormBuilder, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.editUserForm = this.formBuilder.group({
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      society: ['', Validators.required],
      addressLine1: ['', Validators.required],
      zipcode: ["", Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10), Validators.pattern('[0-9]+')])],
      place: ["", Validators.compose([Validators.required, Validators.required])],
      accountNumber: ["", Validators.compose([Validators.pattern('[0-9]+')])],
      contactNumber: ["", Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern('[- +()0-9]+')])],
      country: ['', Validators.required],
      termsCon: ['', Validators.required],
      bankCode: [''],
      iban: [''],
      addressLine2: [''],
      status: [''],
      userRole: new FormArray([]),
      image: [''],
    });

  }


  get formControls() { return this.editUserForm.controls; }

  get f() {
    return this.editUserForm.controls;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');    
    this.language = this.langService.getLanguaageFile();
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.getUserData();

  }

  mustMatch() {
    this.editUserForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }

  validIban(event:any){    
    let validateIban ={
      iban: event.target.value
  }
    var endPoint = 'validate-iban';
    this.authService.sendRequest('post', endPoint, validateIban).subscribe(result => {
      this.result = result;        
      if (this.result.success == false) {
       this.isValidIban  = this.result.content.messageList.iban;
      } else if (this.result.success == true) {
        this.isValidIban = '';          
      }
    })
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.editUserForm.invalid) {
      return;
    } else {
      var formData: any = new FormData();
      this.authService.setLoader(true);
      this.editUserForm.value.image = this.imageSrc;
      let self = this;
      for (const key in this.editUserForm.value) {
        if (Object.prototype.hasOwnProperty.call(this.editUserForm.value, key)) {
          const element = this.editUserForm.value[key];

          if (key == 'image') {
            if (self.imageSrc) {
              formData.append('file', self.imageSrc);
            } else {
              formData.append('imageUrl', self.imagePreview);
            }
          }
          if (key == 'userRole') {
            formData.append('userRole', JSON.stringify(element));
          }
          else {
            if ((key != 'image') && (key != 'userRole')) {
              formData.append(key, element);
            }
          }
        }

      }
      if (this.id) {
        this.id = this.id
      } else {
        let id = JSON.parse(localStorage.getItem('user-data') || '{}');
        this.id = id[0].uid;
      }
      this.editUserForm.value.image = this.file
      var endPoint = 'profile-edit/' + this.id
      this.authService.sendRequest('post', endPoint, formData).subscribe((result: any) => {
        this.result = result;

        if (this.result.success == false) {
          this.msg = this.result.content.messageList
          if (this.msg == null || this.msg == "") {
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.Signup;
          }
        } else if (this.result.success == true) {
          this.message = this.result.content.messageList.Signup;
          this.getOrderProductData();
        }
      })

    }
  }


  onFileSelected(event: any) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0].name
    }
  }

  getUserData() {
    if (this.id) {
      this.authService.setLoader(true);
      var endPoint = 'userbyid/' + this.id;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result;
        this.authService.setLoader(false);

        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.reEnterData = this.result.content.dataList;
          this.reEnterValue();
          if (this.reEnterData && this.reEnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }
          if (this.reEnterData.role == 'admin') {
            this.show = true;
          }

        }
      })
    } else {
      this.authService.setLoader(true);

      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}')
      var endPoint = 'userbyid/' + id[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result;
        this.authService.setLoader(false);

        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.reEnterData = this.result.content.dataList;
          this.reEnterValue();
          if (this.reEnterData && this.reEnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }
          if (this.reEnterData.role == 'admin') {
            this.show = true;
          }

        }
      })
    }
  }

  reEnterValue() {
    this.editUserForm.controls["email"].setValue(this.reEnterData[0].mail);
    this.editUserForm.controls["firstName"].setValue(this.reEnterData[0].firstName);
    this.editUserForm.controls["lastName"].setValue(this.reEnterData[0].lastName);
    this.editUserForm.controls["society"].setValue(this.reEnterData[0].society);
    this.editUserForm.controls["addressLine1"].setValue(this.reEnterData[0].addressLine1);
    this.editUserForm.controls["zipcode"].setValue(this.reEnterData[0].zipcode);
    this.editUserForm.controls["place"].setValue(this.reEnterData[0].place);
    this.editUserForm.controls["accountNumber"].setValue(this.reEnterData[0].accountNumber);
    this.editUserForm.controls["country"].setValue(this.reEnterData[0].country);
    this.editUserForm.controls["contactNumber"].setValue(this.reEnterData[0].contactNumber);
    this.editUserForm.controls["addressLine2"].setValue(this.reEnterData[0].addressLine2);
    this.editUserForm.controls["bankCode"].setValue(this.reEnterData[0].bankCode);
    this.editUserForm.controls["status"].setValue(this.reEnterData[0].status);
    this.editUserForm.controls["iban"].setValue(this.reEnterData[0].ibanValue);
    this.reEnterData[0].role.forEach((element: any) => {
      const formArray: FormArray = this.editUserForm.get('userRole') as FormArray;
      formArray.push(new FormControl(element.rid));
    });

    this.editUserForm.controls["termsCon"].setValue(true);
    if (this.reEnterData[0].image != '' && this.reEnterData[0].image != 'undefined') {
      this.editUserForm.controls['image'].clearValidators();
      this.editUserForm.controls['image'].updateValueAndValidity();
      this.hasPicture = true;
      this.imagePreview = this.reEnterData[0].image;
    }

  }

  // getDomainName() {
  //   this.authService.setLoader(true);
  //   let self = this;
  //   if (localStorage.getItem('token') !== null) {

  //     var endPoint = 'domains';
  //     this.authService.sendRequest('get', endPoint, '').subscribe(result => {
  //       this.result = result
  //       if (this.result.success == false) {
  //         this.msg = this.result.content.messageList.Signup;
  //       } else if (this.result.success == true) {
  //         this.authService.setLoader(false);
  //         this.msg = this.result.content.messageList.Signup;
  //         this.domain = this.result.content.dataList;
  //         if (this.domain.length > 0) {
  //           this.domain.forEach((element: any, index: any) => {
  //             let d_id = this.reEnterData[0].domain.find((o: any) => o.domain_id === element.domain_id);
  //             if (d_id) {
  //               const formArray: FormArray = this.editUserForm.get('domain') as FormArray;
  //               formArray.push(new FormControl(element.domain_id));
  //               self.domain[index]['d_id'] = element.domain_id;
  //             } else {
  //               self.domain[index]['d_id'] = '';
  //             }
  //           });
  //         }
  //       }
  //     })
  //   }
  // }

  // onCheckDomain(event: any) {
  //   const formArray: FormArray = this.editUserForm.get('domain') as FormArray;
  //   /* Selected */
  //   if (event.target.checked) {
  //     // Add a new control in the arrayForm
  //     formArray.push(new FormControl(event.target.value));
  //   }
  //   /* unselected */
  //   else {
  //     // find the unselected element
  //     let i: number = 0;
  //     formArray.controls.forEach((ctrl: any) => {
  //       if (ctrl.value == event.target.value) {
  //         // Remove the unselected element from the arrayForm
  //         formArray.removeAt(i);
  //         return;
  //       }
  //       i++;
  //     });
  //   }
  // }

  errorImage: any = { isError: false, errorMessage: '' };
  uploadFile(event: any) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.errorImage = { isError: true, errorMessage: this.language.error_message.common_valid };
    }
    else {
      this.errorImage = { Error: true, errorMessage: '' };
      this.imageSrc = file;
      this.editUserForm.patchValue({
        image: file
      });
      this.editUserForm.controls["image"].updateValueAndValidity();
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    var url: any;
    let self = this
    reader.onload = function (_event) {
      url = reader.result;
      var imagee = new Image();
      imagee.src = URL.createObjectURL(file);
      imagee.onload = (e: any) => {
        const imagee = e.path[0] as HTMLImageElement;
        var imgHeight = imagee.height
        var imgWidth = imagee.width
      }
      $('#imagePreview').attr('src', url);
    }
    $('#textPreview').show();
    $('#textPreview').text(file.name);
  }

  getOrderProductData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'billwerk-orders/bycustomer/' + id[0].uid;
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        if (this.result.content.dataList.length > 0) {
          this.customer_id = this.result.content.dataList[0].CustomerId;
          if (this.customer_id) {
            this.getUpdateUserData();
          }
        } else {
          this.authService.setLoader(false);
          this.error = false;
          this.success = true;
          setTimeout(() => {
            if (this.id) {
              this.router.navigate(['/view-profile/'+this.id]);

            }else{
              this.router.navigate(['/view-profile']);
            }
          }, 2000);
        }

      }
    })
  }

  getUpdateUserData() {
    if (localStorage.getItem('token') !== null) {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}')
      var endPoint = 'userbyid/' + id[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result;

        if (this.result.success == false) {
          this.error = true;
          this.success = false;
          this.message = this.result.content.messageList.user;
        } else if (this.result.success == true) {
          this.user = this.result.content.dataList
          this.updateBillwerkUser(this.user);
        }
      })
    }
  }

  updateBillwerkUser(user: any) {
    let updateCustomer =
    {
      "companyName": user[0].society,
      "firstName": user[0].firstName,
      "lastName": user[0].lastName,
      "emailAddress": user[0].mail,

      "address": {
        "street": user[0].addressLine1,
        "postalCode": user[0].zipcode,
        "city": user[0].place,
        "country": "DE"
      },
      "defaultBearerMedium": "Email",
      "hidden": false
    };
    var endPoint = 'billwerk-customer-update/' + this.customer_id;
    this.authService.sendRequest('put', endPoint, updateCustomer).subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.error = false;
        this.success = true;

        setTimeout(() => {
          this.router.navigate(['/view-profile']);
        }, 2000);
      }
    })
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

}
