import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOrderHistoryListComponent } from './user-order-history-list.component';

describe('UserOrderHistoryListComponent', () => {
  let component: UserOrderHistoryListComponent;
  let fixture: ComponentFixture<UserOrderHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserOrderHistoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOrderHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
