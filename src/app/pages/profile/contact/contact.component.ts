import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  isSubmitted: any = false;
  message: any;
  result: any;
  error: any = false;
  success: any = false;
  show: any = false;
  language: any;
  reEnterData: any;
  msg: any;
  imagePreview: any;
  hasPicture = false;
  role: any;
  id: any;

  constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, public authService: AuthService, private lang: LanguageService) {
    this.contactForm = this.formBuilder.group({
      uid: [''],
      email: ["", Validators.compose([Validators.required, Validators.pattern("([!# - ' * +/-9=?A-Z^-~-]+(\.[!#-' * +/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])")])],
      subject: ['', Validators.required],
      message: ['', Validators.required],
      status: [''],
    });

  }

  get formControls() { return this.contactForm.controls; }

  get f() {
    return this.contactForm.controls;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');    

    this.mustMatch();
    this.getUserData();
    this.language = this.lang.getLanguaageFile();
    this.role = JSON.parse(localStorage.getItem('role') || '{}');    
  }

  mustMatch() {
    this.contactForm.valueChanges.subscribe(data => {
      if (data.email == data.conEmail) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }


  onSubmit() {
    this.isSubmitted = true;
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    this.contactForm.value.uid = id[0].uid
    if (this.contactForm.invalid) {
      return;
    } else {
      this.authService.setLoader(true);

      if (this.contactForm.value.status == true) {
        this.contactForm.value.status = 1
      } else {
        this.contactForm.value.status = 0
      }
      var endPoint = 'contactadmin'
      this.authService.sendRequest('post', endPoint, this.contactForm.value).subscribe((result: any) => {
        this.result = result;
        this.authService.setLoader(false);
        if (this.result.success == false) {
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.contact;
        } else if (this.result.success == true) {
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.contact;
          this.contactForm.reset();
          this.isSubmitted = false;
        }

      })
    }

  }

  getUserData() {
    if (this.id) {
      var endPoint = 'userbyid/' + this.id;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.reEnterData = this.result.content.dataList;
          if (this.reEnterData[0].image != ''&& this.reEnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.reEnterData[0].image;
          }
          this.reEnterValue();
          if (this.reEnterData.role == 'admin') {
            this.show = true;
          }
        }
      })
    }else{
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}')
      var endPoint = 'userbyid/' + id[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.reEnterData = this.result.content.dataList;
          if (this.reEnterData[0].image != ''&& this.reEnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.reEnterData[0].image;
          }
          this.reEnterValue();
          if (this.reEnterData.role == 'admin') {
            this.show = true;
          }
        }
      })
    }
  }

  reEnterValue() {
    this.contactForm.controls["email"].setValue(this.reEnterData[0].mail);
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/signin'])
  }
  
}
