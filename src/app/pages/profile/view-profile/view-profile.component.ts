import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {

  userData: any;
  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  id: any;
  EnterData: any;
  language: any;
  imagePreview: any;
  hasPicture = false;
  role: any;
  error: boolean = false;
  success: boolean = false;
  memberData: any;
  accessSupport: boolean = false;

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getUserData();
    this.getmemberData();
  }

  getUserData() {
    if (this.id) {
      this.authService.setLoader(true);
      var endPoint = 'userbyid/' + this.id;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.authService.setLoader(false);
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;
          if (this.EnterData && this.EnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    } else {
      this.authService.setLoader(true);
      var endPoint = 'userbyid/' + this.userData[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.authService.setLoader(false);
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;
          if (this.EnterData && this.EnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    }

  }

  getmemberData() {
    this.authService.setLoader(true);
    let id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'membersby-customer/' + id[0].uid
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.memberData = this.result.content.dataList;
      }
    })

  }

  onCheckRole(event: any) {
    let access
    if (event.checked == true) {
      access = {
        "toggleAccess": 1,
      }
    }
    else {
      access = {
        "toggleAccess": 0,
      }
    }
    this.authService.setLoader(true);
    let id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'members-supportaccess/' + id[0].uid
    this.authService.sendRequest('put', endPoint, access).subscribe((result: any) => {
      this.result = result
      this.authService.setLoader(false);

      if (this.result.success == false) {
        this.msg = this.result.content.messageList
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.member;
      } else if (this.result.success == true) {
        this.error = false;
        this.success = true;
        this.message = this.result.content.messageList.member;
        setTimeout(() => {
          this.error = false;
          this.success = false;
          this.message = ''
          this.router.navigate(['/view-profile']);
        }, 1000)
      }
    })
  }

  isCustomer() {
    return this.role.some((o: any) => o.name == 'SGP-Wechsler' || o.role == 'SGP-Wechsler' || o.name == 'Customer' || o.role == 'Customer');
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

  redirect() {
    if (this.id) {
      this.router.navigate(['/user-docs/' + this.id]);
    } else {
      this.router.navigate(['/user-docs']);
    }
  }

}
