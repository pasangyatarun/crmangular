import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
@Component({
  selector: 'app-profile-sidebar',
  templateUrl: './profile-sidebar.component.html',
  styleUrls: ['./profile-sidebar.component.css']
})
export class ProfileSidebarComponent implements OnInit {

  isSubmitted: any = false;
  message: any;
  result: any;
  msg: any;
  show: any = false;
  file: any;
  id: any;
  EnterData: any;
  language: any;
  imagePreview: any;
  hasPicture = false;
  role: any;
  error: boolean = false;
  success: boolean = false;
  memberData: any;
  accessSupport: boolean = false;
  
  @Input() userId: any;
  @Input() userData:any;
  permissions: any;

  constructor(private router: Router, public authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    
  }

  ngOnInit(): void {  
    this.permissions = JSON.parse(localStorage.getItem('permission') || '{}');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    if (this.userData && this.userData[0].toogleAccess == 1) {
      this.accessSupport = true;
    } else {
      this.accessSupport = false;
    }

    // if ((this.userData[0].image || this.userData[0].picture) != '' && (this.userData[0].image || this.userData[0].picture)!= 'undefined') {
    //   this.hasPicture = true;
    //   this.imagePreview = (this.userData[0].image || this.userData[0].picture);
    // }

  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

  redirect() {
    if (this.userId) {
      this.router.navigate(['/user-docs/' + this.userId]);
    } else {
      this.router.navigate(['/user-docs']);
    }
  }

}
