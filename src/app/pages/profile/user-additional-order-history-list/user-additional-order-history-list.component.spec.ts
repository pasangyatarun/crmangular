import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAdditionalOrderHistoryListComponent } from './user-additional-order-history-list.component';

describe('UserAdditionalOrderHistoryListComponent', () => {
  let component: UserAdditionalOrderHistoryListComponent;
  let fixture: ComponentFixture<UserAdditionalOrderHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAdditionalOrderHistoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAdditionalOrderHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
