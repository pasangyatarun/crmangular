
import { Component, Input, OnInit } from '@angular/core';
import { LanguageService } from 'src/app/service/language.service';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
import { User } from 'src/app/models/user.model';
import { Permission } from 'src/app/models/permission.model';
declare let $: any;

@Component({
  selector: 'app-user-additional-order-history-list',
  templateUrl: './user-additional-order-history-list.component.html',
  styleUrls: ['./user-additional-order-history-list.component.css']
})
export class UserAdditionalOrderHistoryListComponent implements OnInit {

  displayedColumns: string[] = [
    'Plan',
    'Amount',
    'Order-Details',
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource!: MatTableDataSource<any>;
  error: boolean = false;
  result: any;
  p: any;
  nameSearch: any;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;
  language: any;
  confirm: any;
  msg: any;
  isSubmitted: boolean = false;
  message: any;
  success: boolean = false;
  role: any;
  productStatus: any;
  id: any;
  isData: boolean = true;
  userDetail!: User[];
  @Input() userId: any;

  constructor(private confirmBoxService: ConfirmDialogService, private router: Router, public authService: AuthService, private langService: LanguageService, private formBuilder: FormBuilder) {

  }
  
  ngOnInit(): void {
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getOrderData();
  }

  getOrderData() {
    this.authService.setLoader(true);
    let endPoint:string;
    let isAdmin = this.authService.isAdmin();
    // if(isAdmin){
    //   endPoint = 'crmproduct-orders-admin';
    // }else{
      let uData = JSON.parse(localStorage.getItem('user-data') || '{}');
      endPoint = 'crmproductOrderList/'+this.userId;
    // }
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;    
        
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
        this.isData = false;
      } else if (this.result.success == true) {
        this.result.content.dataList.forEach((element:any) => {
          var payment = element.payment_method.split(':');
          element.payment_method = payment[0];
          element.payment_provider = payment[1];
        });
        
        this.dataSource = new MatTableDataSource(this.result.content.dataList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;

        }
      }
    })
  }

  userDetails(id:string){
    var endPoint = 'userbycustomerid/'+id
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.user;
      } else if (this.result.success == true) {
        this.error = false;
        this.userDetail = this.result.content.dataList
        this.router.navigate(['/view-profile/'+this.userDetail[0].uid]);
      }
    })
   }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    /**set a new filterPredicate function*/
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm: any, key: any) => {
        return this.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      /**  Transform the filter by converting it to lowercase and removing whitespace. */
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    }
  }

  /** also add this nestedFilterCheck class function */
  nestedFilterCheck(search: any, data: { [x: string]: any; }, key: string) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);

        }
      }
    } else {
      search += data[key];
    }
    return search;
  }


  sortData(sort: Sort) {
    /** a Sort sorts the current list, but it wasnt updating it unless i reassigned.*/
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


  goTo(id: any) {
    localStorage.setItem('backItem', JSON.stringify('allOrders'));
    this.router.navigate(['/order-details/' + id])
  }


}
