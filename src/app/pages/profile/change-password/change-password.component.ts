import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { CustomPasswordValidators } from 'src/app/providers/customPasswordValidators';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  changepassForm: FormGroup
  isSubmitted: boolean = false;
  message: any;
  result: any;
  msg: any;
  show: boolean = false;
  file: any;
  id: any;
  reEnterData: any;
  error: boolean = false;
  success: boolean = false;
  language: any;

  constructor(private router: Router, private formBuilder: FormBuilder, private authService: AuthService, private route: ActivatedRoute, private langService: LanguageService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.changepassForm = this.formBuilder.group(
      {
        id:[this.id],
        oldPassword: ["", Validators.compose([Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')])],
        password: ["", Validators.compose([Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')])],
        confirmPassword: ['', Validators.required],
      },
      {
        validators: [CustomPasswordValidators.mustMatch('password', 'confirmPassword')],
      }
    );

  }


  get formControls() { return this.changepassForm.controls; }

  get f() {
    return this.changepassForm.controls;
  }

  ngOnInit(): void {
    this.language = this.langService.getLanguaageFile();
  }

  mustMatch() {
    this.changepassForm.valueChanges.subscribe(data => {
      if (data.password == data.confirmPassword) {
        this.show = false;
      } else {
        this.show = true;
      }
    });
  }

  onSubmit() {
    this.isSubmitted = true;    
    if (this.changepassForm.invalid) {
      return;
    } else {      
      this.changepassForm.value.image = this.file
      var endPoint = 'changepassword'
      this.authService.sendRequest('post', endPoint, this.changepassForm.value).subscribe((result: any) => {
        this.result = result;
        if (this.result.success == false) {
          this.msg = this.result.content.messageList
            this.error = true;
            this.success = false;
            this.message = this.result.content.messageList.reset;
        } else if (this.result.success == true) {
          this.isSubmitted = false;
          this.changepassForm.reset();
          this.error = false;
          this.success = true;
          this.message = this.result.content.messageList.reset;
        }
      })
    }
  }





}
