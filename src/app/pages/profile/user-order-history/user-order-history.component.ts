import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
import { MatTableDataSource } from '@angular/material/table';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
declare let $: any;

@Component({
  selector: 'app-user-order-history',
  templateUrl: './user-order-history.component.html',
  styleUrls: ['./user-order-history.component.css']
})
export class UserOrderHistoryComponent implements OnInit {

  userData: any;
  language: any;
  result: any;
  error: boolean = false;
  success: boolean = false;
  productData: any
  msg: any;
  imagePreview!: string;
  hasPicture:boolean = false;
  message: any;
  role: any;
  id: any;
  isData: boolean = true;
  accessSupport: boolean = false;
  displayAllOrderedList: boolean = true;
  displayAdditionalComponents: boolean = false;
  permissions: any;
  EnterData: any;
  licenseBlocked: boolean = false;

  constructor(private confirmBoxService: ConfirmDialogService,  private route: ActivatedRoute, private router: Router,
      public authService: AuthService, private langService: LanguageService) {}

  ngOnInit(): void {    
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getUserData();

  }

  getUserData() {
    if (this.id) {
      var endPoint = 'userbyid/' + this.id;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
          
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;
          if (this.EnterData && this.EnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }

          if (this.EnterData && (this.EnterData[0].licence_block_status == '' || this.EnterData[0].licence_block_status == 'active')) {
            this.licenseBlocked = false;
          } else {
            this.licenseBlocked = true;
          }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    } else {
      var endPoint = 'userbyid/' + this.userData[0].uid;
      this.authService.sendRequest('get', endPoint, '').subscribe(result => {
        this.result = result
        if (this.result.success == false) {
          this.msg = this.result.content.messageList.Signup;
        } else if (this.result.success == true) {
          this.msg = this.result.content.messageList.Signup;
          this.EnterData = this.result.content.dataList;
          if (this.EnterData && this.EnterData[0].toogleAccess == 1) {
            this.accessSupport = true;
          } else {
            this.accessSupport = false;
          }

          if (this.EnterData && (this.EnterData[0].licence_block_status == '' || this.EnterData[0].licence_block_status == 'active')) {
            this.licenseBlocked = false;
          } else {
            this.licenseBlocked = true;
          }

          if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
            this.hasPicture = true;
            this.imagePreview = this.EnterData[0].image;
          }
        }
      })
    }

  }

  onSelectCategory(id: any) {
    $('.tab-pane').removeClass('active');
    $('.nav-link').removeClass('active');
    if (id == 1) {
      this.displayAllOrderedList = true;
      this.displayAdditionalComponents = false;
      $('#tabs-1').addClass('active');
      $('#head-allTask').addClass('active');
    }
    if (id == 2) {
      this.displayAllOrderedList = false;
      this.displayAdditionalComponents = true;
      $('#tabs-2').addClass('active');
      $('#head-personalTask').addClass('active');
    }
  }
}
