import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageService } from 'src/app/service/language.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';
import { MatTableDataSource } from '@angular/material/table';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
declare let $: any;

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

  userData: any;
  language: any;
  result: any;
  error: any = false;
  success: any = false;
  productDetails: any;
  productData: any
  msg: any;
  visibility: any = 0;
  customer: any;
  userOrderList: any;
  imagePreview: any;
  hasPicture = false;
  clubTypeProducts: any = [];
  subTypeProducts: any = [];
  message: any;
  downloadButton: any = false;
  contract: any;
  role: any;
  id: any;
  loginData: any;
  loginUserId: any;
  displayedColumns: string[] = [
    'User-Name',
    'Email',
    'firstname',
    'lastname',
    'type',

  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource !: MatTableDataSource<any>;
  isData: any = true;
  accessSupport: boolean = false;

  @ViewChild(MatPaginator) matpaginator!: MatPaginator;
  @ViewChild(MatSort) matsort!: MatSort;

  EnterData: any;
  licenseBlocked: boolean = false;
  supportuserData: any;
  supportuserDataVisible: any = [
    'Administrator', 'Sales-Administrator', 'Support admin', 'Support manager', 'Super-Admin'
  ]


  constructor(private confirmBoxService: ConfirmDialogService, private route: ActivatedRoute, private router: Router,
    public authService: AuthService, private langService: LanguageService) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.id = this.route.snapshot.paramMap.get('id');
    this.role = JSON.parse(localStorage.getItem('role') || '{}');
    this.language = this.langService.getLanguaageFile();
    this.getUserData();
    this.getLoginData();
    this.getOrderData();
    this.supportUserData();
  }

  getUserData() {
    var endPoint: string;
    if (this.id) {
      endPoint = 'userbyid/' + this.id;
    } else {
      endPoint = 'userbyid/' + this.userData[0].uid;
    }
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;

      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.EnterData = this.result.content.dataList;
        if (this.EnterData && this.EnterData[0].toogleAccess == 1) {
          this.accessSupport = true;
        } else {
          this.accessSupport = false;
        }

        if (this.EnterData && (this.EnterData[0].licence_block_status == '' || this.EnterData[0].licence_block_status == 'active')) {
          this.licenseBlocked = false;
        } else {
          this.licenseBlocked = true;
        }

        if (this.EnterData[0].image != '' && this.EnterData[0].image != 'undefined') {
          this.hasPicture = true;
          this.imagePreview = this.EnterData[0].image;
        }
      }
    })

  }


  getOrderData() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}');
    this.authService.setLoader(true);
    var endPoint: string;
    if (this.id) {
      endPoint = 'billwerk-licences/bycustomer/' + this.id;
    } else {
      endPoint = 'billwerk-licences/bycustomer/' + id[0].uid;
    }
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.userOrderList = this.result.content.dataList;
        if (this.userOrderList && this.userOrderList.length > 0) {

          this.getContract();
        }
        this.userOrderList.forEach((element: any) => {
          if (element && element.Contract && element.Contract.PaymentBearer && element.Contract.PaymentBearer.MandateText) {
            this.downloadButton = true
          }
        });
      }
    })
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

  cancleLicense(contractId: any) {
    let cudate = new Date();
    let cancleOrder = {
      "endDate": cudate.toISOString()
    }
    let self = this;
    self.confirmBoxService.confirmThis(this.language.productPrice.cancleOrder, this.language.confirmBox.cancleOrderMsg, function () {
      self.authService.setLoader(true);
      var endPoint = 'billwerk-contract-end/' + contractId
      self.authService.sendRequest('post', endPoint, cancleOrder).subscribe((result: any) => {
        self.result = result;
        self.authService.setLoader(false);
        if (self.result.success == false) {
          self.error = true;
          self.success = false;
          self.message = self.result.content.messageList.billwerk;
        } else if (self.result.success == true) {
          self.error = false;
          self.success = true;
          self.message = 'Your ordered product is canceled successfully';
          location.reload();
        }
      })
    }, function () { }
    )

  }

  downloadPdf() {
    let id: any
    id = JSON.parse(localStorage.getItem('user-data') || '{}')
    var endPoint = 'billwerk-download-mandate/' + id[0].uid;
    this.authService.sendRequest('get', endPoint, '').subscribe(result => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.msg = this.result.content.messageList.Signup;
      } else if (this.result.success == true) {
        this.msg = this.result.content.messageList.Signup;
        this.result.content.dataList;
        this.download(this.result.content.dataList[0].filename);
      }
    });
  }

  download(filename: any) {
    window.open('http://95.111.202.157/' + filename, '_blank')
  }

  getContract() {
    var endPoint = 'billwerk-contract-bycustomer/' + this.userOrderList[0].CustomerId;
    this.authService.setLoader(true);
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = this.result.content.messageList.order;
      } else if (this.result.success == true) {
        this.contract = this.result.content.dataList;
      }
    })
  }

  buttonDisabbled(contractId: any) {
    if (this.contract) {
      return this.contract.some((obj: any) => obj.Id === contractId && obj.LifecycleStatus == 'Active' || obj.LifecycleStatus == 'TemporarilyInactive');
    } else {
      return true;
    }
  }

  getLoginData() {
    var endPoint: string;
    if (!this.id ) {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}');
      endPoint = 'getuserlogininfo/' + id[0].uid;
    } else {
      endPoint = 'getuserlogininfo/' + this.id;
    }
    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      if (this.result.success == false) {
        this.error = this.result.content.messageList.user;
        this.isData = false;
      } else if (this.result.success == true) {
        this.loginData = this.result.content.dataList;
        this.dataSource = new MatTableDataSource(this.result.content.dataList[0].users);
        if (this.dataSource.data.length) {
          this.isData = true;
        } else {
          this.isData = false;
        }
        this.dataSource.paginator = this.matpaginator;
        this.dataSource.sort = this.matsort;
      }
    })

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    // Sort sorts the current list, but it wasnt updating it unless i reassigned.
    this.dataSource.data = this.dataSource.data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this._compare(a[sort.active], b[sort.active], isAsc);
    });
  }

  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  blockLicense(event: any) {
    let self = this;
    let block = {
      'action': ""
    }
    if (this.licenseBlocked == false) {
      self.confirmBoxService.confirmThis('Block License', "Are you sure you want to block the license", function () {
        self.licenseBlocked = true;
        self.authService.setLoader(true);
        let block = {
          action: 'block'
        }
        var endPoint = 'club-licence-block/' + self.id
        self.authService.sendRequest('put', endPoint, block).subscribe((result: any) => {
          self.result = result
          self.authService.setLoader(false);
          if (self.result.success == false) {
            self.error = true;
            self.success = false;
            self.message = self.result.content.messageList.licence;
          } else if (self.result.success == true) {
            self.error = false;
            self.success = true;
            self.message = self.result.content.messageList.licence;
            setTimeout(() => {
              self.success = false;
              self.message = '';
              self.getUserData();
            }, 2000)
          }
        });

      }, function () {
        event.source.checked = false;
      }
      )
    } else if (this.licenseBlocked == true) {
      self.confirmBoxService.confirmThis('Block License', "Are you sure you want to unblock the license", function () {
        self.licenseBlocked = false;
        self.authService.setLoader(true);
        let block = {
          action: 'active'
        }
        var endPoint = 'club-licence-block/' + self.id
        self.authService.sendRequest('put', endPoint, block).subscribe((result: any) => {
          self.result = result
          self.authService.setLoader(false);
          if (self.result.success == false) {
            self.error = true;
            self.success = false;
            self.message = self.result.content.messageList.licence;
          } else if (self.result.success == true) {
            self.error = false;
            self.success = true;
            self.message = self.result.content.messageList.licence;
            setTimeout(() => {
              self.success = false;
              self.message = '';
              self.getUserData();
            }, 2000)
          }
        });

      }, function () {
        event.source.checked = true;
      }
      )
    }
  }



  visibleTo() {    
    let a3 = this.supportuserDataVisible.filter((entry1: any) => this.role.some((entry2: any) => entry1 === (entry2.name || entry2.role)));
    a3 = a3.length > 0 ? true : false;
    return a3
  }

  supportUserStore() {
    var endPoint: string
    if (this.id) {
      endPoint = 'support-user/' + this.id;
    } else {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}');
      endPoint = 'support-user/' + id[0].uid;
    }
    this.authService.setLoader(true);

    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.error = true;
        this.success = false;
        this.message = this.result.content.messageList.club;
      } else if (this.result.success == true) {
        this.error = false;
        this.success = true;
        this.message = this.result.content.messageList.club;
        this.supportUserData();
        setTimeout(() => {
          this.success = false;
          this.message = '';
        }, 3000);
      }
    })

  }

  supportUserData() {
    var endPoint: string
    if (this.id) {
      endPoint = 'support-user-userpass/' + this.id;
    } else {
      let id: any
      id = JSON.parse(localStorage.getItem('user-data') || '{}');
      endPoint = 'support-user-userpass/' + id[0].uid;
    }
    this.authService.setLoader(true);

    this.authService.sendRequest('get', endPoint, "").subscribe((result: any) => {
      this.result = result;
      
      this.authService.setLoader(false);
      if (this.result.success == false) {
        this.success = false;
        this.supportuserData = this.result.content.dataList; 
      } else if (this.result.success == true) {
        this.error = false;
        this.supportuserData = this.result.content.dataList;        
      }
    })
  }

}
