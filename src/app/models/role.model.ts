export interface Role {
    description: string
    role: string
    name: string
    rid: number
    weight: number
    society: string
    r_id:string
}
