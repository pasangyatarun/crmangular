export interface PersonalDoc {
    customer_id: string
    order_id: string
    contract_id: string
    plan_id: string
    plan_variant_id: string
    plan_name: string
    plan_variant_name: string
    user_personalized_pdf_url: string
    created_at: string
    created_by: number
    id: number
    path: string
    updated_at: string
}

