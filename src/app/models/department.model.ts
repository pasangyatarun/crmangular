export interface Department {
    created_at: string
    customer_id: number
    department_name: string
    id: number
    name:string
}
