export interface EmailTemplate {
    footer_content: string
    header_content: string
    id: number
    logo: string
    subject: string
    template_body: string
    template_type: string
    url: string
}