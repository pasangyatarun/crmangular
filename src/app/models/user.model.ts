import { Role } from 'src/app/models/role.model';
export interface User {
    email: string;
    password: string;
    confirmPassword: string;
    firstName: string;
    lastName: string;
    society: string;
    addressLine1: string;
    zipcode: string;
    place: string;
    accountNumber: string;
    bankCode: string;
    contactNumber: string;
    address2: string;
    status: number;
    country: string;
    image: string;
    picture: string;
    userName: string;
    access: string;
    addressLine2: string;
    domain: object
    filename: File
    imageStatus: string;
    username: string;
    mail: string;
    membersince: string;
    role: Role[];
    uid: number
    uri: string;
    toogleAccess: number;
    iban:number;
    ibanValue:string;
    auther_id:string
}
