export interface Permission {
    customerAssign:String[] 
    domain: String[]
    emailTemplate: String[]
    news: String[]
    permission: String[]
    product: String[]
    productAssign: String[]
    productComponent: String[]
    role: String[]
    slicense: String[]
    staticWebPage: String[]
    survey: String[]
    user: String[]
    
}