export interface Survey {
    description: string
    author: string
    title: string
    author_id: number
    status: number
    survey_start_date: string
    id: any
    survey_end_date: string
    created_at: string
    dayCount: number
    remain: string
    additional_cast_vote: string
    votingOption: number
    surveyAnswer: { id: number, surveyId: number, surveyAnswer: string }
    TotalVoteCount: number
    answerId: number
    per: number
    survey_answer: string
    survey_id: number
    votedUsers: { user_id: string, voterName: string }
    votedUsersCount: number
    surveyAnswerOption: SurveyAnswerOption[]
    additional_anonymous_voting: string
    dayLeft: string
    month: string
    showVote: boolean
    survey_type: string
    survey_view_option: string
    survey_notification_option:string
}

export interface SurveyAnswerOption {
    id: number
    survey_answer: string
    survey_id: number
}

export interface VoteAnswer{
    TotalVoteCount:number
    answerId: number
    id: number
    per: number
    survey_answer: string
    votedUsers:{user_id: number, voterName: string}
  }