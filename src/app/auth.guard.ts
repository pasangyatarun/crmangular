import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  process: any;
  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.process = localStorage.getItem('process') || '{}'

    if (this.authService.IsLoggedIn() && (this.process == 'n1' || this.process == 's1')) {
      if (this.process == 'n1') {
        this.router.navigate(['/profile-edit']);
        return false;
      } else {
        this.router.navigate(['/profile-edit-spg']);
        return false;
      }
    } else if (this.authService.IsLoggedIn() || (this.process == 'n0' || this.process == 's0')) {
      return true;
    }
    else {
      this.router.navigate(['/signin']);
      return false;
    }
  }

}
