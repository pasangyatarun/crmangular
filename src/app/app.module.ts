import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgxCaptchaModule } from 'ngx-captcha'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './common/login/login.component';
import { RegisterComponent } from './common/register/register.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { HeaderComponent } from './common/header/header.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';
import { DashboardComponent } from './pages/dashboards/dashboard/dashboard.component';
import { LayoutComponent } from './common/layout/layout.component';
import { ForgetPasswordComponent } from './common/forget-password/forget-password.component';
import { FaqComponent } from './pages/content/add-content/faq/faq.component';
import { ViewProfileComponent } from './pages/profile/view-profile/view-profile.component';
import { PasswordComponent } from './common/email-verify/emailVerify-Password/password/password.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
registerLocaleData(localeDe);

import localeEl from '@angular/common/locales/en';
registerLocaleData(localeEl, 'en');


import * as $ from "jquery";
import { ContactComponent } from './pages/profile/contact/contact.component';
import { QuestionnaireComponent } from './pages/content/add-content/questionnaire/questionnaire.component';
import { OnlineHelpComponent } from './pages/content/add-content/online-help/online-help.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { SLicenseComponent } from './pages/content/add-content/license/s-license/s-license.component';
import { UserComponent } from './pages/user/user/user.component';
import { AddUserComponent } from './pages/user/add-user/add-user.component';
import { EditUserComponent } from './pages/user/edit-user/edit-user.component';
import { ForgetPasswordResetComponent } from './common/forget-password-reset/forget-password-reset.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ChangePasswordComponent } from './pages/profile/change-password/change-password.component';
import { AddNewsComponent } from './pages/content/add-content/news/add-news/add-news.component';
import { EditNewsComponent } from './pages/content/add-content/news/edit-news/edit-news.component';
import { ViewNewsComponent } from './pages/content/add-content/news/view-news/view-news.component';
import { EditRoleComponent } from './pages/role/edit-role/edit-role.component';
import { RolesComponent } from './pages/role/roles/roles.component';
import { ViewSLicenseComponent } from './pages/content/add-content/license/view-s-license/view-s-license.component';
import { EditSLicenseComponent } from './pages/content/add-content/license/edit-s-license/edit-s-license.component';
import { CustomerAssignComponent } from './pages/CustomerAssign/customer-assign/customer-assign.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ViewCustomerAssignComponent } from './pages/CustomerAssign/view-customer-assign/view-customer-assign.component';
import { AddPermissionComponent } from './pages/permisson/add-permission/add-permission.component';
import { ViewPermissionComponent } from './pages/permisson/view-permission/view-permission.component';
import { EditPermissionComponent } from './pages/permisson/edit-permission/edit-permission.component';
import { ProductAssignComponent } from './pages/assign-products/product-assign/product-assign.component';
import { ViewAssignedProductsComponent } from './pages/assign-products/view-assigned-products/view-assigned-products.component';
import { AddPagesComponent } from './pages/content/add-content/static-pages/add-pages/add-pages.component';
import { ViewPagesComponent } from './pages/content/add-content/static-pages/view-pages/view-pages.component';
import { EditPagesComponent } from './pages/content/add-content/static-pages/edit-pages/edit-pages.component';
import { TrialRegistrationComponent } from './common/trial-registration/trial-registration.component';
import { TrialEmailVerifyComponent } from './common/email-verify/trial-email-verify/trial-email-verify.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { TruncateModule } from 'ng2-truncate';
import { NewsDescriptionComponent } from './pages/content/add-content/news/news-description/news-description.component';
import { AllNewsComponent } from './pages/content/add-content/news/all-news/all-news.component';
import { OrderProductListComponent } from './pages/billwerk/order-product-list/order-product-list.component';
import { OrderDetailsComponent } from './pages/billwerk/order-details/order-details.component';
import { OrderHistoryComponent } from './pages/billwerk/order-history/order-history.component';
import { CompletedOrdersComponent } from './pages/billwerk/completed-orders/completed-orders.component';
import { InvoicesComponent } from './pages/billwerk/invoices/invoices.component';
import { InvoicesDetailsComponent } from './pages/billwerk/invoices-details/invoices-details.component';
import { UserInvoicesDetailsComponent } from './pages/billwerk/user-invoices-details/user-invoices-details.component';
import { UserCompletedOrdersComponent } from './pages/billwerk/user-completed-orders/user-completed-orders.component';
import { CreateSurveyComponent } from './pages/survey/create-survey/create-survey.component';
import { PushNotificationComponent } from './pages/push-notification/push-notification/push-notification.component';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { provideFirestore,getFirestore } from '@angular/fire/firestore';
import { fireStore } from '../environments/environment';
import { SurveyComponent } from './pages/survey/survey/survey.component';
import { MatTabsModule } from '@angular/material/tabs';
import { UpdateSurveyComponent } from './pages/survey/update-survey/update-survey.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog/confirm-dialog.component';
import { ProductPriceComponent } from './pages/product-price/product-price.component';
import { ClubsComponent } from './pages/clubs/clubs.component';
import { TicketingComponent } from './pages/ticketing/ticketing.component';
import { RequestComponent } from './pages/request/request.component';
import { SurveyVoteComponent } from './pages/survey/survey-vote/survey-vote.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {ProgressBarModule} from "angular-progress-bar"
import {MatIconModule} from '@angular/material/icon';
import { ActiveSurveyComponent } from './pages/survey/active-survey/active-survey.component';
import { MyVotesComponent } from './pages/survey/my-votes/my-votes.component';
import { CompletedSurveyComponent } from './pages/survey/completed-survey/completed-survey.component';
import { UserRolesComponent } from './pages/role/user-roles/user-roles.component';
import { ViewSurveyComponent } from './pages/survey/view-survey/view-survey.component';
import { CreateEmailTemplateComponent } from './pages/email-template/create-email-template/create-email-template.component';
import { EmailTemplateComponent } from './pages/email-template/email-template/email-template.component';
import { UpdateEmailTemplateComponent } from './pages/email-template/update-email-template/update-email-template.component';
import { UserDashboardComponent } from './pages/dashboards/user-dashboard/user-dashboard.component';
import { AdminDashboardComponent } from './pages/dashboards/admin-dashboard/admin-dashboard.component';
import { BillwerkProductPriceComponent } from './common/billwerk-products/billwerk-product-price/billwerk-product-price.component';
import { NewsDetailsComponent } from './pages/content/add-content/news/news-details/news-details.component';
import { BillwerkProductsDetailsComponent } from './common/billwerk-products/billwerk-products-details/billwerk-products-details.component';
import { OuterHeaderComponent } from './common/outer-header/outer-header.component';
import { OuterLayoutComponent } from './common/outer-layout/outer-layout.component';
import { ContentComponent } from './common/content/content.component';
import { EditProfilePopUpComponent } from './common/edit-profile-pop-up/edit-profile-pop-up.component';
import { ContactDocumentsComponent } from './pages/contact-documents/contact-documents.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { ProductComponentDetailsComponent } from './pages/product-component-details/product-component-details.component';
import { BillwerkProductsComponentDetailsComponent } from './common/billwerk-products/billwerk-products-component-details/billwerk-products-component-details.component';
import { AddUserEmailVerifyComponent } from './common/email-verify/add-user-email-verify/add-user-email-verify.component';
import { AdminAssignProductComponent } from './pages/admin-assign-product/admin-assign-product/admin-assign-product.component';
import { AdminAssignedProductsViewComponent } from './pages/admin-assign-product/admin-assigned-products-view/admin-assigned-products-view.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SpgInstallationComponent } from './common/email-verify/spg-installation/spg-installation.component';
import { UserPersonalizedDocsComponent } from './pages/profile/user-personalized-docs/user-personalized-docs.component';
import { NormalInstallationComponent } from './common/email-verify/normal-installation/normal-installation.component';
import { MemberManagementComponent } from './pages/member-management/member-management/member-management.component';
import { AddMemberComponent } from './pages/member-management/add-member/add-member.component';
import { EditMemberComponent } from './pages/member-management/edit-member/edit-member.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { CutomerMemberListComponent } from './pages/profile/cutomer-member-list/cutomer-member-list.component';
import { ProfileSidebarComponent } from './pages/profile/profile-sidebar/profile-sidebar.component';
import { MembersStatisticsComponent } from './pages/profile/members-statistics/members-statistics.component';
import { DepartmentsListComponent } from './pages/member-management/department/departments-list/departments-list.component';
import { EditDepartmentsComponent } from './pages/member-management/department/edit-departments/edit-departments.component';
import { InformationComponent } from './pages/profile/information/information.component';
import { ErrorHandlerComponent } from './common/error-handler/error-handler.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AddProductComponent } from './pages/component-products/add-product/add-product.component';
import { ViewProductComponent } from './pages/component-products/view-product/view-product.component';
import { EditProductComponent } from './pages/component-products/edit-product/edit-product.component';
import { AllOrderDisplayComponent } from './pages/billwerk/all-order-display/all-order-display.component';
import { AdditioncomponentordersComponent } from './pages/billwerk/additioncomponentorders/additioncomponentorders.component';
import { ComponentProductOrderDetailsComponent } from './pages/billwerk/component-product-order-details/component-product-order-details.component';
import { UserInvoicesComponent } from './pages/billwerk/user-invoices/user-invoices.component';
import { UserOrderHistoryComponent } from './pages/profile/user-order-history/user-order-history.component';
import { UserOrderHistoryListComponent } from './pages/profile/user-order-history-list/user-order-history-list.component';
import { UserAdditionalOrderHistoryListComponent } from './pages/profile/user-additional-order-history-list/user-additional-order-history-list.component';
import { SalesPartnerComponent } from './pages/sales_partner/sales-partner/sales-partner.component';
import { AssignedCustomersComponent } from './pages/sales_partner/assigned-customers/assigned-customers.component';

export function getCulture() {
  let language = localStorage.getItem('language');
  if(language=="en"){
    return 'en';
  }else if(language=="ru"){
    return 'ru';
  }else if(language=="tr"){
    return 'tr';
  }else{
    return 'de';
  }

}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    PageNotFoundComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    LayoutComponent,
    FaqComponent,
    ForgetPasswordComponent,
    ViewProfileComponent,
    PasswordComponent,
    EditProfileComponent,
    ContactComponent,
    QuestionnaireComponent,
    OnlineHelpComponent,
    ProductDetailComponent,
    SLicenseComponent,
    UserComponent,
    AddUserComponent,
    EditUserComponent,
    ForgetPasswordResetComponent,
    ChangePasswordComponent,
    AddNewsComponent,
    EditNewsComponent,
    ViewNewsComponent,
    EditRoleComponent,
    RolesComponent,
    ViewSLicenseComponent,
    EditSLicenseComponent,
    CustomerAssignComponent,
    ViewCustomerAssignComponent,
    AddPermissionComponent,
    ViewPermissionComponent,
    EditPermissionComponent,
    ProductAssignComponent,
    ViewAssignedProductsComponent,
    AddPagesComponent,
    ViewPagesComponent,
    EditPagesComponent,
    TrialRegistrationComponent,
    TrialEmailVerifyComponent,
    NewsDescriptionComponent,
    AllNewsComponent,
    OrderProductListComponent,
    OrderDetailsComponent,
    OrderHistoryComponent,
    CompletedOrdersComponent,
    InvoicesComponent,
    InvoicesDetailsComponent,
    UserInvoicesDetailsComponent,
    UserCompletedOrdersComponent,
    CreateSurveyComponent,
    PushNotificationComponent,
    SurveyComponent,
    UpdateSurveyComponent,
    ConfirmDialogComponent,
    ProductPriceComponent,
    SalesPartnerComponent,
    ClubsComponent,
    TicketingComponent,
    RequestComponent,
    SurveyVoteComponent,
    ActiveSurveyComponent,
    MyVotesComponent,
    CompletedSurveyComponent,
    UserRolesComponent,
    ViewSurveyComponent,
    CreateEmailTemplateComponent,
    EmailTemplateComponent,
    UpdateEmailTemplateComponent,
    UserDashboardComponent,
    AdminDashboardComponent,
    BillwerkProductPriceComponent,
    NewsDetailsComponent,
    BillwerkProductsDetailsComponent,
    OuterHeaderComponent,
    OuterLayoutComponent,
    ContentComponent,
    EditProfilePopUpComponent,
    ContactDocumentsComponent,
    ProductComponentDetailsComponent,
    BillwerkProductsComponentDetailsComponent,
    AddUserEmailVerifyComponent,
    AdminAssignProductComponent,
    AdminAssignedProductsViewComponent,
    SpgInstallationComponent,
    UserPersonalizedDocsComponent,
    NormalInstallationComponent,
    MemberManagementComponent,
    AddMemberComponent,
    EditMemberComponent,
    CutomerMemberListComponent,
    ProfileSidebarComponent,
    MembersStatisticsComponent,
    DepartmentsListComponent,
    EditDepartmentsComponent,
    InformationComponent,
    ErrorHandlerComponent,
    AddProductComponent,
    ViewProductComponent,
    EditProductComponent,
    AllOrderDisplayComponent,
    AdditioncomponentordersComponent,
    ComponentProductOrderDetailsComponent,
    UserInvoicesComponent,
    UserOrderHistoryComponent,
    UserOrderHistoryListComponent,
    UserAdditionalOrderHistoryListComponent,
    AssignedCustomersComponent,

  ],
  imports: [
    NgxPaginationModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxCaptchaModule,
    Ng2SearchPipeModule,
    MatTableModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    AngularEditorModule,
    NgMultiSelectDropDownModule.forRoot(),
    provideFirebaseApp(() => initializeApp(fireStore)),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 50,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    }),
    TruncateModule,
    MatTabsModule,
    MatProgressBarModule,
    ProgressBarModule,
    MatIconModule,
    MatDialogModule,
    NgxDocViewerModule,
    PdfViewerModule,
    MatSlideToggleModule,
    ChartsModule,
    MatSnackBarModule,
  ],
  providers: [ConfirmDialogComponent ,  { provide: LOCALE_ID, useValue: getCulture() }],
  bootstrap: [AppComponent]

})
export class AppModule { }
