import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'test';
  result: any;

  constructor(private authService: AuthService, private route: Router) {
  }

  public ngOnInit() {
    $(document).ready(function () {
      $('button.navbar-toggler').on('click', function () {
        $('.header .navbar-collapse').toggleClass('expand');
      });
    });

    if (!localStorage.getItem('language')) {
      localStorage.setItem('language', 'en');
    }

    this.setLanguage();
  }

  setLanguage() {
    let lan = localStorage.getItem('language');
    var endPoint = 'language'
    let data = {
      "language": lan
    }
    this.authService.sendRequest('post', endPoint, data).subscribe((result: any) => {
      this.result = result;
    })
  }

}
