import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import {ConfirmDialogService} from './confirm-dialog.service';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    //   ConfirmDialogComponent
  ],
  imports: [
      BrowserModule,
      CommonModule
  ],
  exports: [
    //   ConfirmDialogComponent
  ],
  providers: [
     ConfirmDialogService
  ]
})
export class ConfirmDialogModule { }
