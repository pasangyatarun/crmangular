import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ConfirmDialogService {
    private subject = new Subject<any>();

    confirmThis(heading: string, message: string, yesFn: () => void, noFn: () => void): any {
        this.setConfirmation(heading, message, yesFn, noFn);
    }

    setConfirmation(heading: string, message: string, yesFn: () => void, noFn: () => void): any {
        const that = this;
        this.subject.next({
            type: 'confirm',
            text: message,
            head: heading,
            yesFn(): any {
                that.subject.next(undefined); // that will close the modal
                yesFn();
            },
            noFn(): any {
                that.subject.next(undefined);
                noFn();
            }
        });

    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
