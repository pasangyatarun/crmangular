import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';

import { LayoutComponent } from './common/layout/layout.component';
import { LoginComponent } from './common/login/login.component';
import { RegisterComponent } from './common/register/register.component';
import { DashboardComponent } from './pages/dashboards/dashboard/dashboard.component';
import { FaqComponent } from './pages/content/add-content/faq/faq.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { ForgetPasswordComponent } from './common/forget-password/forget-password.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { ViewProfileComponent } from './pages/profile/view-profile/view-profile.component';
import { TermsAndConditionComponent } from './common/terms-and-condition/terms-and-condition.component';
import { PasswordComponent } from './common/email-verify/emailVerify-Password/password/password.component';
import { ContactComponent } from './pages/profile/contact/contact.component';
import { QuestionnaireComponent } from './pages/content/add-content/questionnaire/questionnaire.component';
import { OnlineHelpComponent } from './pages/content/add-content/online-help/online-help.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { SLicenseComponent } from './pages/content/add-content/license/s-license/s-license.component';
import { UserComponent } from './pages/user/user/user.component';
import { AddUserComponent } from './pages/user/add-user/add-user.component';
import { EditUserComponent } from './pages/user/edit-user/edit-user.component';
import { ForgetPasswordResetComponent } from './common/forget-password-reset/forget-password-reset.component';
import { ChangePasswordComponent } from './pages/profile/change-password/change-password.component';
import { AddNewsComponent } from './pages/content/add-content/news/add-news/add-news.component';
import { ViewNewsComponent } from './pages/content/add-content/news/view-news/view-news.component';
import { EditNewsComponent } from './pages/content/add-content/news/edit-news/edit-news.component';
import { EditRoleComponent } from './pages/role/edit-role/edit-role.component';
import { RolesComponent } from './pages/role/roles/roles.component';
import { ViewSLicenseComponent } from './pages/content/add-content/license/view-s-license/view-s-license.component';
import { EditSLicenseComponent } from './pages/content/add-content/license/edit-s-license/edit-s-license.component';
import { CustomerAssignComponent } from './pages/CustomerAssign/customer-assign/customer-assign.component';
import { ViewCustomerAssignComponent } from './pages/CustomerAssign/view-customer-assign/view-customer-assign.component';
import { AddPermissionComponent } from './pages/permisson/add-permission/add-permission.component';
import { ViewPermissionComponent } from './pages/permisson/view-permission/view-permission.component';
import { EditPermissionComponent } from './pages/permisson/edit-permission/edit-permission.component';
import { ProductAssignComponent } from './pages/assign-products/product-assign/product-assign.component';
import { ViewAssignedProductsComponent } from './pages/assign-products/view-assigned-products/view-assigned-products.component';
import { AddPagesComponent } from './pages/content/add-content/static-pages/add-pages/add-pages.component';
import { ViewPagesComponent } from './pages/content/add-content/static-pages/view-pages/view-pages.component';
import { EditPagesComponent } from './pages/content/add-content/static-pages/edit-pages/edit-pages.component';
import { TrialRegistrationComponent } from './common/trial-registration/trial-registration.component';
import { TrialEmailVerifyComponent } from './common/email-verify/trial-email-verify/trial-email-verify.component';
import { NewsDescriptionComponent } from './pages/content/add-content/news/news-description/news-description.component';
import { AllNewsComponent } from './pages/content/add-content/news/all-news/all-news.component';
import { OrderProductListComponent } from './pages/billwerk/order-product-list/order-product-list.component';
import { OrderDetailsComponent } from './pages/billwerk/order-details/order-details.component';
import { OrderHistoryComponent } from './pages/billwerk/order-history/order-history.component';
import { InvoicesComponent } from './pages/billwerk/invoices/invoices.component';
import { CompletedOrdersComponent } from './pages/billwerk/completed-orders/completed-orders.component';
import { InvoicesDetailsComponent } from './pages/billwerk/invoices-details/invoices-details.component';
import { UserInvoicesDetailsComponent } from './pages/billwerk/user-invoices-details/user-invoices-details.component';
import { UserCompletedOrdersComponent } from './pages/billwerk/user-completed-orders/user-completed-orders.component';
import { CreateSurveyComponent } from './pages/survey/create-survey/create-survey.component';
import { SurveyComponent } from './pages/survey/survey/survey.component';
import { UpdateSurveyComponent } from './pages/survey/update-survey/update-survey.component';
import { ProductPriceComponent } from './pages/product-price/product-price.component';
import { ClubsComponent } from './pages/clubs/clubs.component';
import { TicketingComponent } from './pages/ticketing/ticketing.component';
import { RequestComponent } from './pages/request/request.component';
import { SurveyVoteComponent } from './pages/survey/survey-vote/survey-vote.component';
import { UserRolesComponent } from './pages/role/user-roles/user-roles.component';
import { ViewSurveyComponent } from './pages/survey/view-survey/view-survey.component';
import { CreateEmailTemplateComponent } from './pages/email-template/create-email-template/create-email-template.component';
import { EmailTemplateComponent } from './pages/email-template/email-template/email-template.component';
import { UpdateEmailTemplateComponent } from './pages/email-template/update-email-template/update-email-template.component';
import { BillwerkProductPriceComponent } from './common/billwerk-products/billwerk-product-price/billwerk-product-price.component';
import { NewsDetailsComponent } from './pages/content/add-content/news/news-details/news-details.component';
import { BillwerkProductsDetailsComponent } from './common/billwerk-products/billwerk-products-details/billwerk-products-details.component';
import { OuterHeaderComponent } from './common/outer-header/outer-header.component';
import { OuterLayoutComponent } from './common/outer-layout/outer-layout.component';
import { ContentComponent } from './common/content/content.component';
import { ContactDocumentsComponent } from './pages/contact-documents/contact-documents.component';
import { ProductComponentDetailsComponent } from './pages/product-component-details/product-component-details.component';
import { BillwerkProductsComponentDetailsComponent } from './common/billwerk-products/billwerk-products-component-details/billwerk-products-component-details.component';
import { AddUserEmailVerifyComponent } from './common/email-verify/add-user-email-verify/add-user-email-verify.component';
import { AdminAssignProductComponent } from './pages/admin-assign-product/admin-assign-product/admin-assign-product.component';
import { AdminAssignedProductsViewComponent } from './pages/admin-assign-product/admin-assigned-products-view/admin-assigned-products-view.component';
import { SpgInstallationComponent } from './common/email-verify/spg-installation/spg-installation.component';
import { UserPersonalizedDocsComponent } from './pages/profile/user-personalized-docs/user-personalized-docs.component';
import { NormalInstallationComponent } from './common/email-verify/normal-installation/normal-installation.component';
import { MemberManagementComponent } from './pages/member-management/member-management/member-management.component';
import { AddMemberComponent } from './pages/member-management/add-member/add-member.component';
import { EditMemberComponent } from './pages/member-management/edit-member/edit-member.component';
import { CutomerMemberListComponent } from './pages/profile/cutomer-member-list/cutomer-member-list.component';
import { MembersStatisticsComponent } from './pages/profile/members-statistics/members-statistics.component';
import { DepartmentsListComponent } from './pages/member-management/department/departments-list/departments-list.component';
import { EditDepartmentsComponent } from './pages/member-management/department/edit-departments/edit-departments.component';
import { InformationComponent } from './pages/profile/information/information.component';
import { AddProductComponent } from './pages/component-products/add-product/add-product.component';
import { ViewProductComponent } from './pages/component-products/view-product/view-product.component';
import { EditProductComponent } from './pages/component-products/edit-product/edit-product.component';
import { AllOrderDisplayComponent } from './pages/billwerk/all-order-display/all-order-display.component';
import { ComponentProductOrderDetailsComponent } from './pages/billwerk/component-product-order-details/component-product-order-details.component';
import { UserInvoicesComponent } from './pages/billwerk/user-invoices/user-invoices.component';
import { UserOrderHistoryComponent } from './pages/profile/user-order-history/user-order-history.component';
import { SalesPartnerComponent } from './pages/sales_partner/sales-partner/sales-partner.component';
import { AssignedCustomersComponent } from './pages/sales_partner/assigned-customers/assigned-customers.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'signin',
    pathMatch: 'full'
  },

  {
    path: '', component: OuterLayoutComponent, children: [
      { path: 'signin', component: LoginComponent, data: { title: 'SignIn' } },
      { path: 'signup', component: RegisterComponent, data: { title: 'SignUp' } },
      { path: 'outer-header', component: OuterHeaderComponent, data: { title: 'Outer Header' } },


      { path: 'forget-password', component: ForgetPasswordComponent, data: { title: 'Forget Password' } },
      { path: 'terms-and-condition', component: TermsAndConditionComponent, data: { title: 'Terms And Condition' } },
      { path: 'user/password/reset/:id', component: ForgetPasswordResetComponent, data: { title: ' Forget Password Reset' } },
      { path: 'user-password-set/:data/:id', component: PasswordComponent, data: { title: 'Email Verfiy' } },
      { path: 'trial-passwords-set/:id', component: TrialEmailVerifyComponent, data: { title: 'Trial Email Verify' } },
      { path: 'password-set/:data', component: AddUserEmailVerifyComponent, data: { title: 'User Add Email Verify' } },
      { path: 'profile-edit-spg', component: SpgInstallationComponent, data: { title: 'SPG Role Installation 3 month trial' } },
      { path: 'profile-edit', component: NormalInstallationComponent, data: { title: 'Normal user license installation page' } },

      { path: 'trial-signup', component: TrialRegistrationComponent, data: { title: 'Trial SignUp' } },

      { path: 'news-description/:data', component: NewsDescriptionComponent, data: { title: ' News Description' } },
      { path: 'all-news', component: AllNewsComponent, data: { title: 'All News' } },

      { path: 'products-prices', component: BillwerkProductPriceComponent, data: { title: 'Billwerk Product Price' } },
      { path: 'products-details/:productId', component: BillwerkProductsDetailsComponent, data: { title: 'Billwerk Product Detail' } },
      { path: 'products-components-details/:productId', component: BillwerkProductsComponentDetailsComponent, data: { title: 'Billwerk Product Component Detail' } },

      { path: 'content/:data', component: ContentComponent, data: { title: 'content' } },

    ]
  },

  {
    path: '', component: LayoutComponent, children: [
      { path: 'edit-profile', component: EditProfileComponent, canActivate: [AuthGuard], data: { title: 'Edit Profile' } },
      { path: 'edit-profile/:id', component: EditProfileComponent, canActivate: [AuthGuard], data: { title: 'Edit Profile' } },
      { path: 'view-profile/:id', component: ViewProfileComponent, canActivate: [AuthGuard], data: { title: 'View Profile' } },
      { path: 'view-profile', component: ViewProfileComponent, canActivate: [AuthGuard], data: { title: 'View Profile' } },
      { path: 'information', component: InformationComponent, canActivate: [AuthGuard], data: { title: 'Information' } },
      { path: 'information/:id', component: InformationComponent, canActivate: [AuthGuard], data: { title: 'Information' } },
      { path: 'contact', component: ContactComponent, canActivate: [AuthGuard], data: { title: 'Contact' } },
      { path: 'contact/:id', component: ContactComponent, canActivate: [AuthGuard], data: { title: 'Contact' } },
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], data: { title: 'Dashboard' } },
      { path: 'user-order-history/:id', component: UserOrderHistoryComponent, canActivate: [AuthGuard], data: { title: 'User Order History' } },

      { path: 'faq', component: FaqComponent, canActivate: [AuthGuard], data: { title: 'Faq' } },
      { path: 'questionnaire', component: QuestionnaireComponent, canActivate: [AuthGuard], data: { title: 'Questionnaire' } },

      { path: 'add-news', component: AddNewsComponent, canActivate: [AuthGuard], data: { title: 'Add News' } },
      { path: 'view-news', component: ViewNewsComponent, canActivate: [AuthGuard], data: { title: 'View News' } },
      { path: 'edit-news/:id', component: EditNewsComponent, canActivate: [AuthGuard], data: { title: 'Edit News' } },
      { path: 'news-details/:data', component: NewsDetailsComponent, canActivate: [AuthGuard], data: { title: ' News Details' } },

      { path: 'online-help', component: OnlineHelpComponent, canActivate: [AuthGuard], data: { title: 'Online Help' } },
      { path: 's-license', component: SLicenseComponent, canActivate: [AuthGuard], data: { title: 'S-License' } },
      { path: 'view-s-license', component: ViewSLicenseComponent, canActivate: [AuthGuard], data: { title: 'View S-License' } },
      { path: 'edit-s-license/:id', component: EditSLicenseComponent, canActivate: [AuthGuard], data: { title: 'Edit S-License' } },
      { path: 'add-pages', component: AddPagesComponent, canActivate: [AuthGuard], data: { title: 'Add Pages' } },
      { path: 'view-pages', component: ViewPagesComponent, canActivate: [AuthGuard], data: { title: 'View Pages' } },
      { path: 'edit-pages/:id', component: EditPagesComponent, canActivate: [AuthGuard], data: { title: 'Edit Pages' } },
      { path: 'edit-pages/:id', component: EditPagesComponent, canActivate: [AuthGuard], data: { title: 'Edit Pages' } },

      { path: 'user', component: UserComponent, canActivate: [AuthGuard], data: { title: 'User' } },
      { path: 'add-permission', component: AddPermissionComponent, canActivate: [AuthGuard], data: { title: 'Add Permission' } },
      { path: 'view-permission', component: ViewPermissionComponent, canActivate: [AuthGuard], data: { title: 'View Permission' } },
      { path: 'edit-permission/:id', component: EditPermissionComponent, canActivate: [AuthGuard], data: { title: 'edit Permission' } },
      { path: 'add-user', component: AddUserComponent, canActivate: [AuthGuard], data: { title: 'Add User' } },
      { path: 'edit-user/:id', component: EditUserComponent, canActivate: [AuthGuard], data: { title: 'Edit User' } },
      { path: 'roles', component: RolesComponent, canActivate: [AuthGuard], data: { title: 'Edit Role' } },
      { path: 'edit-role/:id', component: EditRoleComponent, canActivate: [AuthGuard], data: { title: 'Edit Role' } },
      { path: 'change-password/:id', component: ChangePasswordComponent, canActivate: [AuthGuard], data: { title: 'Change Password' } },
      { path: 'assign-customer', component: CustomerAssignComponent, canActivate: [AuthGuard], data: { title: 'Customer Assign' } },
      { path: 'view-assign-customer', component: ViewCustomerAssignComponent, canActivate: [AuthGuard], data: { title: 'View Customer Assign' } },
      // { path: 'assign-products', component: ProductAssignComponent, canActivate: [AuthGuard], data: { title: 'Products Assign' } },
      { path: 'view-assign-products', component: ViewAssignedProductsComponent, canActivate: [AuthGuard], data: { title: 'View products Assign' } },

      { path: 'order-product-list', component: AllOrderDisplayComponent, canActivate: [AuthGuard], data: { title: 'Order Products List' } },
      { path: 'order-details/:order-id', component: OrderDetailsComponent, canActivate: [AuthGuard], data: { title: 'Order Details' } },
      { path: 'order-history', component: OrderHistoryComponent, canActivate: [AuthGuard], data: { title: 'Order History' } },
      { path: 'completed-order', component: CompletedOrdersComponent, canActivate: [AuthGuard], data: { title: 'Completed Order' } },
      { path: 'invoices', component: InvoicesComponent, canActivate: [AuthGuard], data: { title: 'Invoices' } },
      { path: 'invoice-details/:invoiceId', component: InvoicesDetailsComponent, canActivate: [AuthGuard], data: { title: 'Invoice Details' } },
      { path: 'user-invoice-details/:customerId/:contractId', component: UserInvoicesDetailsComponent, canActivate: [AuthGuard], data: { title: 'User Invoice Details' } },
      { path: 'user-completed-order', component: UserCompletedOrdersComponent, canActivate: [AuthGuard], data: { title: 'Users Completed Order' } },
      { path: 'product-details/:productId', component: ProductDetailComponent, canActivate: [AuthGuard], data: { title: 'Product Detail' } },
      { path: 'product-compoenent-details/:productId', component: ProductComponentDetailsComponent, canActivate: [AuthGuard], data: { title: 'Product Component Detail' } },
      { path: 'component-product-order-details/:order-id', component: ComponentProductOrderDetailsComponent, canActivate: [AuthGuard], data: { title: 'Component Product Order Details' } },
      { path: 'my-invoices', component: UserInvoicesComponent, canActivate: [AuthGuard], data: { title: ' User Invoices' } },

      { path: 'create-survey', component: CreateSurveyComponent, canActivate: [AuthGuard], data: { title: 'Create Survey' } },
      { path: 'survey', component: SurveyComponent, canActivate: [AuthGuard], data: { title: ' Survey' } },
      { path: 'update-survey/:surveyId', component: UpdateSurveyComponent, data: { title: 'Update Survey' } },
      { path: 'survey-vote/:surveyId', component: SurveyVoteComponent, data: { title: ' Survey Vote' } },
      { path: 'survey-view/:surveyId', component: ViewSurveyComponent, data: { title: ' Survey View' } },

      { path: 'requests', component: RequestComponent, data: { title: 'Requests' } },
      { path: 'product-price', component: ProductPriceComponent, data: { title: 'Product Price' } },
      { path: 'sales-partner', component: SalesPartnerComponent, data: { title: 'Sales Partner' } },
      { path: 'clubs', component: ClubsComponent, data: { title: 'Clubs' } },
      { path: 'ticketing', component: TicketingComponent, data: { title: 'Ticketing' } },
      { path: 'requests', component: RequestComponent, data: { title: 'Requests' } },

      { path: 'user-roles', component: UserRolesComponent, data: { title: 'User Roles' } },
      { path: 'create-email-template', component: CreateEmailTemplateComponent, data: { title: 'Create Email Template' } },
      { path: 'email-template', component: EmailTemplateComponent, data: { title: 'Email Template' } },
      { path: 'edit-email-template/:templateId', component: UpdateEmailTemplateComponent, data: { title: 'Update Email Template' } },
      { path: 'contact-documents', component: ContactDocumentsComponent, data: { title: 'Contact Documents' } },
      { path: 'assign-products-user', component: AdminAssignProductComponent, canActivate: [AuthGuard], data: { title: 'Admin Products Assign' } },
      { path: 'assigned-products', component: AdminAssignedProductsViewComponent, canActivate: [AuthGuard], data: { title: 'Admin Products Assign' } },
      { path: 'user-docs/:id', component: UserPersonalizedDocsComponent, canActivate: [AuthGuard], data: { title: 'User Persionalized Docs' } },
      { path: 'user-docs', component: UserPersonalizedDocsComponent, canActivate: [AuthGuard], data: { title: 'User Persionalized Docs' } },

      { path: 'member-management', component: MemberManagementComponent, canActivate: [AuthGuard], data: { title: 'Member Management' } },
      { path: 'add-member', component: AddMemberComponent, canActivate: [AuthGuard], data: { title: 'Add Member' } },
      { path: 'edit-member/:id', component: EditMemberComponent, canActivate: [AuthGuard], data: { title: 'Edit Member' } },
      { path: 'members-list/:customerId', component: CutomerMemberListComponent, canActivate: [AuthGuard], data: { title: 'Customer Member List' } },
      { path: 'members-statistics/:customerId', component: MembersStatisticsComponent, canActivate: [AuthGuard], data: { title: ' Member Statistics' } },
      { path: 'department-list', component: DepartmentsListComponent, canActivate: [AuthGuard], data: { title: ' Department List' } },
      { path: 'edit-department/:id', component: EditDepartmentsComponent, canActivate: [AuthGuard], data: { title: ' Update Department' } },
      { path: 'members-statistics', component: MembersStatisticsComponent, canActivate: [AuthGuard], data: { title: ' Member Statistics' } },

      { path: 'add-product', component: AddProductComponent, canActivate: [AuthGuard], data: { title: 'Add Product' } },
      { path: 'view-product', component: ViewProductComponent, canActivate: [AuthGuard], data: { title: 'View Product' } },
      { path: 'edit-product/:id', component: EditProductComponent, canActivate: [AuthGuard], data: { title: 'Edit Product' } },
      { path: 'assigned-customers/:id', component: AssignedCustomersComponent, canActivate: [AuthGuard], data: { title: 'Assigned Customer' } },
    ]
  },
  { path: '**', component: PageNotFoundComponent, data: { title: 'Page-Not-Found' } },
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }